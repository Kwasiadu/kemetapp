(function () {
    'use strict';

    angular.module('kemetApp')
        .controller('MainCtrl', MainCtrl);



    MainCtrl.$inject = ['GameFactory', 'TileFactory', 'DICardFactory',
        'PlayFactory', 'MLFactory', '$interval', '$timeout', '$mdSidenav', '$http'];


    function MainCtrl (GameFactory, TileFactory, DICardFactory,  PlayFactory,
                       MLFactory, $interval, $timeout, $mdSidenav, $http) {


        var vm = this;
        var gameNo = 1;



        /********************************************************************
         *                General
         *******************************************************************/


        vm.title = "Kemet";
        vm.playerArray = PlayFactory.playerArray;
        vm.redTiles = TileFactory.redTiles;
        vm.whiteTiles = TileFactory.whiteTiles;
        vm.blueTiles = TileFactory.blueTiles;
        vm.dICards = DICardFactory.cards;
        vm.round = GameFactory.round;
        vm.locations = GameFactory.locations;
        vm.message = GameFactory.message;
        vm.buyTile = PlayFactory.buyTile;
        vm.beginGame = beginGame;
        vm.storeLocation = storeLocation;
        vm.destination = GameFactory.destination;
        vm.selectArmy = PlayFactory.selectArmy;
        vm.applyDICard = PlayFactory.applyDICard;
        vm.pray = PlayFactory.pray;
        vm.upgrade = PlayFactory.upgradeAction;
        vm.nextTurn = nextTurn;
        vm.finishMove = PlayFactory.finishMove;
        vm.recruit = recruit;
        vm.nightPhase = PlayFactory.nightPhase;
        vm.isBattlePhase = PlayFactory.isBattlePhase;
        vm.applyBattleCard = PlayFactory.applyBattleCard;
        //vm.army = PlayFactory.armyFn;
        vm.actionButton = actionButton;
        vm.upgradePyramid = PlayFactory.upgradePyramid;
        vm.readyToFight = readyToFight;
        vm.powerTileFunction = PlayFactory.powerTileFunction;
        vm.gameOver = PlayFactory.gameOver;
        vm.showCards = showCards;
        vm.openLeftMenu = openLeftMenu;
        vm.aiControl = aiControl;
        vm.openMenu = openMenu;
        vm.simulation = simulation;
        vm.filterFunction = filterFunction;
        vm.red = false;
        vm.white = false;
        vm.blue = false;
        vm.moves = MLFactory.moves;
        vm.games = MLFactory.games;
        vm.gameNo = gameNo;
        vm.showExtraButtons = GameFactory.showExtraButtons;


        /**
         * Gets the two AI players to play each other in multiple games
         */
        function simulation() {
            $http.get('/').success(function () {
                PlayFactory.allPlayers(function (player) {
                    player.aiControlled = true;
                });
                $interval(function () {
                    beginGame();
                    $timeout(runSimulation, 10);
                    gameNo++;
                }, 2000, 10 )

            })

        }

        /**
         * Gets the AI players to play each other for one game
         */
        function runSimulation() {
            nextTurn();
            aI();
            if(PlayFactory.gameOver) {
                if (GameFactory.recordGame) {
                    MLFactory.recordGame(gameNo);
                    console.log('game ' + gameNo + ' recorded');
                }
                var stats = '';
                PlayFactory.allPlayers(function(player){
                    stats += '\nplayer ' + player.name + ': ' + player.points;
                });
                //console.log('Game ' + gameNo + ' over' + stats);
                PlayFactory.resetGame();
                return;

            }
            $timeout(runSimulation, 10);
        }

        /**
         * Takes initial player actions
         */
        function beginGame(){
            var player = PlayFactory.getPlayer();
            GameFactory.gameStarted = true;
            vm.gameStarted = GameFactory.gameStarted;
            player.aiControlled = true;
            var opponent = PlayFactory.getOpponent(player);
            opponent.aiControlled = true;
            if(player.aiControlled || opponent.aiControlled) {
                MLFactory.beginGame();
            } else {
                PlayFactory.beginGame();
            }
            aI();
        }

        /**
         * Ends current players turn and starts next player's turn
         */
        function nextTurn() {
            PlayFactory.nextTurn();
            if(PlayFactory.getPlayer().actionsLeft <= 0) {
                PlayFactory.nextTurn();
            }
            PlayFactory.checkForEndOfRound();
            aI();
        }

        /**
         * Initiates battle phase
         */
        function readyToFight() {
            var player = PlayFactory.getPlayer();
            var opponent = PlayFactory.getOpponent();
            if (opponent.aiControlled) {
                PlayFactory.readyToFight();
                player = PlayFactory.getPlayer();

                opponent = PlayFactory.getOpponent();
                MLFactory.aiFight(player, opponent);
                PlayFactory.readyToFight();
            } else {
                PlayFactory.readyToFight();
            }
        }

        /**
         * Executes the turn of the AI
         */
        function aI () {
            if(PlayFactory.getPlayer().aiControlled && !PlayFactory.gameOver) {
                MLFactory.run();
                PlayFactory.nextTurn();
            }
        }

        /**
         * Reveals players hidden card
         * @param owner
         */
        function showCards(owner) {
            PlayFactory.showCards(owner);
        }

        /**
         * toogles between human and AI control
         * @param owner
         */
        function aiControl(owner){
            PlayFactory.toggleAIControl(owner);
        }

        /**
         * opens left side navigation bar
         */
        function openLeftMenu(){
            vm.red = true;
            vm.white = true;
            vm.blue = true;
            $mdSidenav('left').toggle();
        }

        /**
         * opens side navigation bar
         * @param componentId
         */
        function openMenu(componentId) {
            $mdSidenav(componentId).toggle();
        }

        /**
         * responds to user clicking an action button
         * @param action
         * @param owner
         * @param id
         */
        function actionButton(action, owner, id){
            vm.red = false;
            vm.white = false;
            vm.blue = false;
            var pristine = false;
            if(action === 'finish'){
                pristine = PlayFactory.checkButtonAvailability('#'+id);
                if(pristine){
                    openMenu('left');
                    var color = id.substr(0, id.length-3);
                    vm[color]= true;
                }
            } else if(action === 'upgrade'){
                pristine = PlayFactory.checkButtonAvailability('#'+id);
                if(pristine){
                    openMenu('right');
                }            }
            PlayFactory.actionButton(action, owner, id)
        }

        /**
         * responds to player selecting the recruit button
         * @param event
         */
        function recruit(event) {
            var player = PlayFactory.getPlayer();
            var location = event.target.id;
            var cityClass = event.target.className.baseVal;
            PlayFactory.recruit(location, player, cityClass);
        }

        /**
         * stores location or space that the user has selected for movement purposes
         * @param event
         */
        function storeLocation(event) {
            PlayFactory.storeLocation(event.target.id);
        }

        /**
         * filter function allowing ng-repeat to exclude displaying the first game
         * @param element
         * @returns {boolean}
         */
        function filterFunction(element) {
            return element.game > 1;
        }
    }
})();
