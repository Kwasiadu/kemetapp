(function () {

    'use strict';
    angular.module('kemetApp')
        .directive('pyramid', pyramid)
        .directive('space', space)
        .directive('army', army)
        .directive('tile', tile)
        .directive('mouse', mouse);

    var color1 = 'transparent';
    var color2 = 'purple';
    var color2m = '#800080';
    var color3 = 'black';
    var color3m = '#000000';
    var color5 = 'red';


    pyramid.$inject = [];
    function pyramid () {
        return {
            restrict: 'E',
            transclude: false,
            scope: {
                color: "@",
                level: "="
            },
            template: '<div class={{color}}>{{level}}</div>',

        }
    }

    space.$inject = ['PlayFactory', 'GameFactory'];
    function space (PlayFactory, GameFactory) {
        return {
            restrict: 'A',

            link: function(scope, element, attrs) {
                var name = attrs.id;
                
                element.on('click', storeLocation);
                element.on('dblclick', recruit);


                function storeLocation () {
                    PlayFactory.destination = name;
                    angular.element('polygon.space').css('fill', color1);
                    element.css('fill', color5);
                }

                function recruit () {
                    
                    PlayFactory.recruit(name);
                    scope.$apply();
                }

            }
        }

    }


    army.$inject = ['PlayFactory'];
    function army (PlayFactory)  {
        return {
            templateNamespace: 'svg',
            replace: true,
            restrict: 'E',
            controller: 'MainCtrl',
            controllerAs: 'vm',
            transclude: false,
            scope: {
                size: "=",
                location: "=",
                creature: "=",
                posx: '=',
                posy: '=',
                color: '=',
                armyId: '=',
                playerNo: '='

            },
            template: '<g class="army" style="fill: {{color}}" ' +
            'ng-attr-data={{location}} id="{{armyId}}"> <rect height="30"' +
        ' width="30"  rx="5" ry="5"' +
            ' ng-attr-x="{{posx}}" ng-attr-y="{{posy}}" ></rect> ' +
            '<text ng-attr-x="{{posx}}" ng-attr-y="{{posy}}"' +
            '  dy="20px" dx="10px">{{size}} {{creature}}</text></g>',
            link: function(scope, element, attrs) {
                element.on('click', function () {
                    PlayFactory.armyFn(
                        PlayFactory.playerArray[scope.playerNo].armies[scope.armyId],
                        PlayFactory.playerArray[scope.playerNo].name

                    );

                    scope.$apply();

                });
                // scope.$watch('vm.playerArray[playerNo].armies[armyId]', function (newValue) {
                //     if(newValue) {
                //         console.log('army ' + scope.armyId + ' has changed');
                //     }
                // }, true);


            }

        }
    }


    tile.$inject = [];
    function tile () {
        return {
            restrict: 'E',
            controller: 'MainCtrl',
            controllerAs: 'vm',
            transclude: true,
            scope: {
                name: '=',
                img: '='
            },
            template: '<md-card> <md-card-content> <h3>{{name | uppercase }}</h3>' +
            ' </md-card-content> <img ng-src="{{img}}"> </md-card>',

        }
    }



    mouse.$inject = [];
    function mouse () {
        return {
            restrict: 'E',
            scope: {
                posx: '=',
                posy: '='
            },
            template:'<pre>x: {{posx}}  y: {{posy}}</pre>',

        }
    }
})();


