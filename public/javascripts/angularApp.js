(function () {

    'use strict';

    angular.module('kemetApp', ['ui.router', 'ngMaterial'])
        .config(appConfig);

    appConfig.$inject = ['$urlRouterProvider', '$stateProvider'];
    function appConfig ($urlRouterProvider, $stateProvider) {
        //UI-Router Default Config
        $urlRouterProvider.otherwise('home');

        //Defining routes
        $stateProvider
            .state('home', {
                url:        '/home',
                templateUrl:'templates/gameBoard.html',
                controller: 'MainCtrl',
                controllerAs: 'vm',


            })
            .state('moves', {
                url: '/moves',
                templateUrl: 'templates/moves.html',
                controller: 'MainCtrl',
                controllerAs: 'vm',
                resolve :{
                    postPromise: ['MLFactory', function(MLFactory){
                        return MLFactory.getAllMoves();
                    }]
                }
            })
            .state('games', {
                url: '/games',
                templateUrl: 'templates/games.html',
                controller: 'MainCtrl',
                controllerAs: 'vm',
                resolve :{
                    postPromise: ['MLFactory', function(MLFactory){
                        return MLFactory.getAllGames();
                    }]
                }

            });
    }
})();
