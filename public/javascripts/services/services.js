(function () {
    'use strict';

    angular.module('kemetApp')
        .factory('GameFactory', GameFactory)
        .factory('MoveFactory', MoveFactory)
        .factory('PlayerOneFactory', PlayerOneFactory)
        .factory('PlayerTwoFactory', PlayerTwoFactory)
        .factory('PlayerThreeFactory', PlayerThreeFactory)
        .factory('PlayerFourFactory', PlayerFourFactory)
        .factory('PlayerFiveFactory', PlayerFiveFactory)
        .factory('TileFactory', TileFactory)
        .factory('DICardFactory', DICardFactory)
        .factory('BattleCardFactory', BattleCardFactory);


    /*********************************************************************
     factory Factory
     /*********************************************************************/

    GameFactory.$inject = [];
    function GameFactory() {
        var factory = {};
        factory.maxPyramidLevel = 4;
        factory.initialPrayer = 7;
        factory.maxPrayer = 11;
        factory.maxSoldiers = 12;
        factory.round = 1;
        factory.movement = 1;
        factory.armyOffset = 25;
        //Make sure the names in the array match the names of the
        // factories;
        factory.LearningMode = true;
        factory.maxDistance = 5;

        factory.noOfFreeUpgrades = 3;
        factory.maxActions = 5;
        factory.pointsToWin = 8;
        factory.reinforcementsNo = 4;
        factory.initialRecruitNo = 10;
        factory.veto = false;
        factory.endOfBattle = false;
        factory.isAI = true;
        factory.isNightPhase = false;
        factory.recordGame = true;
        factory.assert = assert;
        factory.testing = false;
        factory.gameStarted = false;
        factory.showExtraButtons = false;

        factory.colors = ['red', 'white', 'blue'];
        factory.creatures = ['royal scarab', 'giant scorpion', 'phoenix',
            'the mummy', 'ancestral elephant', 'deep desert snake', 'sphinx'];

        factory.markerPoints =  [
            "653,240 670,240 670,256 653,256",
            "653,258 670,258 670,274 653,274"
        ];
        factory.turnMarker = [
            {posx: 653, posy: 240},
            {posx: 653, posy: 258}
        ];



        // nextTurn sets whoseTurn variable to the next player in the

        // turn order.

        // This object contains the names, army positions and boundaries of the
        // locations on the playing board
        factory.locations = {
            redC1: {
                name: 'redC1', primary: [8,80], secondary: [30,55],
                points: "0,52, 33,55 58,106 57,128 12,126 8,165 0,169 "
            },
            whiteC1: {
                name: 'whiteC1', primary: [25,155], secondary: [40,100],
                points: "71,133 70,206 13,222 17,129"
            },
            blueC1:
            {
                name: 'blueC1', primary: [10,230], secondary: [30,150],
                points:"0,171 11,171 11,226 73,211 38,276 3,276"
            },
            redC2: {
                name: 'redC2', primary: [450,3], secondary: [275,3],
                points:"416,0 544,0 550,31 515,43 504,24 425,47 "
            },
            whiteC2:             {
                name: 'whiteC2', primary: [460,50], secondary: [290,40],
                points:"430,56 499,31 501,75 461,95 433,75"
            },
            blueC2:            {
                name: 'blueC2', primary: [500,80], secondary: [310,70],
                points:"462,106 507,83, 514,52 561,32 545,102 482,151 "
            },
            buff1:            {
                name: 'buff1', primary: [90,90], secondary: [90,115],
                points:"0,0 70,0 121,71 154,80 150,178 161,260 137,275" +
                " 54,275 82,210 73,101 35,50 0,48"
            },
            buff2:            {
                name: 'buff2', primary: [300,160], secondary: [220,108],
                points:"146,275 186,256 214,261 240,254 259,242 295,160" +
                " 332,146 437,170 458,188 410,213 348,180 303,222 300,274"
            },
            buff3:             {
                name: 'buff3', primary: [330,35], secondary: [225, 60],
                points:"243,38 284,0 413,0 415,82 471,166 462,183 444,168" +
                " 440,166 328,139 304,83"
            },
            buff4:            {
                name: 'buff4', primary: [500,160], secondary: [330, 105],
                points:"488,186 563,102 571,133 563,155 522,185"
            },
            temp1:            {
                name: 'temp1', primary: [175,10], secondary: [100,10],
                points:"75,0 276,0 232,42 189,57 150,76 121,69"
            },
            temp2:            {
                name: 'temp2', primary: [330,200], secondary: [243,135],
                points:"348,186 377,196 395,224 327,274 303,274 304,225 306,222"
            },
            temp3:            {
                name: 'temp3', primary: [580,160], secondary: [360,120],
                points:"524,184 625,271 635,77 563,94 573,147"
            },
            obel1:            {
                name: 'obel1', primary: [170,100], secondary: [200,100],
                points:"158,77 238,43 294,84 322,140 290,155 257,237 211,257" +
                " 185,252 165,235 158,198"
            }
        };

        // The nextTo object contains the neighbors of every location. This
        // is used in move function to know where the army can move
        factory.nextTo = {
            redC1: ['whiteC1', 'blueC1', 'buff1'],
            whiteC1: ['redC1', 'blueC1', 'buff1'],
            blueC1: ['redC1', 'whiteC1', 'buff1'],
            redC2: ['whiteC2', 'blueC2', 'buff3'],
            whiteC2: ['redC2', 'blueC2', 'buff3'],
            blueC2: ['redC2', 'whiteC2', 'buff3'],
            buff1: ['redC1', 'whiteC1', 'blueC1', 'buff2', 'obel1', 'temp1'],
            buff2: ['temp2', 'buff1', 'buff3', 'obel1'],
            buff3: ['redC2', 'whiteC2', 'blueC2', 'buff2', 'obel1', 'temp1'],
            buff4: ['temp3'],
            temp1: ['buff1', 'buff3', 'obel1'],
            temp2: ['buff2'],
            temp3: ['buff4'],
            obel1: ['temp1', 'buff1', 'buff2', 'buff3']
        };

        factory.obeliskSpaces = ['obel1', 'temp1', 'temp2', 'temp3'];
        factory.cities = ['redC1', 'whiteC1', 'blueC1', 'redC2', 'whiteC2', 'blueC2'];

        function assert(condition, errorMessage, successMessage) {
            if (!condition) {
                errorMessage = errorMessage || "Assertion failed";
                console.trace('Error: ' + errorMessage);
            } else {
                successMessage = successMessage || 'Assertion passed';
                console.trace('Pass: ' + successMessage);
            }
        }

        return factory;
    }


    /*********************************************************************
     Move Factory
     /*********************************************************************/

    MoveFactory.$inject = [];
    function MoveFactory() {
        var factory = {};
        factory.moves = [];
        return factory;
    }
    
    
    /*********************************************************************
     Player One Factory
     /*********************************************************************/

    PlayerOneFactory.$inject = ['GameFactory'];
    function PlayerOneFactory(GameFactory) {
        var factory = {};
        factory.pyramidGeneralDiscountApplied = false;

        factory.name ="One";
        factory.color = 'red';
        factory.colorm = '#ff0000';
        factory.position = 0;
        factory.turnMarker = {};
        factory.turnMarker.posx = GameFactory.turnMarker[factory.position].posx;
        factory.turnMarker.posy = GameFactory.turnMarker[factory.position].posy;
        factory.isTurn = true;
        factory.playedBattleCards = [];
        factory.unplayedBattleCards = [];
        factory.dICards = [];
        factory.fightCards = [];
        
        factory.tileBought = false;
        factory.isBuyingRedTile = false;
        factory.isBuyingWhiteTile = false;
        factory.isBuyingBlueTile = false;
        factory.canRecruit = false;
        factory.isRecruiting = false;
        factory.isMoving = false;
        factory.canMove = false;
        factory.moved = false;
        factory.movementStarted = false;
        factory.isUpgrading = false;

        factory.redTileBought = false;
        factory.whiteTileBought = false;
        factory.blueTileBought = false;
        factory.hasUpgraded = false;
        factory.noOfPrayers = 0;
        factory.noOfRecruitments = 0;
        factory.noOfMovementActions = 0;

        factory.attackStrength = 0;
        factory.defenseStrength = 0;
        factory.movement = GameFactory.movement;
        factory.damage = 0;
        factory.shield = 0;
        factory.points = 0;
        factory.battlePoints = 0;
        factory.templeTPoints = 0;
        factory.templePPoints = 0;
        factory.tilePoints = 0;
        factory.pyramidPoints = 0;
        factory.nightPoints = 0;
        factory.nightPrayer = 2;
        factory.templePrayerBonus = 0;
        factory.maxArmy = 5;
        factory.dIBonus = 0;
        factory.teleportCost = 2;
        factory.attacksInitiated = 0;

        factory.isBuyingOffensiveStrategy = false;
        factory.isBuyingDefensiveStrategy = false;

        factory.godSpeedAbility = false;
        factory.openGatesAbility = false;
        factory.obeliskTeleportAbility = false;
        factory.divineWoundAbility = false;
        factory.initiativeAbility = false;
        factory.actOfGodAbility = false;
        factory.handOfGodAbility = false;
        factory.divineWillAbility = false;
        factory.crusadeAbility = false;
        factory.prescienceAbility = false;
        factory.reinforcementsAbility = false;
        factory.visionAbility = false;
        factory.prayerIncrement = 2;
        factory.holyWarAbility = false;
        factory.deepDesertSnakeAbility = false;
        factory.defensiveVictoryAbility = false;
        factory.sphinxPointsApplied = false;


        factory.generalDiscount = false;
        factory.teleportDiscount = 0;
        factory.tileDiscount = 0;
        factory.pyramidDiscount = 0;
        factory.recruitDiscount = 0;

        factory.redPyramidLevel = 0;
        factory.whitePyramidLevel = 0;
        factory.bluePyramidLevel = 0;
        factory.redOriginalPyramidLevel = 0;
        factory.whiteOriginalPyramidLevel = 0;
        factory.blueOriginalPyramidLevel = 0;
        factory.prayer = GameFactory.initialPrayer;
        factory.actionsLeft = GameFactory.maxActions;
        factory.actionsTaken = 0;
        factory.message = "";
        factory.fieldArmyNo = 0;
        factory.reserveArmyNo = GameFactory.maxSoldiers - factory.fieldArmyNo;

        factory.cardStrength = 0;
        factory.cardDamage = 0;
        factory.cardShield = 0;
        factory.dWCardStrength = 0;

        factory.creatureMovement = 0;

        factory.battleCardApplied = false;
        factory.cardDiscarded = false;
        factory.isSelectingDivineWoundCards = false;
        factory.divineWoundCardsSelected = false;
        factory.actOfGodAbilitySelected = false;
        factory.divineWillAbilitySelected = false;
        factory.visionDICardSelected = false;
        factory.isUsingTeleportationCard = false;
        factory.isRainingFire = false;
        factory.isMovingOpponent = false;
        factory.isEscaping = false;
        factory.hasOpenGates = false;
        factory.pyramidColorSelected = "";
        factory.victoryPointBought = false;
        factory.actOfGodBought = false;
        factory.actOfGodUsed = false;
        factory.divineWillUsed = false;
        factory.isAttacking = false;
        factory.aiControlled = false;
        factory.postBattleChoice = '';
        factory.retreatLocation = '';
        factory.showCards = false;



        factory.redTiles = [];
        factory.whiteTiles = [];
        factory.blueTiles = [];

        factory.creatureInHand = false;
        factory.creatureName = '';
        factory.creaturesDeployed = [];


        factory.buttons = [
            {
                name: "moveOnerow1",
                points: "33,361 59,361 59,381 33,381",
                class: "button move One row1",
                effect: "finishMove"
            },
            {
                name: "recruitOne",
                points: "68,361 95,361 95,381 68,381",
                class: "button recruit One row1",
                effect: "recruit"
            },
            {
                name: "upgradeOne",
                points: "16,394, 43,394 43, 415 16,415",
                class: "button upgrade One row2",
                effect: "upgrade"
            },
            {
                name: "moveOnerow2",
                points: "51,394 80,394 80,415 51,415",
                class: "button move One row2",
                effect: "finishMove"
            },
            {
                name: "prayOnerow2",
                points: "85,394 114,394 114,415 85,415",
                class: "button pray One row2",
                effect: "pray"
            },
            {
                name: "whiteOne",
                points: "0,428 28,428 28,450 0,450",
                class: "button white One row3",
                effect: "finish"
            },
            {
                name: "redOne",
                points: "34,428 62,428 62,450 34,450",
                class: "button red One row3",
                effect: "finish"
            },
            {
                name: "blueOne",
                points: "68,429 97,429 97,450 68,450",
                class: "button blue One row3",
                effect: "finish"
            },
            {
                name: "prayOnerow3",
                points: "104,429 131,429 131,450 104,450",
                class: "button pray One row3",
                effect: "pray"
            },
        ];

        // Armies object contains data for all of the player's armies
        factory.armies = [
            // {id: 'One1', size: 5, location: 'redC1', creature:'',
            //     posx: GameFactory.locations.redC1.primary[0],
            //     posy: GameFactory.locations.redC1.primary[1] },
            // {id: 'One2', size: 5, location: 'whiteC1', creature:'',
            //     posx: GameFactory.locations.whiteC1.primary[0],
            //     posy: GameFactory.locations.whiteC1.primary[1] },
            {id: 'One1', size: 0, location: 'heaven', creature:'',
                posx: GameFactory.locations.redC1.primary[0],
                posy: GameFactory.locations.redC1.primary[1] },
            {id: 'One2', size: 0, location: 'heaven', creature:'',
                posx: GameFactory.locations.whiteC1.primary[0],
                posy: GameFactory.locations.whiteC1.primary[1] },
            {id: 'One3', size: 0, location: 'heaven', creature:'',
                posx: GameFactory.locations.blueC1.primary[0],
                posy: GameFactory.locations.blueC1.primary[1]},
            {id: 'One4', size: 0, location: 'heaven', creature:'',
                posx: GameFactory.locations.redC1.primary[0],
                posy: GameFactory.locations.redC1.primary[1] },
            {id: 'One5', size: 0, location: 'heaven', creature:'',
                posx: GameFactory.locations.temp1.primary[0],
                posy: GameFactory.locations.temp1.primary[1] },
            {id: 'One6', size: 0, location: 'heaven', creature:'',
                posx: GameFactory.locations.temp2.primary[0],
                posy: GameFactory.locations.temp2.primary[1] },
            {id: 'One7', size: 0, location: 'heaven', creature:'',
                posx: GameFactory.locations.redC1.primary[0],
                posy: GameFactory.locations.redC1.primary[1] },
            {id: 'One8', size: 0, location: 'heaven', creature:'',
                posx: GameFactory.locations.redC1.primary[0],
                posy: GameFactory.locations.redC1.primary[1] },
            {id: 'One9', size: 0, location: 'heaven', creature:'',
                posx: GameFactory.locations.redC1.primary[0],
                posy: GameFactory.locations.redC1.primary[1] },

        ];
        return factory;
    }

    /*********************************************************************
     Player Two Factory
     /*********************************************************************/

    PlayerTwoFactory.$inject = ['GameFactory'];
    function PlayerTwoFactory(GameFactory) {
        var factory = {};
        factory.pyramidGeneralDiscountApplied = false;

        factory.name ="Two";
        factory.color = 'blue';
        factory.colorm = '#0000ff';
        factory.position = 1;
        factory.turnMarker = {};

        factory.turnMarker.posx = GameFactory.turnMarker[factory.position].posx;
        factory.turnMarker.posy = GameFactory.turnMarker[factory.position].posy;

        factory.isTurn = false;
        factory.playedBattleCards = [];
        factory.unplayedBattleCards = [];
        factory.dICards = [];
        factory.fightCards = [];

        factory.tileBought = false;
        factory.canRecruit = false;
        factory.isRecruiting = false;
        factory.isMoving = false;
        factory.canMove = false;
        factory.moved = false;
        factory.movementStarted = false;
        factory.isUpgrading = false;

        factory.redTileBought = false;
        factory.whiteTileBought = false;
        factory.blueTileBought = false;
        factory.hasUpgraded = false;
        factory.noOfPrayers = 0;
        factory.noOfRecruitments = 0;
        factory.noOfMovementActions = 0;


        factory.attackStrength = 0;
        factory.defenseStrength = 0;
        factory.movement = GameFactory.movement;
        factory.damage = 0;
        factory.shield = 0;
        factory.points = 0;
        factory.battlePoints = 0;
        factory.templeTPoints = 0;
        factory.templePPoints = 0;
        factory.tilePoints = 0;
        factory.pyramidPoints = 0;
        factory.nightPoints = 0;
        factory.nightPrayer = 2;
        factory.templePrayerBonus = 0;
        factory.maxArmy = 5;
        factory.dIBonus = 0;
        factory.teleportCost = 2;
        factory.attacksInitiated = 0;

        factory.isBuyingOffensiveStrategy = false;
        factory.isBuyingDefensiveStrategy = false;

        factory.godSpeedAbility = false;
        factory.openGatesAbility = false;
        factory.obeliskTeleportAbility = false;
        factory.divineWoundAbility = false;
        factory.initiativeAbility = false;
        factory.actOfGodAbility = false;
        factory.handOfGodAbility = false;
        factory.divineWillAbility = false;
        factory.crusadeAbility = false;
        factory.prescienceAbility = false;
        factory.reinforcementsAbility = false;
        factory.visionAbility = false;
        factory.prayerIncrement = 2;
        factory.holyWarAbility = false;
        factory.deepDesertSnakeAbility = false;
        factory.defensiveVictoryAbility = false;
        factory.isAttacking = false;

        factory.generalDiscount = false;
        factory.teleportDiscount = 0;
        factory.tileDiscount = 0;
        factory.pyramidDiscount = 0;
        factory.recruitDiscount = 0;

        factory.redPyramidLevel = 0;
        factory.whitePyramidLevel = 0;
        factory.bluePyramidLevel = 0;
        factory.redOriginalPyramidLevel = 0;
        factory.whiteOriginalPyramidLevel = 0;
        factory.blueOriginalPyramidLevel = 0;
        
        factory.prayer = GameFactory.initialPrayer;
        factory.actionsLeft = GameFactory.maxActions;
        factory.actionsTaken = 0;
        factory.message = "";
        factory.fieldArmyNo = 0;
        factory.reserveArmyNo = GameFactory.maxSoldiers - factory.fieldArmyNo;
        factory.cardStrength = 0;
        factory.cardDamage = 0;
        factory.cardShield = 0;
        factory.dWCardStrength = 0;

        factory.creatureMovement = 0;

        factory.battleCardApplied = false;
        factory.cardDiscarded = false;
        factory.isSelectingDivineWoundCards = false;
        factory.divineWoundCardsSelected = false;
        factory.actOfGodAbilitySelected = false;
        factory.divineWillAbilitySelected = false;
        factory.visionDICardSelected = false;
        factory.isUsingTeleportationCard = false;
        factory.isRainingFire = false;
        factory.isMovingOpponent = false;
        factory.isEscaping = false;
        factory.hasOpenGates = false;
        factory.pyramidColorSelected = "";
        factory.victoryPointBought = false;
        factory.actOfGodBought = false;
        factory.actOfGodUsed = false;
        factory.divineWillUsed = false;
        factory.aiControlled = false;
        factory.postBattleChoice = '';
        factory.retreatLocation = '';
        factory.showCards = false;
        factory.sphinxPointsApplied = false;




        factory.redTiles = [];
        factory.whiteTiles = [];
        factory.blueTiles = [];
        

        factory.creatureInHand = false;
        factory.creatureMovementApplied = false;
        factory.creatureName = '';
        factory.creaturesDeployed = [];

        factory.buttons = [
            {
                name: "moveTworow1",
                points: "577,360 604,360 604,381 577,381",
                class: "button move Two row1",
                effect: "finishMove"
            },
            {
                name: "recruitTwo",
                points: "612,361 640,361 640,381 612,381",
                class: "button recruit Two row1",
                effect: "recruit"
            },
            {
                name: "upgradeTwo",
                points: "556,394, 590,394 590,415 556,415",
                class: "button upgrade Two row2",
                effect: "upgrade"
            },
            {
                name: "moveTworow2",
                points:"593,394 625,394 625,415 593,415",
                class: "button move Two row2",
                effect: "finishMove"
            },
            {
                name: "prayTworow2",
                points: "632,394 661,394 661,415 632,415",
                class: "button pray Two row2",
                effect: "pray"
            },
            {
                name: "whiteTwo",
                points: "544,428 572,428 572,450 544,450",
                class: "button white Two row3",
                effect: "finish"
            },
            {
                name: "redTwo",
                points: "578,428 606,428 606,450 578,450",
                class: "button red Two row3",
                effect: "finish"
            },
            {
                name: "blueTwo",
                points: "612,429 641,429 641,450 612,450",
                class: "button blue Two row3",
                effect: "finish"
            },
            {
                name: "prayTworow3",
                points: "648,429 675,429 675,450 648,450",
                class: "button pray Two row3",
                effect: "pray"
            },
        ];
        // Armies object contains data for all of the player's armies
        factory.armies = [
            // {id: 'Two1', size: 5, location: 'redC2', creature:'',
            //     posx: GameFactory.locations.redC2.primary[0],
            //     posy: GameFactory.locations.redC2.primary[1] },
            // {id: 'Two2', size: 5, location: 'whiteC2', creature:'',
            //     posx: GameFactory.locations.whiteC2.primary[0],
            //     posy: GameFactory.locations.whiteC2.primary[1] },
            {id: 'Two1', size: 0, location: 'heaven', creature:'',
               posx: GameFactory.locations.redC2.primary[0],
               posy: GameFactory.locations.redC2.primary[1] },
            {id: 'Two2', size: 0, location: 'heaven', creature:'',
               posx: GameFactory.locations.whiteC2.primary[0],
               posy: GameFactory.locations.whiteC2.primary[1] },
            {id: 'Two3', size: 0, location: 'heaven', creature:'',
                posx: GameFactory.locations.blueC2.primary[0],
                posy: GameFactory.locations.blueC2.primary[1] },
            {id: 'Two4', size: 0, location: 'heaven', creature:'',
                posx: GameFactory.locations.blueC2.primary[0],
                posy: GameFactory.locations.blueC2.primary[1]},
            {id: 'Two5', size: 0, location: 'heaven', creature:'',
                posx: GameFactory.locations.temp3.primary[0],
                posy: GameFactory.locations.temp3.primary[1] },
            {id: 'Two6', size: 0, location: 'heaven', creature:'',
                posx: GameFactory.locations.redC2.primary[0],
                posy: GameFactory.locations.redC2.primary[1] },
            {id: 'Two7', size: 0, location: 'heaven', creature:'',
                posx: GameFactory.locations.redC2.primary[0],
                posy: GameFactory.locations.redC2.primary[1] },
            {id: 'Two8', size: 0, location: 'heaven', creature:'',
                posx: GameFactory.locations.redC2.primary[0],
                posy: GameFactory.locations.redC2.primary[1] },
            {id: 'Two9', size: 0, location: 'heaven', creature:'',
                posx: GameFactory.locations.redC2.primary[0],
                posy: GameFactory.locations.redC2.primary[1] },




        ];





        return factory;
    }


    /*********************************************************************
     Tile Factory
     /*********************************************************************/

    TileFactory.$inject = ['BattleCardFactory'];
    function TileFactory (BattleCardFactory) {
        var factory = {};
        factory.offensiveStrategy = offensiveStrategy;
        factory.defensiveStrategy = defensiveStrategy;


        /****************************************************
         * Red power tiles
         * **************************************************/

        factory.redTiles = [
            {
                name: 'charge',
                cost: 1,
                color: 'red',
                phase: 'battle',
                img: "img/charge.jpg",
                effect: charge
            },
            {
                name: 'charge',
                cost: 1,
                color: 'red',
                phase: 'battle',
                img: "img/charge.jpg",
                effect: charge
            },
            {
                name: 'stargate',
                cost: 1,
                color: 'red',
                phase: 'day',
                img: "img/stargate.jpg",
                effect: stargate
            },
            {
                name: 'god speed',
                cost: 1,
                color: 'red',
                phase: 'day',
                img: "img/god-speed.jpg",
                effect: godSpeed
            },

            {
                name: 'carnage',
                cost: 2,
                color: 'red',
                phase: 'battle',
                img: "img/carnage.jpg",
                effect: carnage
            },
            {
                name: 'offensive strategy',
                cost: 2,
                color: 'red',
                phase: 'day',
                img: "img/offensive-strategy.jpg",
                effect: offensiveStrategy
            },
            {
                name: 'open gates',
                cost: 2,
                color: 'red',
                phase: 'day',
                img: "img/open-gatesP.jpg",
                effect: openGates
            },
            {
                name: 'teleport',
                cost: 2,
                color: 'red',
                phase: 'day',
                img: "img/teleport.jpg",
                effect: teleport
            },

            {
                name: 'royal scarab',
                cost: 3,
                color: 'red',
                phase: 'day',
                img: "img/royal-scarab.jpg",
                effect: royalScarab
            },
            {
                name: 'blades of neath',
                cost: 3,
                color: 'red',
                phase: 'battle',
                img: "img/blades-of-neath.jpg",
                effect: bladesOfNeath
            },
            {
                name: 'divine wound',
                cost: 3,
                color: 'red',
                phase: 'battle',
                img: "img/divine-wound.jpg",
                effect: divineWound
            },
            {
                name: 'red victory point',
                cost: 3,
                color: 'red',
                phase: 'day',
                img: "img/red-victory-point.jpg",
                effect: victoryPoint
            },

            {
                name: 'red act of god',
                cost: 4,
                color: 'red',
                phase: 'day',
                img: "img/red-act-of-god.jpg",
                effect: actOfGod
            },
            {
                name: 'giant scorpion',
                cost: 4,
                color: 'red',
                phase: 'day',
                img: "img/scorpion.jpg",
                effect: giantScorpion
            },
            {
                name: 'initiative',
                cost: 4,
                color: 'red',
                phase: 'day',
                img: "img/initiative.jpg",
                effect: initiative
            },
            {
                name: 'phoenix',
                cost: 4,
                color: 'red',
                phase: 'day',
                img: "img/phoenix.jpg",
                effect: phoenix
            }

        ];

        /****************************************************
         * White power tiles
         * **************************************************/
        factory.whiteTiles = [

            {
                name: 'priest',
                cost: 1,
                color: 'white',
                phase: 'day',
                img: "img/priest.jpg",
                effect: priest
            },
            {
                name: 'priest',
                cost: 1,
                color: 'white',
                phase: 'day',
                img: "img/priest.jpg",
                effect: priest
            },
            {
                name: 'priestess',
                cost: 1,
                color: 'white',
                phase: 'day',
                img: "img/priestess.jpg",
                effect: priestess
            },
            {
                name: 'priestess',
                cost: 1,
                color: 'white',
                phase: 'day',
                img: "img/priestess.jpg",
                effect: priestess
            },

            {
                name: 'slaves',
                cost: 2,
                color: 'white',
                phase: 'day',
                img: "img/slaves.jpg",
                effect: slaves
            },
            {
                name: 'high priest',
                cost: 2,
                color: 'white',
                phase: 'night',
                img: "img/high-priest.jpg",
                effect: highPriest
            },
            {
                name: 'crusade',
                cost: 2,
                color: 'white',
                phase: 'battle',
                img: "img/crusade.jpg",
                effect: crusade
            },
            {
                name: 'divine boon',
                cost: 2,
                color: 'white',
                phase: 'night',
                img: "img/divine-boon.jpg",
                effect: divineBoon
            },

            {
                name: 'hand of god',
                cost: 3,
                color: 'white',
                phase: 'night',
                img: "img/hand-of-god.jpg",
                effect: handOfGod
            },
            {
                name: 'vision',
                cost: 3,
                color: 'white',
                phase: 'night',
                img: "img/vision.jpg",
                effect: vision
            },
            {
                name: 'holy war',
                cost: 3,
                color: 'white',
                phase: 'battle',
                img: "img/holy-war.jpg",
                effect: holyWar
            },
            {
                name: 'white victory point',
                cost: 3,
                color: 'white',
                phase: 'day',
                img: "img/white-victory-point.jpg",
                effect: victoryPoint
            },

            {
                name: 'priest of ra',
                cost: 4,
                color: 'white',
                phase: 'day',
                img: "img/priest-of-ra.jpg",
                effect: priestOfRa
            },
            {
                name: 'priest of amon',
                cost: 4,
                color: 'white',
                phase: 'night',
                img: "img/priest-of-amon.jpg",
                effect: priestOfAmon
            },
            {
                name: 'the mummy',
                cost: 4,
                color: 'white',
                phase: 'night',
                img: "img/mummy.jpg",
                effect: theMummy
            },
            {
                name: 'white act of god',
                cost: 4,
                color: 'white',
                phase: 'day',
                img: "img/white-act-of-god.jpg",
                effect: actOfGod
            }


        ];

        /****************************************************
         * Blue power tiles
         * **************************************************/
        factory.blueTiles = [

            {
                name: 'recruiting scribe',
                cost: 1,
                color: 'blue',
                phase: 'day',
                img: "img/recruiting-scribe.jpg",
                effect: recruitingScribe
            },
            {
                name: 'recruiting scribe',
                cost: 1,
                color: 'blue',
                phase: 'day',
                img: "img/recruiting-scribe.jpg",
                effect: recruitingScribe
            },
            {
                name: 'defense',
                cost: 1,
                color: 'blue',
                phase: 'day',
                img: "img/defense.jpg",
                effect: defense
            },
            {
                name: 'defense',
                cost: 1,
                color: 'blue',
                phase: 'day',
                img: "img/defense.jpg",
                effect: defense
            },

            {
                name: 'legion',
                cost: 2,
                color: 'blue',
                phase: 'day',
                img: "img/legion.jpg",
                effect: legion
            },
            {
                name: 'ancestral elephant',
                cost: 2,
                color: 'blue',
                phase: 'night',
                img: "img/elephant.jpg",
                effect: ancestralElephant
            },
            {
                name: 'defensive strategy',
                cost: 2,
                color: 'blue',
                phase: 'day',
                img: "img/defensive-strategy.jpg",
                effect: defensiveStrategy
            },
            {
                name: 'deep desert snake',
                cost: 2,
                color: 'blue',
                phase: 'battle',
                img: "img/snake.jpg",
                effect: deepDesertSnake
            },

            {
                name: 'shield of neath',
                cost: 3,
                color: 'blue',
                phase: 'battle',
                img: "img/shield-of-neath.jpg",
                effect: shieldOfNeath
            },
            {
                name: 'defensive victory',
                cost: 3,
                color: 'blue',
                phase: 'battle',
                img: "img/defensive-victory.jpg",
                effect: defensiveVictory
            },
            {
                name: 'prescience',
                cost: 3,
                color: 'blue',
                phase: 'battle',
                img: "img/prescience.jpg",
                effect: prescience
            },
            {
                name: 'blue victory point',
                cost: 3,
                color: 'blue',
                phase: 'day',
                img: "img/blue-victory-point.jpg",
                effect: victoryPoint
            },

            {
                name: 'reinforcements',
                cost: 4,
                color: 'blue',
                phase: 'night',
                img: "img/reinforcements.jpg",
                effect: reinforcements
            },
            {
                name: 'sphinx',
                cost: 4,
                color: 'blue',
                phase: 'day',
                img: "img/sphinx.jpg",
                effect: sphinx
            },
            {
                name: 'divine will',
                cost: 4,
                color: 'blue',
                phase: 'day',
                img: "img/divine-will.jpg",
                effect: divineWill
            },
            {
                name: 'blue act of god',
                cost: 4,
                color: 'blue',
                phase: 'day',
                img: "img/blue-act-of-god.jpg",
                effect: actOfGod
            }

        ];


        /****************************************************
         * Red power tile functions
         * **************************************************/


        //red power tile functions
        function charge (player) {
            player.attackStrength += 1;
        }

        function stargate (player) {
            player.teleportCost -= 1;

        }

        function godSpeed (player) {
            player.movement += 1;
            player.godSpeedAbility = true;

        }

        function carnage (player) {
            player.damage += 1;

        }

        function offensiveStrategy (player) {
            //ask user to select battle card to replace
            player.message = "Bro, please select battle card to replace!";
            //move all played cards into unplayed card array
            for (var n = 0; n < BattleCardFactory.cards.length; n++) {
                player.unplayedBattleCards[n] = BattleCardFactory.cards[n];
            }
            player.playedBattleCards = [];
            //indicate that player is buying offensive strategy.
            player.isBuyingOffensiveStrategy = true;
        }

        function openGates (player) {
            player.openGatesAbility = true;
        }

        function teleport (player) {
            player.obeliskTeleportAbility = true;

        }

        function royalScarab (player, isBattlePhase) {
            if (isBattlePhase) {
                player.cardStrength += 2;
            }
            if(player.isMoving) {
                player.creatureMovement += 2;
            }
        }

        function bladesOfNeath (player) {
            player.attackStrength += 1;
            player.defenseStrength += 1;
        }

        function divineWound (player) {
            player.divineWoundAbility = true;

        }
        function victoryPoint (player) {
            player.points += 1;
            player.tilePoints += 1;
        }
        function giantScorpion (player, isBattlePhase) {
            if (isBattlePhase) {
                player.cardStrength += 2;
                player.cardDamage += 2;
            }
            if(player.isMoving) {
                player.creatureMovement += 1;
            }
        }
        function initiative (player) {
            player.initiativeAbility = true;

        }
        function phoenix (player, isBattlePhase) {
            if(isBattlePhase) {
                player.cardStrength += 2;
            }
            if(player.isMoving) {
                player.openGatesAbility = true;
                player.creatureMovement += 1;
            }
        }

        function actOfGod (player) {
            player.actOfGodAbility = true;

        }


        /****************************************************
         * White power tile functions
         * **************************************************/
        function priest (player) {
            player.prayerIncrement += 1;
        }

        function priestess (player) {
            player.tileDiscount += 1;
        }

        function slaves (player) {
            player.pyramidDiscount += 1;
        }

        function highPriest (player) {
            player.nightPrayer += 2;
        }

        function crusade (player) {
            player.crusadeAbility = true;
        }

        function divineBoon (player) {
            player.dIBonus += 1;
        }

        function handOfGod (player) {
            player.handOfGodAbility = true;
        }

        function vision (player) {
            player.visionAbility = true;
        }
        function holyWar (player) {
            player.holyWarAbility = true;
        }
        function priestOfRa (player) {
            player.generalDiscount = true;
            player.tileDiscount += 1;
            player.recruitDiscount += 1;
            player.teleportDiscount += 1;

        }

        function priestOfAmon (player) {
            player.nightPrayer += 5;

        }

        function theMummy (player, isBattlePhase) {
            if (isBattlePhase) {
                player.cardStrength += 2;
                player.cardDamage += 2;
            }
            if(player.isMoving) {
                player.creatureMovement += 1;
            }
            player.dIBonus += 1;
        }


        /****************************************************
         * Blue power tile functions
         * **************************************************/


        function recruitingScribe (player) {
            player.recruitDiscount += 2;

        }

        function defense (player) {
            player.defenseStrength += 1;
        }

        function legion (player) {
            player.maxArmy += 2;
        }

        function ancestralElephant (player, isBattlePhase) {
            if (isBattlePhase) {
                player.cardStrength += 1;
                player.cardShield += 1;
            }
            if(player.isMoving) {
                player.creatureMovement += 1;
            }
        }

        function defensiveStrategy (player) {
            //ask user to select battle card to replace
            player.message = "Bro, please select battle card to replace!";
            //move all played cards into unplayed card array
            for (var n = 0; n < BattleCardFactory.cards.length; n++) {
                player.unplayedBattleCards[n] = BattleCardFactory.cards[n];
            }
            player.playedBattleCards = [];
            //indicate that player is buying defensive strategy.
            player.isBuyingDefensiveStrategy = true;
        }

        function deepDesertSnake (player) {
            player.deepDesertSnakeAbility = true;
            player.creatureMovement += 1;

        }
        function shieldOfNeath (player) {
            player.shield += 1;

        }
        function defensiveVictory (player) {
            player.defensiveVictoryAbility = true;
        }
        function prescience (player) {
            player.prescienceAbility = true;

        }

        function reinforcements (player) {
            player.reinforcementsAbility = true;
        }
        
        function sphinx (player, isBattlePhase) {
            if (isBattlePhase) {
                player.cardStrength += 2;
            }
            if (!player.sphinxPointsApplied) {
                player.points += 1;
                player.tilePoints += 1;
                player.sphinxPointsApplied = true;
            }

        }

        function divineWill (player) {
            player.divineWillAbility = true;

        }


        return factory;
    }


    /*********************************************************************
     Battle Cards
     /*********************************************************************/


    BattleCardFactory.$inject = [];
    function BattleCardFactory () {
        var factory = {};
        factory.cards = [
            {
                strength: 4,
                damage: 1,
                shield: 0,
                img: "img/battle41.jpg",
            },
            {
                strength: 1,
                damage: 3,
                shield: 0,
                img: "img/battle13.jpg",
            },
            {
                strength: 3,
                damage: 0,
                shield: 1,
                img: "img/battle31.jpg",
            },
            {
                strength: 2,
                damage: 0,
                shield: 2,
                img: "img/battle22.jpg",
            },
            {
                strength: 3,
                damage: 2,
                shield: 0,
                img: "img/battle32.jpg",
            },

            {
                strength: 2,
                damage: 2,
                shield: 1,
                img: "img/battle221.jpg",
            },




        ]

        factory.offensiveStrategy = {
            strength: 3,
            damage: 3,
            shield: 0,
            img: "img/battle33.jpg"
        };

        factory.defensiveStrategy = {
            strength: 3,
            damage: 0,
            shield: 3,
            img: "img/battle33D.jpg"
        };

        return factory;
    }


    //when tile is bought
    //show battleCards
    //ask user to select battle card to replace
    //set player.isBuyingOffensiveStrategy to true

    //when player clicks card
    //check if player isBuyingOffensiveStrategy


    //remove clicked card
    //add Offensive strategy card
    //set isBuyingOffensiveStrategy to false



    /*********************************************************************
     DI Cards
     /*********************************************************************/
    DICardFactory.$inject = [];
    function DICardFactory () {
        var factory = {};
        
        factory.discardPile = []; 

        factory.cards = [
            {
                name: 'Bloodbath',
                cost: 1,
                phase: 'battle',
                img: "img/bloodbath.jpg",
                effect: bloodBath,
            },
            {
                name: 'Bloodbath',
                cost: 1,
                phase: 'battle',
                img: "img/bloodbath.jpg",
                effect: bloodBath,
            },
            {
                name: 'Bloody Battle',
                cost: 0,
                phase: 'battle',
                img: "img/bloody-battle.jpg",
                effect: bloodyBattle,
            },
            {
                name: 'Bloody Battle',
                cost: 0,
                phase: 'battle',
                img: "img/bloody-battle.jpg",
                effect: bloodyBattle,
            },
            {
                name: 'Bloody Battle',
                cost: 0,
                phase: 'battle',
                img: "img/bloody-battle.jpg",
                effect: bloodyBattle,
            },
            {
                name: 'Bronze Wall',
                cost: 0,
                phase: 'battle',
                img: "img/bronze-wall.jpg",
                effect: bronzeWall,
            },
            {
                name: 'Bronze Wall',
                cost: 0,
                phase: 'battle',
                img: "img/bronze-wall.jpg",
                effect: bronzeWall,
            },
            {
                name: 'Bronze Wall',
                cost: 0,
                phase: 'battle',
                img: "img/bronze-wall.jpg",
                effect: bronzeWall,
            },
            {
                name: 'Enlistment',
                cost: 0,
                phase: 'day',
                img: "img/enlistment.jpg",
                effect: enlistment,
            },
            {
                name: 'Enlistment',
                cost: 0,
                phase: 'day',
                img: "img/enlistment.jpg",
                effect: enlistment,
            },
            {
                name: 'Enlistment',
                cost: 0,
                phase: 'day',
                img: "img/enlistment.jpg",
                effect: enlistment,
            },
            {
                name: 'Enlistment',
                cost: 0,
                phase: 'day',
                img: "img/enlistment.jpg",
                effect: enlistment,
            },
            {
                name: 'Escape',
                cost: 0,
                phase: 'other',
                img: "img/escape.jpg",
                effect: escape,
            },
            {
                name: 'Iron Wall',
                cost: 1,
                phase: 'battle',
                img: "img/iron-wall.jpg",
                effect: ironWall,
            },
            {
                name: 'Iron Wall',
                cost: 1,
                phase: 'battle',
                img: "img/iron-wall.jpg",
                effect: ironWall,
            },
            {
                name: 'Mana Theft',
                cost: 0,
                phase: 'day',
                img: "img/mana-theft.jpg",
                effect: manaTheft,
            },
            {
                name: 'Mana Theft',
                cost: 0,
                phase: 'day',
                img: "img/mana-theft.jpg",
                effect: manaTheft,
            },
            {
                name: 'Open Gates',
                cost: 1,
                phase: 'day',
                img: "img/open-gates.jpg",
                effect: openGates,
            },
            {
                name: 'Prayer',
                cost: 0,
                phase: 'day',
                img: "img/prayer.jpg",
                effect: prayer,
            },
            {
                name: 'Prayer',
                cost: 0,
                phase: 'day',
                img: "img/prayer.jpg",
                effect: prayer,
            },
            {
                name: 'Raining Fire',
                cost: 1,
                phase: 'day',
                img: "img/raining-fire.jpg",
                effect: prayer,
            },
            {
                name: 'Raining Fire',
                cost: 1,
                phase: 'battle',
                img: "img/raining-fire.jpg",
                effect: rainingFire,
            },
            {
                name: 'Raining Fire',
                cost: 1,
                phase: 'day',
                img: "img/raining-fire.jpg",
                effect: rainingFire,
            },
            {
                name: 'Raining Fire',
                cost: 1,
                phase: 'day',
                img: "img/raining-fire.jpg",
                effect: prayer,
            },

            {
                name: 'Teleportation',
                cost: 1,
                phase: 'day',
                img: "img/teleportation.jpg",
                effect: teleportation,
            },
            {
                name: 'Teleportation',
                cost: 1,
                phase: 'day',
                img: "img/teleportation.jpg",
                effect: teleportation,
            },
            {
                name: 'Teleportation',
                cost: 1,
                phase: 'day',
                img: "img/teleportation.jpg",
                effect: teleportation,
            },

            //{
            //    name: 'Veto',
            //    cost: 0,
            //    phase: 'day',
            //    img: "img/veto.jpg",
            //    effect: veto,
            //},
            //{
            //    name: 'Veto',
            //    cost: 0,
            //    phase: 'day',
            //    img: "img/veto.jpg",
            //    effect: veto,
            //},
            {
                name: 'War Fury',
                cost: 1,
                phase: 'battle',
                img: "img/war-fury.jpg",
                effect: warFury,
            },
            {
                name: 'War Fury',
                cost: 1,
                phase: 'battle',
                img: "img/war-fury.jpg",
                effect: warFury,
            },
            {
                name: 'War Fury',
                cost: 1,
                phase: 'battle',
                img: "img/war-fury.jpg",
                effect: warFury,
            },
            {
                name: 'War Rage',
                cost: 0,
                phase: 'battle',
                img: "img/war-rage.jpg",
                effect: warRage,
            },
            {
                name: 'War Rage',
                cost: 0,
                phase: 'battle',
                img: "img/war-rage.jpg",
                effect: warRage,
            },
            {
                name: 'War Rage',
                cost: 0,
                phase: 'battle',
                img: "img/war-rage.jpg",
                effect: warRage,
            },
        ];
        function bloodBath(player) {
            player.cardDamage += 2;
        }

        function bloodyBattle(player) {
            player.dICardDamage += 1;
        }

        function bronzeWall(player) {
            player.cardShield += 1;
        }

        function enlistment(player) {
            if(player.aiControlled) {
                
            }
        }

        function escape(player) {
        }

        function ironWall (player) {
            player.cardShield += 2;
        }

        function manaTheft(player) {

        }

        function openGates(player) {
            player.hasOpenGates = true;
        }

        function prayer(player) {
            player.prayer += 2;
        }

        function rainingFire(player) {
            player.cardStrength += 1;
        }


        function teleportation(player) {
            player.prayer += 1;
        }
        function veto(player) {

        }

        function warFury(player) {
            player.cardStrength += 2;

        }

        function warRage(player) {
            player.cardStrength += 1;
        }
        return factory;
    }







    /*********************************************************************
     Player Three Factory
     /*********************************************************************/

    PlayerThreeFactory.$inject = ['GameFactory'];
    function PlayerThreeFactory(GameFactory) {
        var factory = {};
        factory.test = "test";


        return factory;
    }

    /*********************************************************************
     Player Four Factory
     /*********************************************************************/

    PlayerFourFactory.$inject = ['GameFactory'];
    function PlayerFourFactory(GameFactory) {
        var factory = {};
        factory.test = "test";


        return factory;
    }

    /*********************************************************************
     Player Five Factory
     /*********************************************************************/

    PlayerFiveFactory.$inject = ['GameFactory'];
    function PlayerFiveFactory(GameFactory) {
        var factory = {};
        factory.test = "test";


        return factory;
    }

})();