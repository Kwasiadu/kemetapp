/**
 * The PlayFactory contains most of the game functionality
 */

(function () {

    //'use strict';

    angular.module('kemetApp').factory('PlayFactory', PlayFactory);

    PlayFactory.$inject = ['GameFactory', 'MoveFactory', 'PlayerOneFactory', 'PlayerTwoFactory',
        'PlayerThreeFactory', 'PlayerFourFactory', 'PlayerFiveFactory',
        'TileFactory', 'BattleCardFactory', 'DICardFactory', '$timeout'];
    function PlayFactory(GameFactory, MoveFactory, PlayerOneFactory, PlayerTwoFactory,
                         PlayerThreeFactory, PlayerFourFactory, PlayerFiveFactory,
                         TileFactory, BattleCardFactory, DICardFactory, $timeout) {
        var factory = {};
        factory.turnOrder = ['One', 'Two'];
        factory.whoseTurn = factory.turnOrder[0];
        GameFactory.message = "It's player " + factory.whoseTurn +
            "'s turn.";
        var noOfPlayers = factory.turnOrder.length;
        var color1 = 'transparent';
        var color2 = 'purple';
        var color2m = '#800080';
        var color3 = 'black';
        var color3m = '#000000';
        var color5 = 'red';
        var secondFighterReady = false;
        var winner = "";
        var cities = ['redC', 'whiteC', 'blueC'];
        factory.isBattlePhase = false;
        var didVeto = false;
        factory.firstFighterReady = false;
        factory.playerArray = [];
        factory.getPlayer = getPlayer;
        factory.buyTile = buyTile;
        factory.nightPhase = nightPhase;
        factory.beginGame = beginGame;
        factory.selectArmy = selectArmy;
        factory.applyDICard = applyDICard;
        factory.finishMove = finishMoveAction;
        factory.recruitAction = recruitAction;
        factory.upgradeAction = upgradeAction;
        factory.prayAction = prayAction;
        factory.nextTurn = nextTurn;
        factory.storeLocation = storeLocation;
        factory.armyFn = armyFn;
        factory.actionButton = actionButton;
        factory.upgradePyramid = upgradePyramid;
        factory.readyToFight = readyToFight;
        factory.powerTileFunction = powerTileFunction;
        factory.recruit = recruit;
        factory.storeLocation = storeLocation;
        factory.checkForEndOfRound = checkForEndOfRound;
        factory.getFieldArmyNo = getFieldArmyNo;
        factory.upgradePyramid = upgradePyramid;
        factory.checkSpace = checkSpace;
        factory.finishMoveAction = finishMoveAction;
        factory.getRandomNumber = getRandomNumber;
        factory.checkButtonAvailability = checkButtonAvailability;
        factory.getPlayerNumber = getPlayerNumber;
        factory.getPlayerObj = getPlayerObj;
        factory.toggleAIControl = toggleAIControl;
        factory.showCards = showCards;
        factory.fight = fight;
        factory.finishAction = finishAction;
        factory.checkForEndOfRound = checkForEndOfRound;
        factory.applyCards = applyCards;
        factory.applyBattleCard = applyBattleCard;
        factory.giveDICard = giveDICard;
        factory.getOpponent = getOpponent;
        factory.applyDivineWoundCards = applyDivineWoundCards;
        factory.cleanArray = cleanArray;
        factory.getRetreatLocations = getRetreatLocations;
        factory.getArmyLocations = getArmyLocations;
        factory.getConflictZone = getConflictZone;
        factory.queryArray = queryArray;
        factory.placeSoldier = placeSoldier;
        factory.determineWinner = determineWinner;
        factory.allPlayers = allPlayers;
        factory.resetGame = resetGame;
        factory.getPlayerArmyAtLocation = getPlayerArmyAtLocation;
        factory.isNextTo = isNextTo;
        factory.changeLocation = changeLocation;
        factory.canEnterCity = canEnterCity;
        factory.gameOver = false;
        factory.timeout = false;
        factory.color2 = color2;
        factory.color2m = color2m;
        factory.color3 = color3;
        factory.color3m = color3m;


        factory.isBattlePhase = false;
        factory.destination = "";


        factory.readyForBattle = false;
        var attacker = {};
        var defender = {};
        var currentPlayer = {};




        populatePlayerArray();

        populatePlayerBattleCards();


        /*******************************************************************
         ******************** GENERAL GAME PLAY FUNCTIONS *************************
         *******************************************************************/


        function getTurnOrder() {
            getRanking();
            var ansLegal = false;
            var newTurnOrder = [];
            var chosenPositions = [];
            for (var i = 0; i < factory.turnOrder.length; i++) {
                newTurnOrder[i] = factory.turnOrder[i];
            }
            for (var i = 0; i < factory.playerArray.length - 1; i++) {
                var player = getPlayerAtIndex(i);
                //Set all players turn to false;
                player.isTurn = false;
                while (!ansLegal) {
                    //If its AI
                    if (player.aiControlled) {
                        ans = getAITurnSelection();
                    } else {
                        var ans = prompt("Player " + player.name + ", what" +
                            " position would you like? Please enter a number.");
                    }
                    ans = parseFloat(ans);
                    if (ans > 0 && ans < (factory.turnOrder.length + 1)) {
                        ansLegal = true;
                        chosenPositions.push(ans - 1);
                    }
                }
                newTurnOrder[ans - 1] = player.name;
                player.turnMarker.posx = GameFactory.turnMarker[ans - 1].posx;
                player.turnMarker.posy = GameFactory.turnMarker[ans - 1].posy;
                ansLegal = false;
            }
            var last = factory.turnOrder.length - 1;
            var lastPlayer = getPlayerAtIndex(last);
            for (var i = 0; i < factory.turnOrder.length; i++) {
                if (chosenPositions.indexOf(i) === -1) {
                    newTurnOrder[i] = lastPlayer.name;
                    lastPlayer.turnMarker.posx = GameFactory.turnMarker[i].posx;
                    lastPlayer.turnMarker.posy = GameFactory.turnMarker[i].posy;
                }
            }
            for (var i = 0; i < factory.turnOrder.length; i++) {
                factory.turnOrder[i] = newTurnOrder[i];

            }
            var firstPlayer = getPlayerAtIndex(0);
            firstPlayer.isTurn = true;
            factory.whoseTurn = factory.turnOrder[0];
            GameFactory.message = "It's player " + factory.whoseTurn +
                "'s turn.";
        }

        function getAITurnSelection () {
            var pos = getRandomNumber(factory.playerArray.length) +1;
            return pos;
        }

        function getRanking() {
            setPositioning();

            for (var i = 0; i < factory.turnOrder.length - 1; i++) {
                for (var n = 0; n < factory.turnOrder.length - 1; n++) {
                    var player = getPlayerAtIndex(n);
                    var nextPlayer = getPlayerAtIndex(n + 1);
                    if (nextPlayer.points < player.points) {
                        factory.turnOrder[n] = nextPlayer.name;
                        factory.turnOrder[n + 1] = player.name;
                    }
                    //if game has ended and points are even, check who has
                    // more battle points.
                    //If that is even the game will automatically declare
                    // whoever went earlier a higher ranking.
                    if (factory.gameOver) {
                        if (nextPlayer.points === player.points) {
                            if (nextPlayer.battlePoints < player.battlePoints) {
                                factory.turnOrder[n] = nextPlayer.name;
                                factory.turnOrder[n + 1] = player.name;
                            }
                        }

                    }
                }
            }
        }

        function setPositioning() {
            for (var i = 0; i < factory.playerArray.length; i++) {
                for (var n = 0; n < factory.playerArray.length; n++) {
                    if (factory.playerArray[i].name.indexOf(factory.turnOrder[n]) > -1) {
                        factory.playerArray[i].position = n;
                    }
                }
            }
        }

        function nextTurn(nextPlayer) {
            var player = getPlayer();


            //Determine current player
            var index = getPlayerIndex(player);
            //Complete the current player's turn
            player.isTurn = false;
            //If nextPlayer was not specified, make it the next player in
            // the game's turn
            if (!nextPlayer) {
                //If there is a next player in the array
                if (factory.turnOrder[index + 1]) {
                    //Make it the next player's turn
                    factory.whoseTurn = factory.turnOrder[index + 1];
                } else {
                    //make it the first player's turn
                    factory.whoseTurn = factory.turnOrder[0];
                }
            } else {
                // else, make it the turn of the specified player
                factory.whoseTurn = nextPlayer.name;
            }
            //Begin next player's turn
            player = getPlayer();
            player.isTurn = true;

            // Report whose turn it is.
            GameFactory.message = "Its player " + factory.whoseTurn +
                "'s turn.";
            console.log(factory.whoseTurn)
            //console.info('next Turn');
            //if(nextTurn.caller) {
            //console.info('by ' + nextTurn.caller.name);
            //}
            console.info(factory.whoseTurn + "'s turn")
        }

        function beginGame() {
            for (var i = 0; i < factory.turnOrder.length; i++) {
                //getTurnOrder();

                //Get the player
                var player = getPlayer();
                if(!player.aiControlled) {

                    alert("Hello Player " + player.name);

                    // Give player a DI card
                    giveDICard(player);

                    // pick pyramids to upgrade
                    for (var n = 0; n < GameFactory.noOfFreeUpgrades; n++) {
                        freeUpgrade(player);
                    }

                    // Place initial recruits
                    addInitialRecruits(player);

                }

                factory.nextTurn();
                recallEmptyArmies();

            }
        }




        /*******************************************************************
         ******************** PLAYER INFO FUNCTIONS *************************
         *******************************************************************/


        function getPlayerAtIndex(index) {
            checkParams(this, index);

            for (var i = 0; i < factory.turnOrder.length; i++) {
                if (factory.playerArray[i].name.indexOf(factory.turnOrder[index]) > -1) {
                    return factory.playerArray[i];
                }
            }
            console.error('starting player was not found');
            return PlayerTwoFactory;
        }





        /*******************************************************************
         ******************** UI  FUNCTIONS *************************
         *******************************************************************/


        function checkButtonAvailability(id, finished) {
            checkParams(this, id);
            var player = getPlayer();
            // always return a true value during testing
            if (GameFactory.testing) {
                return true;
            }

            if (!angular.element(id)) {
                console.error('no element');
                return false;
            }

            if (!angular.element(id)[0]) {
                console.error('element has nothing at index zero');

                return false;
            }
            var buttonColor = angular.element(id)[0].style.fill;
            var one = (buttonColor != player.color && buttonColor != player.colorm);
            var two = (buttonColor != color2 && buttonColor != color2m);
            var three = (buttonColor != color3 && buttonColor != color3m);
            //if checking to see if action has been fully used then don't
            // check for if action is still being used
            if (finished) {
                two = true;
            }
            return (one && two && three);
        }

        function toggleAIControl(owner) {
            var player = getPlayerObj(owner);
            player.aiControlled = !player.aiControlled;
        }

        function showCards(owner) {
            var player = getPlayerObj(owner);
            player.showCards = !player.showCards;
        }

        function actionButton(action, owner, id) {
            var pristine, unused;
            var player = getPlayer();
            if (id) {
                pristine = checkButtonAvailability('#' + id);
                unused = checkButtonAvailability('#' + id, true);
            }

            if (!unused && id) {
                return;
            }
            switch (action) {
                case ('finishMove'):
                    finishMoveAction(owner, id);
                    break;
                case ('recruit'):
                    recruitAction(owner, id);
                    break;
                case ('upgrade'):
                    if (pristine) {
                        return;
                    }
                    upgradeAction(owner, id);
                    break;
                case ('pray'):
                    prayAction(owner, id);
                    break;
                case ('finish'):
                    if (pristine && id) {
                        return;
                    }
                    if (player.name != owner) {
                        return;
                    }
                    if (factory.tileButton) {
                        if (player.actOfGodAbilitySelected) {
                            player.actOfGodAbilitySelected = false;
                            player.message = "Act of God action taken, please" +
                                " take regular action";
                            factory.tileButton.css('fill', color3);
                        } else {
                            //nextTurn();
                            factory.tileButton.css('fill', player.color);
                            checkForEndOfRound()
                        }
                    }
                    break;
                default:
                    console.error("Could not resolve action in actionButton");

            }
        }

        function prayAction(owner, id) {
            // get player
            var player = factory.getPlayer();
            if (player.name != owner) {
                player.message = "Chairman, that is not your board";
                return 'error';
            }
            if (player.prayer >= GameFactory.maxPrayer) {
                player.message = "Boss, you have max prayer.";
                return 'error';
            }
            var button = angular.element("#" + id);
            var buttonColor = checkButtonAvailability("#" + id);
            if (!buttonColor) {
                return 'error';
            }
            var result = pray();
            if (player.actOfGodAbilitySelected) {
                player.message = "Act of God action taken, please" +
                    " take regular action";
                player.actOfGodAbilitySelected = false;
                button.css('fill', color3);
            } else {
                button.css('fill', player.color);
                if (!player.aiControlled) {
                    //nextTurn();
                }
            }
            checkForEndOfRound()
            return result;
        }

        function finishMoveAction(owner, id) {
            var player = factory.getPlayer();
            var button = angular.element('#' + id);
            if (button[0] && button[0].style.fill != color2 &&
                button[0].style.fill != color2m) {
                return;
            }
            if (player.name != owner) {
                player.message = "Chairman, that is not your board";
                return;
            }

            var moveRow1 = angular.element('#move' + player.name + 'row1');
            var moveRow2 = angular.element('#move' + player.name + 'row2');
            player.isMoving = false;
            //return movement back to normal, in case it was modified.
            player.creatureMovement = 0;
            player.creatureMovementApplied = false;
            player.movementStarted = false;
            finishMoving();
            if (player.actOfGodAbilitySelected) {
                player.actOfGodAbilitySelected = false;
                button.css('fill', color3);
                player.message = "Act of God action taken, please" +
                    " take regular action";

            } else {
                if (player.divineWillAbilitySelected) {
                    player.divineWillAbilitySelected = false;
                    button.css('fill', player.color);
                    player.message = "Divine will action taken, please" +
                        " take regular action";
                } else {
                    button.css('fill', player.color);
                    if (!player.aiControlled) {
                        //nextTurn();
                    }
                }
            }
            //if any of the move buttons have the unfinished color, set
            // color to unused
            if (moveRow1[0] && moveRow1[0].style.fill === color2 ||
                button[0] && button[0].style.fill === color2m) {
                moveRow1.css('fill', color1);
            }
            if (moveRow2[0] && moveRow2[0].style.fill === color2 ||
                button[0] && button[0].style.fill === color2m) {
                moveRow2.css('fill', color1);
            }

            checkForEndOfRound()
        }

        function recruitAction(owner, id) {
            var player = factory.getPlayer();
            var button = angular.element('#' + id);

            if (button[0] && (button[0].style.fill === color2 ||
                button[0].style.fill === color2m)) {
                return 'error';
            }

            if (player.name != owner) {
                player.message = "Chairman, that is not your board";
                return 'error';
            }

            finishRecruiting();
            if (player.actOfGodAbilitySelected) {
                player.actOfGodAbilitySelected = false;
                player.message = "Act of God action taken, please" +
                    " take regular action";
                button.css('fill', color3);
            } else if (player.divineWillAbilitySelected) {
                player.divineWillAbilitySelected = false;
                button.css('fill', player.color);
                player.message = "Divine will action taken, please" +
                    " take regular action";
            } else {
                button.css('fill', player.color);
                if (!player.aiControlled) {
                    //nextTurn();
                }
            }
            checkForEndOfRound()

        }

        // UpgradeOne function tells the app that the playerOne wants to upgrade. It
// also
// lets the app know when the player has finished upgrading
        function upgradeAction(owner, id) {
            var player = getPlayer();
            var button = angular.element('#' + id);
            if (button[0] && button[0].style.fill != color2
                && button[0].style.fill != color2m) {
                return 'error';
            }

            if (player.name != owner) {
                player.message = "Chairman, that is not your board";
                return;
            }
            finishUpgrading();
            if (player.actOfGodAbilitySelected) {
                button.css('fill', color3);
                player.actOfGodAbilitySelected = false;
            } else {
                button.css('fill', player.color);
                if (!player.aiControlled) {
                    //nextTurn();
                }
                checkForEndOfRound()
            }
        }

        //This function resets data when an action is finished
        function finishAction(actionCost, player, action) {
            checkParams(this, actionCost, player);
            if (actionCost < 0) {
                actionCost = 0;
            }
            player.message = "Action completed";
            player.actionTaken = true;
            if (!player.actOfGodAbilitySelected) {

                player.actionsLeft -= 1;

                player.actionsTaken += 1;
            }
            recordMove(player, action);

            player.prayer -= actionCost;
        }

        function finishRecruiting() {
            var player = getPlayer();
            var action = 'recruit';
            player.message = "Action completed";
            if (!player.actOfGodAbilitySelected && !player.divineWillAbilitySelected) {
                player.actionsLeft -= 1;
                player.actionsTaken += 1;
            }
            player.noOfRecruitments += 1;
            recordMove(player, action);
        }

        //combine all the finish actions into one function
        function finishUpgrading() {
            var player = getPlayer();

            player.message = "Action completed";
            if (!player.actOfGodAbilitySelected) {

                player.actionsLeft -= 1;
                player.actionsTaken += 1;
            }
            var action = 'upgrade ' + player.pyramidColorSelected;
            player.pyramidGeneralDiscountApplied = false;
            player.pyramidColorSelected = "";
            recordMove(player, action);

        }

        function finishMoving() {
            var player = getPlayer();
            player.message = "Action completed";
            if (!player.actOfGodAbilitySelected && !player.divineWillAbilitySelected) {
                player.actionsLeft -= 1;
                player.actionsTaken += 1;
            }
            player.noOfMovementActions += 1;
            var action = 'move';
            recordMove(player, action);

            player.canMove = false;
            player.movementStarted = false;
            if (player.godSpeedAbility) {
                player.movement = GameFactory.movement + 1;
            } else {
                player.movement = GameFactory.movement;
            }
        }

        function powerTileFunction(tile, player) {
            if (tile.name.indexOf('act of god') > -1) {
                if (!player.actOfGodUsed) {
                    player.actOfGodAbilitySelected = true;
                    player.message = "God has granted you an extra turn";
                    player.actOfGodUsed = true;
                } else {
                    player.message = "Boss, you've already used act of God" +
                        " this round!";
                }
            } else if (tile.name === 'divine will') {
                if (!player.divineWillUsed) {
                    player.divineWillAbilitySelected = true;
                    player.message = "By Divine Will, you can take an extra move" +
                        " or recruit action;";
                    player.divineWillUsed = true;
                } else {
                    player.message = "Boss, you've already used divine Will" +
                        " this round!";
                }
            } else if (GameFactory.creatures.indexOf(tile.name) > -1) {
                if (player.creaturesDeployed.indexOf(tile.name) == -1) {

                    player.creatureInHand = true;
                    player.creatureName = tile.name;
                    player.message = "General, double-click which city to place "
                        + tile.name + " in!";
                    return;
                }
                player.message = "Lord of War, the creature is already attached" +
                    " to an army."
            }
        }





        /*******************************************************************
         ******************** MOVEMENT FUNCTIONS *************************
         *******************************************************************/


        // Inspects space and returns the player and the army if any occupying it

        function checkSpace(space) {
            if (!space) {
                console.warn("space parameter missing from checkSpace fn");
                return;
            }
            var spaceInfo;
            for (var n = 0; n < factory.playerArray.length; n++) {
                var player = factory.playerArray[n];
                for (var i = 0; i < player.armies.length; i++) {
                    if (space === player.armies[i].location) {
                        spaceInfo = {
                            player: player,
                            army: player.armies[i]
                        };
                        return spaceInfo;
                    }
                }
            }
        }

        function changeLocation(player, army, origin, destination) {
            checkParams(this, player, army);
            var result;
            destination = destination || factory.destination;
            origin = origin || army.location;
            if (destination) {
                destination = destination || factory.destination;
                // Get information about destination
                var destinationInfo = checkSpace(destination);
                // if there is no army at destination, move army there
                if (!destinationInfo) {

                    result = move(army, origin, destination);
                    // if player's own army is at destination, try to combine armies

                    //Refactor code to get extra points if occupying a city with a pyramid point
                } else if (player === destinationInfo.player) {

                    if (army.creature && destinationInfo.army.creature) {
                        player.message = "You cannot combine two armies with creatures";
                        result = 'error';
                    }
                    // if armies are smaller than max armies combined
                    if (army.size + destinationInfo.army.size <= player.maxArmy) {
                        result = move(army, origin, destination);
                        if (player.moved) {
                            army.size += destinationInfo.army.size;
                            destinationInfo.army.size = 0;
                            destinationInfo.army.location = 'heaven';

                        } else {
                            result = 'error';
                        }
                    } else {
                        player.message = "Armies are too large to be combined";
                        result = 'error';
                    }
                    // If location is occupied by opponent
                } else {
                    // move player to location
                    result = move(army, origin, destination, 'battle');
                    console.log('move result: ' + result)
                    //if movement was successful, start battle.
                    if (result == 'battle') {
                        GameFactory.message = "Battle Begins: Player " + player.name +
                            "'s army " + army.id + " is attacking Player " +
                            destinationInfo.player.name + "'s army " +
                            army.id + ".";
                        battle(destinationInfo, army);
                    } else {
                        result = 'error';
                    }
                }
                if (player.moved) {
                    player.moved = false;
                }
            } else {
                GameFactory.message = "No Destination found";
                result = 'error';
            }
            return result;
        }

        function storeLocation(location) {
            factory.destination = location;
            console.log('location: ' + location)
            var spaces = angular.element('polygon.space');
            spaces.css('fill', color1);
            var selectedSpace = angular.element('polygon[id="' + factory.destination + '"]');
            selectedSpace.css('fill', color5);
        }

        // This function moves an army
        function move(army, origin, destination, phase) {
            checkParams(this, army, origin, destination);
            var result;

            var player = getPlayer();
            if (origin == destination) {
                player.message = "General, we are already here";
                return 'error';
            }
            // If moving into an opponent's city, check for open gate ability or if the army is next to city.
            if (destination.match(/C/) && !isOwnCity(destination)) {
                if (!player.openGatesAbility && !player.hasOpenGates && player.movementStarted) {
                    player.message = "Chairman, you can't enter the city" +
                        " in one turn without open gates ability";
                    return 'error';
                }
            }
            // if player used open gates diCard, reset open gates to false
            if (player.hasOpenGates) {
                player.hasOpenGates = false;
            }
            // if army is moving to an adjacent location
            if (isNextTo(origin, destination)) {
                if (player.movement > 0) {
                    result = moveArmy(army, destination, phase);
                    player.movement--;
                    player.message = player.movement + " movement left";

                } else {
                    player.message = "Master, you've run out of movement";
                    return 'error';
                }
                return result;
            }
            // if player is teleporting...
            if (origin.match(/C/) || (player.obeliskTeleportAbility && hasObelisk(origin))) {
                var actionCost = player.teleportCost - player.generalDiscount;
                if (player.isUsingTeleportationCard) {
                    actionCost = 0;
                }
                //if not enough prayer, end function
                if (!checkAction(actionCost, 'move', player)) {
                    player.message = "Lord of War, you do not have enough prayer!";
                    return 'error';
                }

                if (hasObelisk(destination)) {
                    if (player.isUsingTeleportationCard) {
                        player.isUsingTeleportationCard = false;
                    }
                    result = moveArmy(army, destination, phase);
                    player.prayer -= actionCost;
                    player.message = player.movement + " movement left";
                    return result;
                }
            }

            player.message = "Boss, destination is too far to reach";
            return 'error';
        }





        function armyFn(army, owner) {
            var player = getPlayer();
            var result;
            rainingFire();
            if (result) { return result; }
            isEscaping();
            if(result) { return result; }
            if (player.name != owner) {
                player.message = 'Boss, this is not your army';
                return 'error';
            }
            // Move army

            result = changeLocation(player, army);
            // Finish move
            finishMove();

            return result;


            function rainingFire() {
                if (player.isRainingFire) {
                    if (player.name != owner) {
                        army.size -= 1;
                        // if army is destroyed, remove it
                        if (army.size = 0) {
                            if(army.location.indexOf('temp') > -1) {
                                owner.points -= 1;
                                owner.templeTPoints -=1;
                            }

                            recallEmptyArmies()
                        }
                        player.isRainingFire = false;
                        result = 'success';
                        player.message = "Excellent work boss!"

                    } else {
                        player.message = "You clicked your own army, please" +
                            " click enemy army.";
                        result = 'error';
                    }
                }
            }

            function finishMove () {
                // if move is successful, color css button to indicate player is moving
                if (result == 'success' || result == 'battle') {

                    var moveRow1 = angular.element('#move' + player.name + 'row1');
                    var moveRow2 = angular.element('#move' + player.name + 'row2');
                    //if move any of the move buttons have the unused, set
                    // color to unfinished
                    if (!moveRow1[0].style.fill || moveRow1[0].style.fill === color1) {
                        moveRow1.css('fill', color2);
                    }
                    if (!moveRow2[0].style.fill || moveRow2[0].style.fill === color1) {
                        moveRow2.css('fill', color2);
                    }
                    player.isMoving = true;
                    //If army has creature attached to it, add creature properties
                    if (army.creature) {
                        applyCreature(player, army);
                    }
                    //if creature movement hasn't been applied already, apply it
                    if (!player.creatureMovementApplied) {
                        player.movement += player.creatureMovement;
                        player.creatureMovementApplied = true;
                    }
                }
            }

            // if player is escaping, check if clicked location is empty and
            //  move there and make it the turn of the original player
            function isEscaping() {
                if (player.isEscaping) {
                    var destinationInfo = checkSpace(factory.destination);
                    if (isNextTo(army.location, factory.destination)) {
                        if (!destinationInfo) {
                            result = moveArmy(army, factory.destination);
                            player.isEscaping = false;
                            factory.isBattlePhase = false;
                            if (player.aiControlled) {
                                nextTurn(currentPlayer);
                            }
                            result = 'error';
                        } else {
                            player.message = "Boss, destination is occupied," +
                                " choose another destination or click 'continue'"
                            result = 'error';
                        }
                    } else {
                        player.message = "Boss, destination is too far to reach";
                        result = 'error';
                    }
                    return result;
                }
            }
        }

        function canEnterCity(player, origin, city) {
            // if city belongs to player, return true
            if (isOwnCity(city, player)) {
                return true;
            }
            
            // if city is an opponent's city but player has open gates ability
            // or is right next to the city, return true;
            if (isOpponentCity(city, player)) {
                if (player.hasOpenGates || player.openGatesAbility ||
                    GameFactory.nextTo[origin].indexOf(city) > -1) {
                    return true;
                }
            }
            // otherwise, return false
            return false;
        }

        // This function checks if the destination of the movement action is player's city

        function isOwnCity(city, player) {
            checkParams(this, city);
            player = player || getPlayer();
            if (city.match(/C1/) && player.name === 'One') {
                return true;
            }
            if (city.match(/C2/) && player.name === 'Two') {
                return true;
            }
            if (city.match(/C3/) && player.name === 'Three') {
                return true;
            }
            if (city.match(/C4/) && player.name === 'Four') {
                return true;
            }
            if (city.match(/C5/) && player.name === 'Five') {
                return true;
            }
            return false;
        }
        
        function isOpponentCity(city, player) {
            if (GameFactory.cities.indexOf(city) > -1 ) {
                if(!isOwnCity(city, player)){
                    return true;
                }
            }
            return false;
        }

        // This function will be used to allow points and abilities when player occupies opponents level 4 city
        
        function occupyPyramid(occupier, owner, pyramidColor) {
            if(occupier[pyramidColor + 'PyramidLevel'] < owner[pyramidColor + 'PyramidLevel']) {
                occupier[pyramidColor + 'OriginalPyramidLevel'] = occupier[pyramidColor + 'PyramidLevel'];
                occupier[pyramidColor + 'PyramidLevel'] = owner[pyramidColor + 'PyramidLevel'];
                owner[pyramidColor + 'OriginalPyramidLevel'] = owner[pyramidColor + 'PyramidLevel'];
                owner[pyramidColor + 'PyramidLevel'] = -1;
            }

            if (owner[pyramidColor + 'PyramidLevel'] == 4) {
                occupier.pyramidPoints += 1;
                occupier.points += 1;
                owner.pyramidPoints -= 1;
                owner.points -= 1;
            }
        }

        function leavePyramid(occupier, owner, pyramidColor) {
                occupier[pyramidColor + 'OriginalPyramidLevel'] = 0;
                occupier[pyramidColor + 'PyramidLevel'] = occupier[pyramidColor + 'OriginalPyramidLevel'];
                owner[pyramidColor + 'OriginalPyramidLevel'] = 0;
                owner[pyramidColor + 'PyramidLevel'] = owner[pyramidColor + 'OriginalPyramidLevel'];

            if (owner[pyramidColor + 'PyramidLevel'] == 4) {
                occupier.pyramidPoints -= 1;
                occupier.points -= 1;
                owner.pyramidPoints += 1;
                owner.points += 1;
            }
        }




        // This function moves an army to its new location
        function moveArmy(army, destination, phase) {
            checkParams(this, army, destination);
            var player = getPlayer();
            var newPos = getLocation(destination);
            if (newPos) {
                army.posx = newPos[0];
                army.posy = phase == 'battle' ? newPos[1] + GameFactory.armyOffset : newPos[1];

                //If leaving temple, reduce points by one
                if (army.location.match(/temp/)) {
                    player.points -= 1;
                    player.templeTPoints -= 1;
                }
                army.location = destination;
                //If moving into temple, increase points by one
                if (army.location.match(/temp/)) {
                    player.points += 1;
                    player.templeTPoints += 1;
                }

                //Allows us to control opacity in army directive
                player.moved = true;
                //Allows us to control the open gate ability
                player.movementStarted = true;
                if (phase == 'battle') {
                    return 'battle';
                } else {
                    return 'success';
                }
            } else {
                return 'error';
            }
        }

        // This function returns true if the two locations are next to each other
        function isNextTo(firstSpace, SecondSpace) {
            checkParams(this, firstSpace, SecondSpace);
            if (!GameFactory.nextTo[firstSpace]) {
                console.log('firstSpace error: ' + firstSpace)
                console.log(arguments.callee.caller.name + " function")
                return 0;
            }
            for (var i = 0; i < GameFactory.nextTo[firstSpace].length; i++) {
                if (SecondSpace == GameFactory.nextTo[firstSpace][i]) {
                    return true;
                }
            }
            return false;
        }

        // This function tells if the location has an obelisk. Lets the program know
        // where armies can teleport to.
        function hasObelisk(space) {
            return (space.match(/obel/) || space.match(/temp/));
        }

        //This function adds new recruits to armies


// This function returns the coordinates of an army base when given the name
        // of the location
        function getLocation(zone) {
            checkParams(this, zone);
            switch (zone) {
                case 'redC1':
                    return GameFactory.locations.redC1.primary;
                case 'whiteC1':
                    return GameFactory.locations.whiteC1.primary;
                case 'blueC1':
                    return GameFactory.locations.blueC1.primary;
                case 'redC2':
                    return GameFactory.locations.redC2.primary;
                case 'whiteC2':
                    return GameFactory.locations.whiteC2.primary;
                case 'blueC2':
                    return GameFactory.locations.blueC2.primary;
                case 'buff1':
                    return GameFactory.locations.buff1.primary;
                case 'buff2':
                    return GameFactory.locations.buff2.primary;
                case 'buff3':
                    return GameFactory.locations.buff3.primary;
                case 'buff4':
                    return GameFactory.locations.buff4.primary;
                case 'temp1':
                    return GameFactory.locations.temp1.primary;
                case 'temp2':
                    return GameFactory.locations.temp2.primary;
                case 'temp3':
                    return GameFactory.locations.temp3.primary;
                case 'obel1':
                    return GameFactory.locations.obel1.primary;
                default:
                    factory.message = "Boss, you must click one of" +
                        " the zones inside the board";
                    return;
            }
        }


        /*******************************************************************
         ******************** RECRUITING FUNCTIONS *************************
         *******************************************************************/


        function addInitialRecruits(player) {
            var num = getPlayerNumber(player);

            //alert("Player " + player.name + ", you get "
            // + GameFactory.initialRecruitNo + " free soldiers!");
            for (var n = 0; n < cities.length; n++) {
                if (player.fieldArmyNo < GameFactory.initialRecruitNo) {

                    //var ans = prompt("How many soldiers in " + GameFactory.colors[n] +
                    // " city section?");
                    var ans = 9;
                    if (ans >= 0 && ans <= player.maxArmy) {
                        if (ans > GameFactory.initialRecruitNo - player.fieldArmyNo) {
                            ans = GameFactory.initialRecruitNo - player.fieldArmyNo;
                        }
                        player.fieldArmyNo += parseFloat(ans);
                        player.armies[n].size += parseFloat(ans);
                        player.armies[n].location = cities[n] + num;
                    } else {
                        for (var n = 0; n < cities.length - 1; n++) {
                            player.armies[n].size = 5;
                            player.armies[n].location = cities[n] + num;
                            player.fieldArmyNo = 10;

                        }
                    }

                }
            }
        }


        function recruit(location) {
            var player = getPlayer();

            var result;
            // Check if player is recruiting in his/her own city
            if (location.indexOf(getPlayerNumber(player)) == -1) {
                player.message = "Chairman, that is not your city";
                return 'error';
            }
            //check if retreat button has been used, if so, end function exec.

            var button = angular.element('.button.recruit.' + player.name);
            var buttonColor = checkButtonAvailability('.button.recruit.' + player.name, true);
            if (!buttonColor && !player.creatureInHand) {
                player.message = "Generalissimo, you've already" +
                    " recruited this round!";
                return 'error';
            }
            result = addRecruit(location, player);
            button.css('fill', color2);
            recallEmptyArmies();
            return result;


        }

        function addRecruit(location, player) {
            var army, result;
            var spaceInfo = checkSpace(location);
            // if no army
            // find army in heaven
            // set its location to city
            if (!spaceInfo) {
                army = deployArmy(player);
                army.location = location;
                army.posx = GameFactory.locations[army.location].primary[0];
                army.posy = GameFactory.locations[army.location].primary[1];
            } else {
                // if there is an army in the city
                army = spaceInfo.army;
                //Scan AIs tiles to see if there is an unattached creature;
                checkForUndeployedCreatures(player);
                //If there is creature waiting to be attached to army, attach it
                // and end function
                if (player.creatureInHand) {
                    if (!army.creature) {
                        army.creature = player.creatureName;
                        //add creature to list of deployed creatures
                        player.creaturesDeployed.push(player.creatureName);
                        player.message = player.creatureName + " has been" +
                            " deployed";
                        player.creatureInHand = false;
                        player.creatureName = '';

                        return;

                    } else {
                        player.message = "Army already has a creature attached";
                    }
                }
            }


            // if the army is at max, end function
            if (army.size >= player.maxArmy) {
                player.message = "Boss, your army is full";
                return 'max';
            }
            // add soldier
            return recruitSoldier(army);
        }

        function recruitSoldier(army) {
            var player = getPlayer();
            player.fieldArmyNo = getFieldArmyNo(player);
            if (player.fieldArmyNo >= GameFactory.maxSoldiers) {
                player.message = "Generalissimo, you have no troops left";
                return 'error';
            }
            if (army.size > player.maxArmy) {
                player.message = "Boss, your army is full!";
                return 'max';
            }

            var actionCost = 1;
            if (player.recruitDiscount > 0) {
                actionCost = 0;
                player.recruitDiscount--;
            }

            if (checkAction(actionCost, 'recruit', player)) {
                army.size += 1;
                player.prayer -= actionCost;
                return 'success';
            } else {
                player.message = "function recruit: Recruit no" +
                    " bueno";
                return 'error';
            }


        }

        function deployArmy(player) {
            for (var i = 0; i < player.armies.length; i++) {
                if (player.armies[i].location === 'heaven') {
                    return player.armies[i];
                }
            }
            console.error("no armies found in heaven");
        }

        function checkForUndeployedCreatures(player) {
            checkParams(this, player);
            if (!player.aiControlled) {
                return;
            }
            for (var n = 0; n < GameFactory.colors.length; n++) {

                var tiles = player[GameFactory.colors[n] + 'Tiles'];
                for (var i = 0; i < tiles.length; i++) {

                    //Check if tile is a creature
                    //if it is, check if its not deployed
                    //if so, put creature in hand and waiting
                    if (GameFactory.creatures.indexOf(tiles[i].name) > -1) {
                        if (player.creaturesDeployed.indexOf(tiles[i].name) === -1) {
                            player.creatureInHand = true;
                            player.creatureName = tiles[i].name;
                        }
                    }
                }
            }
        }

        function recallEmptyArmies() {
            for (var i = 0; i < factory.playerArray.length; i++) {
                var player = factory.playerArray[i];
                for (var n = 0; n < player.armies.length; n++) {
                    var army = player.armies[n];
                    if (army.size === 0) {
                        if (army.location.indexOf('temp') > -1) {
                            player.points -= 1;
                            player.templeTPoints -= 1;
                        }
                        army.location = 'heaven';
                    }
                }
            }
        }

        /*******************************************************************
         ******************** PRAY FUNCTIONS *************************
         *******************************************************************/

        function pray() {
            var player = getPlayer();
            var actionCost = 0;
            var result;
            if (checkAction(actionCost, 'pray', player)) {
                player.prayer += player.prayerIncrement;
                player.noOfPrayers += 1;
                var action = 'pray';
                finishAction(actionCost, player, action);
                result = 'success';
            } else {
                result = 'error';
            }
            if (player.prayer > GameFactory.maxPrayer) {
                player.prayer = GameFactory.maxPrayer;
            }
            return result;
        }


        /*******************************************************************
         ******************** UPGRADE FUNCTIONS *************************
         *******************************************************************/


        //This function upgrades the pyramids
        function upgradePyramid(pyramidColor, owner) {
            checkParams(this, pyramidColor, owner);
            var player = getPlayer();
            var result;
            if (!pyramidColor) {
                console.error("No class detected");
                return 'error';
            }
            if (player.name != owner) {
                player.message = "Chairman, this isn't your board";
                return 'error';
            }
            var button = angular.element('.button.upgrade.' + player.name);
            var buttonColor = checkButtonAvailability('.button.upgrade.' + player.name, true);
            if (!buttonColor) {
                player.message = "Master, you've already upgraded a pyramid" +
                    " this turn!";
                return 'error';
            }
            if (player.pyramidColorSelected) {
                if (player.pyramidColorSelected != pyramidColor && !player.aiControlled) {
                    player.message = "Boss, you've already started upgrading" +
                        " the " + player.pyramidColorSelected + " pyramid!";
                    return 'error';
                }
            } else {
                player.pyramidColorSelected = pyramidColor;
            }

            var actionCost = 1;

            if (player.pyramidDiscount > 0) {
                actionCost -= 1;
            }

            if (player[pyramidColor + 'PyramidLevel'] < GameFactory.maxPyramidLevel) {
                actionCost += player[pyramidColor + 'PyramidLevel'];
                if (player.generalDiscount && !player.pyramidGeneralDiscountApplied) {
                    if (actionCost > 0) {
                        actionCost -= 1;
                        player.pyramidGeneralDiscountApplied = true;
                    }
                }
                if (checkAction(actionCost, 'upgradePyramid', player)) {
                    player[pyramidColor + 'PyramidLevel'] += 1;
                    if (actionCost < 0) {
                        actionCost = 0;
                    }
                    player.prayer -= actionCost;
                    button.css('fill', color2);
                    result = 'success';
                    player.hasUpgraded = true;
                } else {
                    result = 'error'
                }
                if (player[pyramidColor + 'PyramidLevel'] == GameFactory.maxPyramidLevel) {
                    player.points += 1;
                    player.pyramidPoints += 1;
                }
            } else {
                result = 'max'
            }
            return result;

        }


        /*******************************************************************
         ******************** BUY TILE FUNCTIONS *************************
         *******************************************************************/

        function buyTile(tile, index) {
            checkParams(this, tile, index);
            var player = getPlayer();
            var result;
            if (tileAlreadyBought(tile, player)) {
                if (!player.aiControlled) {
                    player.message = "Boss, you've already purchased a " + tile.name + " tile!";
                }
                return 'error';
            }

            if (!tileAvailable(tile)) {
                return 'error'
            }

            if (tile.name.indexOf('victory point') > -1) {

                if (player.victoryPointBought) {
                    if (!player.aiControlled) {
                        player.message = "Boss, you can buy only one victory point" +
                            " tile.";
                    }
                    return 'error';
                }
            }

            if (tile.name.indexOf('act of god') > -1) {
                if (player.actOfGodBought) {
                    if (!player.aiControlled) {
                        player.message = "Boss, you can buy only one act of God" +
                            " tile.";
                    }
                    return 'error';
                }
            }

            if (tile.name.indexOf('strategy') > -1) {

            }
            var button = angular.element('.button.' + player.name + '.' + tile.color);
            var buttonColor = checkButtonAvailability('.button.' + player.name + '.' + tile.color);
            if (!buttonColor) {
                if (!player.aiControlled) {
                    player.message = "Boss, you've already purchased a "
                        + tile.color + " this round!";
                }
                return 'error';
            }
            result = buyColorTile(tile, index, player, tile.color);

            if (player.tileBought) {
                // if its an act of God card
                if (tile.name.indexOf('act of god') > -1) {
                    player.actOfGodBought = true;
                } else if (tile.name.indexOf('victory point') > -1) {
                    player.victoryPointBought = true;
                }
                //if player is using act of God
                if (player.actOfGodAbilitySelected) {
                    factory.tileButton.css('fill', color3);
                    player.message = "Act of God action taken, please" +
                        " take regular action";
                    player.actOfGodAbilitySelected = false;
                } else {
                    factory.tileButton.css('fill', player.color);
                    checkForEndOfRound()
                }

                player.tileBought = false;
            }
            
            return result;

        }


        function buyColorTile(tile, index, player, color) {
            checkParams(this, tile, index, player);

            var actionCost = tile.cost - player.tileDiscount - player.generalDiscount;

            if (tile.cost <= player[color + 'PyramidLevel']) {
                var action = 'buy' + toTitleCase(color) + 'Tile';
                if (checkAction(actionCost, action, player)) {
                    moveArrayElement(TileFactory[color + 'Tiles'], player[color + 'Tiles'], tile)
                    factory.tileButton = angular.element('.button.' + color + '.' + player.name);
                    finishAction(actionCost, player, action);
                    player.tileBought = true;
                    player[color + 'TileBought'] = true;
                    tile.effect(player);
                    return 'success';
                } else {
                    return 'error'
                }
            } else {
                player.message = "Please upgrade your " + color + " pyramid to" +
                    " level " + tile.cost + " before buying this tile";
            }
            return 'error';

        }


        function tileAlreadyBought(tile, player) {
            for (var i = 0; i < GameFactory.colors.length; i++) {
                var tileArrayName = GameFactory.colors[i] + 'Tiles';
                for (var n = 0; n < player[tileArrayName].length; n++) {
                    if (tile.name == player[tileArrayName][n].name) {
                        return true;
                    }
                }
            }
            return false;
        }


        function tileAvailable(tile) {
            for (var i = 0; i < GameFactory.colors.length; i++) {
                var tileArrayName = GameFactory.colors[i] + 'Tiles';
                for (var n = 0; n < TileFactory[tileArrayName].length; n++) {
                    if (tile == TileFactory[tileArrayName][n]) {
                        return true;
                    }
                }
            }
            return false;
        }

        /*******************************************************************
         ******************** BATTLE  FUNCTIONS *************************
         *******************************************************************/

        // This function allows the player to sacrifice its troops
        function win(winner, army, didLoserRetreat, loserArmy, loser) {
            checkParams(this, winner, army, didLoserRetreat, loserArmy);
            // If loser wants to retreat and there is a retreat location available
            // ask winner to enter number corresponding to retreat space
            if (didLoserRetreat) {

                var message = '';
                var location = '';
                var retLoc;
                if(loserArmy.location !=  'heaven') {
                    retLoc = getRetreatLocations(loserArmy.location);
                }

                if (retLoc && retLoc.length) {
                    if (winner.aiControlled) {
                        moveArmy(loserArmy, winner.retreatLocation);
                    } else {
                        message = '\nWhich space should loser retreat to?\nPlease enter number!';
                        for (var i = 0; i < retLoc.length; i++) {
                            var line = '\n' + (i + 1).toString() + '. ' + retLoc[i];
                            message += line;
                        }
                        ans = prompt(message);
                        for (var i = 0; i < retLoc.length; i++) {
                            if (ans == (i + 1).toString()) {
                                location = retLoc[i];
                                break;
                            }
                        }
                        if (location) {
                            moveArmy(loserArmy, location)
                        } else {
                            alert('Sir, I did not catch your answer! Please enter no');
                            win(winner, army, didLoserRetreat, loserArmy, loser);
                        }

                    }
                }

            }


            if (winner.aiControlled) {
                winner.postBattleChoice = getPostBattleChoice(army);
                var ans = winner.postBattleChoice;
            } else {
                var ans = confirm("Player " + winner.name +
                    ", your army was victorious!\nDo you want to sacrifice your soldiers");
            }

            if (ans) {
                if (army.location.indexOf('temp') > -1) {
                    winner.points -= 1;
                    winner.templeTPoints -= 1;

                }
                army.location = 'heaven';

                winner.prayer += army.size;
                winner.message = "You sacrificed " + army.size +
                    " soldiers for " + army.size + " prayer";

                army.size = 0;
                army.creature = '';
            }
            if (winner.holyWarAbility) {
                winner.prayer += 4;
            }
            // console.info('Player ' + winner.name + ' won! \nstrength: ' + winner.strength +
            //     ' damage: ' + winner.damage + ' shield: ' + winner.shield +
            //     '\nPlayer ' + loser.name + ' lost! \nstrength: ' + loser.strength +
            //     ' damage: ' + loser.damage + ' shield: ' + loser.shield);

        }

        // This function allows the player to sacrifice or signal a retreat
        function lose(loser, army) {
            var ans;
            checkParams(this, loser, army);
            if (loser.aiControlled) {

                loser.postBattleChoice = getPostBattleChoice(army);
                ans = loser.postBattleChoice;
            } else {
                ans = confirm("Player " + loser.name +
                    ", your army was defeated!\nDo you want to sacrifice your soldiers?")
            }

            if (ans) {
                if (army.location.indexOf('temp') > -1) {
                    loser.points -= 1;
                    loser.templeTPoints -= 1;

                }
                army.location = 'heaven';
                loser.prayer += army.size;
                loser.message = "You sacrificed " + army.size +
                    " soldiers for " + army.size + " prayer";
                army.size = 0;
                army.creature = '';
                return false;
            } else {
                return true;
            }
        }


        function resolveBattle(attacker, defender) {
            checkParams(this, attacker, defender);
            attacker.attacksInitiated += 1;
            // console.info('Player ' + attacker.name + ' attacks player ' +
            //     defender.name + ' at ' + getConflictZone())

            //change these to temporary variables
            attacker.totalDamage = attacker.damage - defender.shield;
            if (attacker.totalDamage < 0) {
                attacker.totalDamage = 0;
            }
            defender.totalDamage = defender.damage - attacker.shield;
            if (defender.totalDamage < 0) {
                defender.totalDamage = 0;
            }
            if (attacker.crusadeAbility) {
                attacker.prayer += attacker.totalDamage * 2;
            } else if (defender.crusadeAbility) {
                defender.prayer += defender.totalDamage * 2;
            }
            attacker.strength += attacker.army.size + attacker.attackStrength;


            defender.strength += defender.army.size + defender.defenseStrength;

            // if (attacker.prescienceAbility) {
            //     nextTurn();
            // }
            // if (attacker.divineWoundAbility ) {
            //     var ans = confirm("Would you like to use your divine wound" +
            //         " ability?");
            //     if (ans) {
            //         attacker.isSelectingDivineWoundCards = true;
            //         attacker.message = "Please select cards for Divine" +
            //             " Wound" +
            //             " and click ready when finished";
            //     }
            // } else if (defender.divineWoundAbility) {
            //     var ans = confirm("Would you like to use your divine wound" +
            //         " ability?");
            //     if (ans) {
            //         defender.isSelectingDivineWoundCards = true;
            //         defender.message = "Please select cards for Divine Wound" +
            //             " and click ready when finished";
            //
            //     }
            // }
            calculateWinner(attacker, defender)


        }


        function applyDivineWound(player) {
            checkParams(this, player);

            player.strength += player.dWCardStrength;
            player.divineWoundCardsSelected = false;
            calculateWinner(attacker, defender);


        }


        function calculateWinner(attacker, defender) {
            //If attacker wins
            if (attacker.strength > defender.strength) {

                if (defender.totalDamage <= attacker.army.size) {
                    attacker.army.size -= defender.totalDamage;
                } else {
                    attacker.army.size = 0;
                    attacker.army.creature = '';

                }
                if (attacker.totalDamage <= defender.army.size) {
                    defender.army.size -= attacker.totalDamage;
                } else {
                    defender.army.size = 0;
                    defender.army.creature = '';

                }

                if (attacker.army.size > 0) {
                    attacker.points += 1;
                    attacker.battlePoints += 1;
                }

                defender.retreat = lose(defender, defender.army);
                win(attacker, attacker.army, defender.retreat, defender.army, defender);
                if (!defender.retreat) {
                    defender.prayer += defender.army.size;
                    defender.army.size = 0;
                    defender.army.creature = '';

                }


                //if defender wins
            } else {

                if (defender.totalDamage <= attacker.army.size) {
                    attacker.army.size -= defender.totalDamage;
                } else {
                    attacker.army.size = 0;
                    attacker.army.creature = '';

                }
                if (attacker.totalDamage <= defender.army.size) {
                    defender.army.size -= attacker.totalDamage;
                } else {
                    defender.army.size = 0;
                    defender.army.creature = '';

                }
                if (defender.defensiveVictoryAbility && defender.army.size > 0) {
                    defender.points += 1;
                    defender.battlePoints += 1;
                }
                attacker.retreat = lose(attacker, attacker.army);
                win(defender, defender.army, attacker.retreat, attacker.army, attacker);
                if (!attacker.retreat) {
                    attacker.prayer += attacker.army.size;
                    attacker.army.size = 0;
                    attacker.army.creature = '';
                }
            }
            recallEmptyArmies();
            reset(attacker, defender);
        }


        function reset(attacker, defender) {
            checkParams(this, attacker, defender);

            attacker.damage -= attacker.cardDamage;
            attacker.shield -= attacker.cardShield;
            defender.damage -= defender.cardDamage;
            defender.shield -= defender.cardShield;
            attacker.strength = 0;
            defender.strength = 0;
            attacker.cardStrength = 0;
            attacker.dWCardStrength = 0;
            attacker.cardDamage = 0;
            attacker.cardShield = 0;
            defender.cardStrength = 0;
            defender.dWCardStrength = 0;
            defender.cardDamage = 0;
            defender.cardShield = 0;
            attacker.fightCards = [];
            defender.fightCards = [];
            factory.isBattlePhase = false;
            attacker.cardDiscarded = false;
            defender.cardDiscarded = false;
            attacker.isSelectingDivineWoundCards = false;
            defender.isSelectingDivineWoundCards = false;
            attacker.divineWoundCardsSelected = false;
            defender.divineWoundCardsSelected = false;
            attacker.isAttacking = false;
            defender.isAttacking = false;
            attacker.showCards = false;
            defender.showCards = false;
            attacker.postBattleChoice = '';
            defender.postBattleChoice = '';
            limitPrayer(attacker);
            limitPrayer(defender);
            factory.firstFighterReady = false;
            secondFighterReady = false;
            factory.readyForBattle = false;
        }


        function selectArmy(army) {
            var armies = angular.element("g.army");
            var player = factory.getPlayer();

            //Check if army is belongs to player
            if (army.id.indexOf(player.name) > -1) {

                //return other armies to normal
                armies.css('opacity', '1');
                //if so, highlight army
                angular.element('g[id="' + army.id + '"]').css('stroke', 'black')
                    .css('opacity', '0.5');
            }
        }


        function readyToFight() {
            var player = getPlayer();

            if (player.isEscaping) {
                player.isEscaping = false;
                nextTurn(currentPlayer);
                player.message = "Escape unsuccessful :-(";
                return;
            }


            if (player.isSelectingVisionDICard) {
                player.isSelectingVisionDICard = false;
                for (var i = factory.countStopped + 1; i < factory.playerArray.length; i++) {
                    takeNightPhaseActions(factory.playerArray[i]);
                }
                player.message = "Vision card selected!";
                if (factory.gameOver) {
                    console.error(factory.gameOver);
                    determineWinner(winner);
                } else {
                    GameFactory.message = "Next round begins!";
                    GameFactory.round += 1;
                    getTurnOrder();
                }
                GameFactory.isNightPhase = false;
            }
            else if (attacker.isSelectingDivineWoundCards) {
                if (factory.isBattlePhase) {
                    applyDivineWound(attacker)
                }
            } else if (defender.isSelectingDivineWoundCards) {
                if (factory.isBattlePhase) {
                    applyDivineWound(defender)
                }
            } else if (!factory.firstFighterReady) {
                if (player.cardDiscarded) {
                    factory.firstFighterReady = true;
                    player.message = "";
                    nextTurn()

                } else {
                    player.message = "Darling, please discard battle card"
                }
            } else {

                if (player.cardDiscarded) {
                    secondFighterReady = true;
                    if (!player.aiControlled) {
                        nextTurn();
                    }
                    if (factory.isBattlePhase) {
                        applyCards(attacker, defender);
                    }
                } else {
                    player.message = "Darling, please discard battle card"
                }
            }
        }

        function getFieldArmyNo(player) {
            var num = 0;
            for (var i = 0; i < player.armies.length; i++) {
                if (player.armies[i].location != 'heaven') {
                    num += player.armies[i].size;
                }
            }
            return num;
        }


        function getOpponent(player) {
            if (!player) {
                var player = getPlayer();
            }

            if (player.name == 'One') {
                return PlayerTwoFactory;
            } else {
                return PlayerOneFactory;
            }
        }


        function applyBattleCard(card, index, owner) {
            checkParams(this, card, index, owner);

            var player = getPlayer();


            //check if player is buying Offensive Strategy

            if (player.name != owner && !player.aiControlled) {
                player.message = "Master, this isn't your card!";
                return;
            }

            if (player.aiControlled) {
                player = getPlayerObj(owner);
            }
            if (factory.isBattlePhase) {
                if (player.cardDiscarded) {
                    return;
                }
                if (!player.battleCardApplied) {
                    player.cardStrength += card.strength;
                    player.cardDamage += card.damage;
                    player.cardShield += card.shield;
                    player.battleCardApplied = true;
                    player.fightCards.push(card);
                    player.battleCardSelected = card;
                    player.message = "Please select card to discard";
                } else {

                    player.battleCardApplied = false;
                    player.cardDiscarded = true;
                    player.message = "Card discarded!"

                }
                moveArrayElement(player.unplayedBattleCards, player.playedBattleCards, card);

                if (!player.unplayedBattleCards.length) {
                    for (var n = 0; n < BattleCardFactory.cards.length; n++) {
                        player.unplayedBattleCards[n] = BattleCardFactory.cards[n];
                    }
                    player.playedBattleCards = [];
                }
            } else if (player.isBuyingOffensiveStrategy) {
                //remove clicked card
                //add Offensive strategy card

                player.unplayedBattleCards.splice(index, 1);
                player.unplayedBattleCards.push(BattleCardFactory.offensiveStrategy);
                //set isBuyingOffensiveStrategy to false
                player.isBuyingOffensiveStrategy = false;
            } else if (player.isBuyingDefensiveStrategy) {
                //remove clicked card
                player.unplayedBattleCards.splice(index, 1);

                //add defensive strategy card
                player.unplayedBattleCards.push(BattleCardFactory.defensiveStrategy);

                //set isBuyingDefensiveStrategy to false
                player.isBuyingDefensiveStrategy = false;

            }
        }


        function battle(destinationInfo, army) {
            checkParams(this, destinationInfo, army);
            factory.isBattlePhase = true;
            defender = destinationInfo.player;
            attacker = getPlayer();

            // if (attacker.prescienceAbility && !attacker.aiControlled) {
            //     nextTurn();
            // }

            attacker.army = army;
            defender.army = destinationInfo.army;
            if (attacker.army.creature && defender.army.creature != 'deep' +
                ' desert snake') {
                applyCreature(attacker, attacker.army);
            }
            if (defender.army.creature && attacker.army.creature != 'deep' +
                ' desert snake') {
                applyCreature(defender, defender.army);
            }

            //Check if snake is attached to any army
            //Give a chance for defender to escape

            if (attacker.initiativeAbility) {
                defender.army.size -= 2;
            }
            //Prepare combatants for battle
            if (!(attacker.aiControlled && defender.aiControlled)) {
                battlePrep(attacker, defender);
                return 'success';
            } else {
                return 'battle';
            }
        }


        function applyCreature(player, army) {
            checkParams(this, player, army);
            //Get function of creature
            for (var i = 0; i < player.redTiles.length; i++) {
                if (army.creature === player.redTiles[i].name) {
                    player.redTiles[i].effect(player, factory.isBattlePhase);
                    return;
                }
            }

            for (var i = 0; i < player.whiteTiles.length; i++) {
                if (army.creature === player.whiteTiles[i].name) {
                    player.whiteTiles[i].effect(player, factory.isBattlePhase);
                    return;
                }
            }
            for (var i = 0; i < player.blueTiles.length; i++) {
                if (army.creature === player.blueTiles[i].name) {
                    player.blueTiles[i].effect(player, factory.isBattlePhase);
                    return;
                }
            }


        }

        function fight(attacker, defender) {
            if (factory.readyForBattle) {
                resolveBattle(attacker, defender);
            }

        }

        // This function allows the player to take pre battle actions like
        // playing battle cards and DI cards
        function battlePrep(attacker, defender) {
            checkParams(this, attacker, defender);
            var player = getPlayer();
            var opponent = getOpponent();
            player.showCards = true;
            opponent.showCards = true;

            if (factory.firstFighterReady) {
                attacker.message = "Waiting for player " + attacker.name
                    + " to select cards!"
            } else if (attacker.cardDiscarded) {
                attacker.message = 'Card discarded, please click ready' +
                    ' after selecting di cards!';
            } else if (attacker.battleCardApplied) {
                attacker.message = "Please discard a card!"
            } else if (attacker.prescienceAbility && factory.firstFighterReady) {
                attacker.message = "Opponent selected a battleCard with" +
                    " strength: " + defender.battleCardSelected.strength
                    + ", damage: " + defender.battleCardSelected.damage
                    + " and shield: " + defender.battleCardSelected.shield +
                    ". Please select your cards!"

            } else if (attacker.prescienceAbility) {
                attacker.message = "Waiting for player " + defender.name
                    + " to select cards!"
            } else if (defender.prescienceAbility) {
                attacker.message = "Opponent has prescience ability, please" +
                    " select cards!"
            } else {
                attacker.message = "Please select cards!"
            }


            if (attacker.prescienceAbility && factory.firstFighterReady) {
                defender.message = "Waiting for player " + attacker.name
                    + " to select cards!"
            } else if (defender.cardDiscarded) {
                defender.message = 'Card discarded, please click ready' +
                    ' after selecting di cards!';
            } else if (defender.battleCardApplied) {
                defender.message = "Please discard a card!"
            } else if (defender.prescienceAbility && factory.firstFighterReady) {
                defender.message = "Opponent selected a battleCard with" +
                    " strength: " + attacker.battleCardSelected.strength
                    + ", damage: " + attacker.battleCardSelected.damage
                    + " and shield: " + attacker.battleCardSelected.shield +
                    ". Please select your cards!"
            } else if (attacker.prescienceAbility && !player.battleCardApplied) {
                defender.message = "Opponent has prescience ability, please" +
                    " select cards!"
            } else if (factory.firstFighterReady) {
                defender.message = "Please select cards!"
            } else {
                defender.message = "Waiting for player " + attacker.name
                    + " to select cards!"
            }


            if ((factory.firstFighterReady && secondFighterReady)) {

            }
        }

        function applyCards(attacker, defender) {
            checkParams(this, attacker, defender);
            attacker.strength = 0;
            defender.strength = 0;


            attacker.strength += attacker.cardStrength;
            attacker.damage += attacker.cardDamage;
            attacker.shield += attacker.cardShield;


            defender.strength += defender.cardStrength;
            defender.damage += defender.cardDamage;
            defender.shield += defender.cardShield;

            if (attacker.army && defender.army) {
                factory.readyForBattle = true;
                fight(attacker, defender);
            }

        }

        function getRetreatLocations(armyLocation) {
            checkParams(this, armyLocation);
            var locations = [];
            if (armyLocation == 'heaven') {
                console.log(arguments.callee.caller.name + ' has passed heaven in ' + this.name)
            }
            // figure out why armyLocation is not always found in getConflictZone even though aiFight has been called
            if (armyLocation) {
                for (var key in GameFactory.locations) {
                    var loc = GameFactory.locations[key].name;
                    if (isNextTo(armyLocation, loc)) {
                        if (!checkSpace(loc)) {
                            locations.push(loc);
                        }
                    }
                }
            }
            return locations;
        }

        function getPostBattleChoice (playerArmy) {
            if (playerArmy < 5) {
                return 'sacrifice';
            }
        }

        function getConflictZone() {
            var locations = getAllOccupiedLocations();
            var uniq = locations
                .map(function (location) {
                    return {count: 1, location: location}
                }).reduce(function (a, b) {
                    a[b.location] = (a[b.location] || 0) + b.count;
                    return a
                }, {});
            var cZ = Object.keys(uniq).filter(function (a) {
                return (uniq[a] > 1)
            });
            if (cZ.length) {
                return cZ[0]
            } else {
                // console.log(locations)
                // console.log(uniq)
                // console.log(cZ)
                // console.error('no conflict zone found')
            }
            
        }

        function getAllOccupiedLocations() {
            var locations = [];
            for (var i = 0; i < factory.playerArray.length; i++) {
                var armyLocations = getArmyLocations(factory.playerArray[i]);
                for (var n = 0; n < armyLocations.length; n++) {
                    locations.push(armyLocations[n]);
                }
            }
            return locations;
        }

        function getArmyLocations(player) {
            var locations = [];
            var spaces = getAllLocations();
            //if  armies, if so, add them to locations
            for (var n = 0; n < spaces.length; n++) {
                var result = queryArray(player.armies, spaces[n], 'location', 'location');
                if (result) {
                    locations.push(result);
                }
            }
            return locations;
        }

        function getPlayerArmyAtLocation(player, location) {
            checkParams(this, player, location);
            var result = queryArray(player.armies, location, 'location');
            return result;
        }




        /*******************************************************************
         ******************** DI CARD FUNCTIONS *************************
         *******************************************************************/

        function applyDICard(card, index, owner) {
            var player = getPlayer();
            if (player.name != owner && !player.aiControlled) {
                return;
            }

            if (player.aiControlled) {
                player = getPlayerObj(owner);
            }

            if (player.visionAbility && !player.visionDICardSelected) {
                applyVision(player, card);
                return;
            }
            if (player.isSelectingDivineWoundCards && player.aiControlled) {
                applyDivineWoundCards(player, card);
                return;
            }


            continueApplyingDICard(player, card, index, owner);


        }

        function continueApplyingDICard(player, card, index, owner) {
            if(!card) {
                console.error('There is no card in continueApplyingDICard function');
                return;
            }
            if (!checkCardCost(player, card)) {
                return;
            }
            if (didVeto) {
                player.prayer -= card.cost;
                player.message = "Your card was vetoed";
                returnCardToDeck(player, card, index);
                didVeto = false;
                return;
            }
            if (card.name == 'Escape') {
                if (player.isTurn) {
                    nextTurn();
                    player = getPlayer();
                    nextTurn();
                    applyEscape(player, card, index);
                    return;
                } else {
                    player.message = "You cant play this card on your turn";
                }
            }
            if (player.name != owner && !player.aiControlled) {
                player.message = "G, This is not your card";
            }
            if (factory.isBattlePhase && card.phase === 'day') {
                player.message = "Boss, please play this card" +
                    " during the day!"
            }
            if (card.name === 'Mana Theft') {
                applyManaTheft(player, card, index);

            } else if (card.name === 'Enlistment') {
                enlistment(player, card, index);

            } else if (card.name === 'Teleportation') {
                if (!checkCardCost(player, card)) {
                    return;
                }
                player.prayer -= card.cost;
                applyTeleportation(player, card, index);

            } else if (card.name === 'Raining Fire') {
                if (!checkCardCost(player, card, card)) {
                    return;
                }
                player.prayer -= card.cost;
                applyRainingFire(player, card, index);

            } else if (card.phase === 'day') {
                if (!checkCardCost(player, card)) {
                    return;
                }
                player.prayer -= card.cost;
                dayDICard(player, card, index)

            } else if (card.phase === 'battle') {
                if (!checkCardCost(player, card)) {
                    return;
                }
                player.prayer -= card.cost;
                battleDICard(player, card, index);

            } else {
                console.error('card phase was not recognized: ' + card.phase);
            }


        }

        function dayDICard(player, card, index) {
            card.effect(player);
            returnCardToDeck(player, card, index);
        }

        function battleDICard(player, card, index) {
            if (factory.isBattlePhase) {
                card.effect(player);
                player.fightCards.push(card);

                returnCardToDeck(player, card, index);

            } else {
                player.message = "Boss, please play this card during battle!"
            }
        }

        function checkCardCost(player, card) {
            return (player.prayer > 0 || card.cost == 0 || player.generalDiscount);
        }

        function returnCardToDeck(player, card) {
            moveArrayElement(player.dICards, DICardFactory.discardPile, card);
        }

        function enlistment(player, card, index) {
            returnCardToDeck(player, card, index);
            // refactor code to make location selection optimal
            var location = getArmyLocations(player);
            addFreeRecruits(player);
            addFreeRecruits(player);
        }

        function applyManaTheft(player, card, index) {
            returnCardToDeck(player, card, index);

            for (var i = 0; i < factory.playerArray.length; i++) {
                if (factory.playerArray[i].name === player.name) {
                    factory.playerArray[i].prayer += 1;
                } else {
                    if (factory.playerArray[i].prayer > 0) {
                        factory.playerArray[i].prayer -= 1;
                    }
                }
            }
        }

        function applyTeleportation(player, card, index) {
            returnCardToDeck(player, card, index);
            player.isUsingTeleportationCard = true;
            player.message = "You can now teleport once for free";
        }

        function applyRainingFire(player, card, index) {
            returnCardToDeck(player, card, index);
            player.isRainingFire = true;
            if (player.aiControlled) {
                killOpponentSoldier();
            } else {
                player.message = "Please click an army to attack";

            }
        }

        function killOpponentSoldier() {
            var opponent = getOpponent();
            for (var i = 0; i < opponent.armies.length; i++) {
                if (opponent.armies[i].location != 'heaven') {
                    opponent.armies[i].size -= 1;
                    return;
                }
            }
        }


        function applyVeto(player, card, index) {
            if (player.isTurn) {
                didVeto = true;
                returnCardToDeck(player, card, index);
            } else {
                player.message = "You cant play veto card on your turn"
            }
        }

//Get current player and store it, make it the turn of the player who played
// the card. Ask user to click the army to be moved which will call the army
// function, move the enemy's army and return it to the current player's turn
        function applyEscape(cardPlayer, card, index) {
            currentPlayer = getPlayer();
            returnCardToDeck(cardPlayer, card, index);
            nextTurn(cardPlayer);
            cardPlayer.message = "Click which space to retreat to!";
            cardPlayer.isEscaping = true;
        }

        // If player has vision but hasn't selected is DI card yet,
        // allow him to select his card,
        // remove selected card from di cards array and store it,
        // return last 5 vision cards to DI deck
        // return selected card back to DI cards array
        // indicate player has selected his card
        function applyVision(player, card) {
            if (!player.aiControlled) {
                player.fightCards.push(card);
                var index1 = player.dICards.length - 4;
                for (var i = index1; i < player.dICards.length; i++) {
                    DICardFactory.cards.push(player.dICards[i]);
                }
                player.dICards.splice(index1, 4);
                player.dICards.push(player.fightCards[0]);
                player.fightCards = [];
                player.message = "Please click the ready button after" +
                    " selecting card.";
                player.visionCardSelected = true;
                player.isSelectingVisionDICard = true;
            }

        }

        function applyDivineWoundCards(player, card, index) {
            for (var i = 0; i < player.dICards.length; i++) {
                if (player.dICards[i] == undefined) {
                    console.error('pf dI card is undefined at index: ' + i);
                    player.dICards = cleanArray(player.dICards)
                }
            }
            player.dWCardStrength += 1;
            var dWCard = card;
            dWCard.img = "img/di.jpg";
            moveArrayElement(player.dICards, player.fightCards, dWCard);

        }

        function giveDICard(player) {
            var noOfCards = DICardFactory.cards.length;
            var incomingCards = player.dIBonus + 1;

            // if player.visionAbility, push 4 more cards
            if (player.visionAbility && !player.aiControlled) {
                incomingCards += 4;
                player.message = "Please click one of the last 5" +
                    " cards to keep it."
            }

            if (noOfCards >= incomingCards) {
                for (var i = 0; i < incomingCards; i++) {
                    moveArrayElement(DICardFactory.cards, player.dICards, DICardFactory.cards[i])
                    // player.dICards.push(DICardFactory.cards[i]);
                    // DICardFactory.cards.splice(i, 1);
                }
            } else {
                console.warn('reshuffling di cards');
                reshuffleDICards();
            }
            for (var i = 0; i < player.dICards.length; i++) {
                if (player.dICards[i] == undefined) {
                    console.error('dI cards is undefined at index: ' + i);
                    player.dICards = cleanArray(player.dICards)
                }
            }


        }

        function reshuffleDICards() {
            for (var i = 0; i < DICardFactory.discardPile.length; i++) {
                DICardFactory.cards.push(DICardFactory.discardPile[i]);
            }
            shuffle(DICardFactory.cards);
            DICardFactory.discardPile = [];
        }






        /*******************************************************************
         ******************** NIGHT PHASE FUNCTIONS *************************
         *******************************************************************/
        function nightPhase() {
            factory.message = "End of round. Night Phase has begun";
            var player = getPlayer();
            if (!player.aiControlled) {

                alert("End of round. Night Phase has begun");
            }
            for (var i = 0; i < factory.playerArray.length; i++) {
                takeNightPhaseActions(factory.playerArray[i]);
                if (factory.playerArray[i].visionAbility && !factory.playerArray[i].aiControlled) {
                    factory.countStopped = i;
                    factory.playerArray[i].isSelectingVisionDICard = true;
                    nextTurn(factory.playerArray[i]);
                    return;
                }

            }

            if (factory.gameOver) {
                determineWinner(winner);
            } else {
                GameFactory.message = "Next round begins!";
                GameFactory.round += 1;
                getTurnOrder();
            }
            GameFactory.isNightPhase = false;
        }

        function takeNightPhaseActions(player) {
            player.visionDICardSelected = false;
            if (!player.aiControlled) {
                if (player.reinforcementsAbility || player.handOfGodAbility || player.visionAbility) {
                    alert("Hello Player " + player.name);
                }
            }

            var buttons = angular.element('polygon.button');
            buttons.css('fill', color1);
            player.actionsLeft = 5;
            player.actionsTaken = 0;
            templePoints(player);
            templePrayer(player);
            if (player.points >= GameFactory.pointsToWin) {
                factory.gameOver = 'true';
            }
            player.prayer += player.nightPrayer;
            handOfGod(player);
            reinforcements(player);
            limitPrayer(player);
            giveDICard(player);

            // Reset round limited actions
            player.actOfGodUsed = false;
            player.divineWillUsed = false;
            player.redTileBought = false;
            player.whiteTileBought = false;
            player.blueTileBought = false;
            player.hasUpgraded = false;
            player.noOfPrayers = 0;
            player.noOfRecruitments = 0;
            player.noOfMovementActions = 0;
        }



        function freeUpgrade(player) {
            checkParams(this, player);

            if (!player) {
                console.warn("player parameter missing in freeUpgrade fn")
            }
            var red = player.redPyramidLevel;
            var blue = player.bluePyramidLevel;
            var white = player.whitePyramidLevel;
            var max = GameFactory.maxPyramidLevel;
            var ans;
            if (red < max || white < max || blue < max) {
                //If its AI
                if (player.aiControlled) {
                    ans = getPyramid()
                } else {
                    ans = prompt("Player " + player.name + ", you get a free" +
                        " pyramid Upgrade! Which pyramid would you like to upgrade:" +
                        " red, white, or blue?");
                }

                switch (ans) {
                    case ("r"):
                    case("red"):
                        if (player.redPyramidLevel < GameFactory.maxPyramidLevel) {
                            player.redPyramidLevel += 1;
                        } else {
                            player.message = "You have reached the max level";

                        }
                        if (player.redPyramidLevel == 4) {
                            player.points += 1;
                            player.pyramidPoints += 1;
                        }
                        break;
                    case ("w"):
                    case("white"):
                        if (player.whitePyramidLevel < GameFactory.maxPyramidLevel) {
                            player.whitePyramidLevel += 1;
                        } else {
                            player.message = "You have reached the max level";

                        }
                        if (player.whitePyramidLevel == 4) {
                            player.points += 1;
                            player.pyramidPoints += 1;
                        }
                        break;
                    case ("b"):
                    case("blue"):
                        if (player.bluePyramidLevel < GameFactory.maxPyramidLevel) {
                            player.bluePyramidLevel += 1;
                        } else {
                            player.message = "You have reached the max level";

                        }
                        if (player.bluePyramidLevel == 4) {
                            player.points += 1;
                            player.pyramidPoints += 1;
                        }
                        break;
                    default:
                        alert("Oh, I didn't quite get your answer");
                        freeUpgrade(player);
                        break;
                }
            }
        }

        function reinforcements(player) {
            checkParams(this, player);

            if (!player) {
                console.warn("player parameter missing in reinforcements fn")
            }

            if (player.reinforcementsAbility) {
                var total = GameFactory.reinforcementsNo;
                var max = GameFactory.maxSoldiers - getFieldArmyNo(player);
                if (max > total) {
                    max = total;
                }
                if (!player.aiControlled) {
                    alert("Player " + player.name + ", you get " + max + " free" +
                        " soldiers!");
                }
                for (var i = 0; i < max; i++) {
                    addFreeRecruits(player);
                }

            }
        }

        function handOfGod(player) {
            checkParams(this, player);
            var red = player.redPyramidLevel,
                white = player.whitePyramidLevel,
                blue = player.bluePyramidLevel,
                pyramid;
            if (player.handOfGodAbility) {
                if (red >= 4 && white >= 4 && blue >= 4) {
                    return;
                }
                //If its AI
                if (player.aiControlled) {
                    pyramid = GameFactory.colors[Math.floor(Math.random() * GameFactory.colors.length)];
                } else {
                    pyramid = prompt("Which pyramid would you like to" +
                        " upgrade, 'red', 'white' or 'blue'?");
                }

                switch (pyramid) {
                    case('r'):
                    case('red'):
                        //If red pyramid is not fully upgraded, upgrade it
                        if (red < 4) {
                            player.redPyramidLevel += 1;
                            if (player.redPyramidLevel === 4) {
                                player.points += 1;
                                player.pyramidPoints += 1;

                            }
                            break;
                        } else {
                            //Ask the user to try again
                            if (!player.aiControlled) {
                                alert("Boss, reports from Giza say" +
                                    " the red pyramid has been completed. We" +
                                    " need to upgrade another pyramid");
                            }

                            handOfGod(player);
                            break;
                        }
                    case('w'):
                    case('white'):
                        //If white pyramid is not fully upgraded, upgrade it
                        if (white < 4) {
                            player.whitePyramidLevel += 1;
                            if (player.redPyramidLevel === 4) {
                                player.points += 1;
                                player.pyramidPoints += 1;

                            }

                            break;
                        } else {
                            //Ask the user to try again
                            if (!player.aiControlled) {

                                alert("Boss, reports from Giza say" +
                                    " the white pyramid has been completed. We" +
                                    " need to upgrade another pyramid");
                            }
                            handOfGod(player);
                            break;
                        }
                    case('b'):
                    case('blue'):
                        //If blue pyramid is not fully upgraded, upgrade it
                        if (blue < 4) {
                            player.bluePyramidLevel += 1;
                            if (player.redPyramidLevel === 4) {
                                player.points += 1;
                                player.pyramidPoints += 1;

                            }
                            break;
                        } else {
                            //Ask the user to try again
                            if (!player.aiControlled) {

                                alert("Boss, reports from Giza say" +
                                    " the blue pyramid has been completed. We" +
                                    " need to upgrade another pyramid");
                            }
                            handOfGod(player);
                            break;
                        }
                    default:

                        alert("Chairman, I didn't quite catch your answer." +
                            " Please enter 'red', 'white' or 'blue'.");
                        handOfGod(player);
                }
            }
        }

        function addFreeRecruits(player) {
            checkParams(this, player);
            var num = getPlayerNumber(player);
            if (!player.aiControlled) {
                var city = prompt(" Which city section do you want to place" +
                    " soldier, " +
                    " red, white or blue?");
                switch (city) {
                    case ("r" || "red"):
                        placeSoldier(player, 'redC' + num);
                        break;
                    case ("w" || "white"):
                        placeSoldier(player, 'whiteC' + num);
                        break;
                    case ("b" || "blue"):
                        placeSoldier(player, 'blueC' + num);
                        break;
                    default:
                        alert("Oh, I didn't quite get your answer, please enter" +
                            " 'red', 'white', 'blue'.");
                        addFreeRecruits();
                }
            } else {
                // refactor code to make location selection optimal
                var location = getArmyLocations(player)[0];
                if(!location) {
                    location = 'redC' + getPlayerNumber(player).toString();
                    //console.warn('no army found by ai in addFreeRecruits func')
                }
                placeSoldier(player, location)
            }
        }

        function placeSoldier(player, location) {
            checkParams(this, player, location);

            for (var x = 0; x < player.armies.length; x++) {
                if (player.armies[x].location == location) {
                    if (player.armies[x].size < player.maxArmy) {
                        player.armies[x].size += 1;
                        return 'success';
                    } else if (!player.aiControlled) {
                        console.log(player.aiControlled);
                        var ans = confirm("Army in " + location +
                            " is full. Would you like to try another city?");
                        if (ans) {
                            addFreeRecruits(player);
                        } else {
                            return;
                        }
                    } else {
                        return 'max'
                    }
                }
            }
            var army = deployArmy(player);
            army.location = location;
            try {
                army.posx = GameFactory.locations[army.location].primary[0];
                army.posy = GameFactory.locations[army.location].primary[1];
            } catch(e) {
                console.log(e);
                console.log('location: ' + location)
                console.log("caller is " + arguments.callee.caller.toString())
            }

            army.size += 1;
            return 'success';
        }

        function templePoints(player) {
            checkParams(this, player);
            var occupiedTemples = 0;
            // Loop through player's armies to find location
            for (var i=0; i<player.armies.length; i++) {
                //if location is temple, add to var occupiedTemples

                if (player.armies[i].location.match(/temp/)) {
                    occupiedTemples += 1;
                }
                //if location is night temple and army is bigger than 2,...
                if (player.armies[i].location.match(/night/) && player.armies[i].size >= 2) {
                    // ask user if he wants to sacrifice
                    //If its AI
                    if (GameFactory.isAI && player.name == player.name) {
                        ans = true;
                    } else {
                        var ans = confirm("Player " + player.name + ", should we" +
                            " sacrifice two soldiers at the Night Temple?");
                    }

                    //if yes, reduce army by two, increase points by one
                    if (ans) {
                        player.points += 1;
                        player.nightPoints += 1;

                        player.armies[i].size -= 2;
                        //If army is depleted, take it off the board
                        if (player.armies[i].size === 0) {
                            player.armies[i].location = "heaven";
                        }
                    }
                }
            }
            //if occupiedTemples > 2, increase points by one
            if (occupiedTemples >= 2) {
                player.points += 1;
                player.templePPoints += 1;
            }
            var diff = player.templeTPoints - occupiedTemples;

            if (diff != 0) {
                // console.warn('Temple points mismatch');
                // console.log('points: ' + player.points );
                // console.log('battlePoints ' + player.battlePoints);
                // console.log('templeTPoints ' + player.templeTPoints);
                // console.log('templePPoints ' + player.templePPoints);
                // console.log('tilePoints ' + player.tilePoints);
                // console.log('pyramidPoints ' + player.pyramidPoints);

                player.templeTPoints = occupiedTemples;
                var recordedPoints = getTotalPoints(player);
                player.points = recordedPoints;
                // console.log('recorded temple points: ' + occupiedTemples);
                // console.log('recordedPoints ' + recordedPoints);
            }
            recallEmptyArmies();
        }


        function getTotalPoints(player) {
            var points = 0;
            points += player.battlePoints;
            points +=  player.templeTPoints;
            points += player.templePPoints;
            points += player.tilePoints;
            points += player.pyramidPoints;
            points += player.nightPoints;
            return points;
        }

        function templePrayer(player) {
            checkParams(this, player)
            // Loop through player's armies to find location
            for (var i in player.armies) {
                if (player.armies[i].location === 'temp1' || player.armies[i].location === 'temp5') {
                    player.prayer += 2;
                }
                //if location is temp2 or temp4, add 3 prayer to total prayer
                else if (player.armies[i].location === 'temp2' || player.armies[i].location === 'temp4') {
                    player.prayer += 3;
                }
                //if location is temp3  ask ....
                else if (player.armies[i].location === 'temp3' && player.armies[i].size >= 1) {
                    //If its AI
                    if (player.aiControlled) {
                        ans = true;
                    } else {
                        var ans = confirm("Player " + player.name + ", should we" +
                            " sacrifice a soldier for 5 points?");
                    }
                    //if yes, reduce army by one, add 5 to total prayer
                    if (ans) {
                        player.prayer += 5;
                        player.armies[i].size -= 1;
                    }
                }
            }
            recallEmptyArmies();
        }

        //This function makes sure that the prayer stays below the max level allowed
        function limitPrayer(player) {
            checkParams(this, player);
            if (player.prayer > GameFactory.maxPrayer) {
                player.prayer = GameFactory.maxPrayer;
            }
        }

        function determineWinner() {
            getRanking();
            var winner = factory.playerArray[0];
            for (var i = 0; i < factory.playerArray.length; i++) {
                var player = factory.playerArray[i];
                if (player.points > winner.points) {
                    winner = player;
                } else if (player.points == winner.points && player.battlePoints > winner.battlePoints) {
                    winner = player;
                }
            }
            var loser = getOpponent(player);
            GameFactory.message = "Player " + winner.name + " wins the game" +
                " with " + winner.points + " points!";
            winner.message = "Points: " + winner.points + " Battle points: " +
                winner.battlePoints + " last position: " + (winner.position + 1);

            loser.message = "Points: " + loser.points + " Battle points: " +
                loser.battlePoints + " last position: " + (loser.position + 1);
            return winner;
        }


        function checkForEndOfRound() {
            var player = getPlayer();
            if (player.actionsLeft <= 0) {
                nextTurn();
            }
            player = getPlayer();
            if (player.actionsLeft <= 0) {
                nextTurn();
                nightPhase();
            }

        }

        /*******************************************************************
         ******************** HELPER FUNCTIONS *************************
         *******************************************************************/





        function getAllLocations() {
            var locations = [];
            for (var key in GameFactory.locations) {
                locations.push(GameFactory.locations[key].name)
            }
            return locations;
        }

        //Loop through array
        //if you find value in spec corresponding to query
        // return the ans that corresponds to the query
        // else return the whole object
        function queryArray(array, query, spec, ans) {
            for (var i = 0; i < array.length; i++) {
                if (spec) {
                    if (array[i][spec] === query) {
                        if (ans) {
                            return array[i][ans];
                        }
                        return array[i];
                    }
                } else {
                    if (array[i] === query) {
                        if (ans) {
                            return array[i][ans];
                        }
                        return array[i];
                    }
                }
            }
        }

        //This function takes num: actionCost and str: name of function
        //as arguments and returns true if action is allowed
        function checkAction(actionCost, action, player) {
            checkParams(this, actionCost, action, player);

            if (actionCost <= -1) {
                actionCost = 0;
            }

            if (!action) {
            }
            if (player.actionsLeft > 0) {

                if (player.prayer >= actionCost) {
                    switch (action) {
                        case 'upgradePyramid':
                            return true;

                        case 'pray':
                            if (player.prayer < GameFactory.maxPrayer) {
                                return true;
                            } else {
                                player.message = 'You have max prayer';
                                return false;
                            }

                        case 'recruit':
                            if (player.reserveArmyNo > 0) {
                                return true;
                            } else {
                                player.message = "El Supremo, you reached" +
                                    " 12 soldiers, the max, no one can" +
                                    " mess with you now";
                                return false;
                            }
                        case 'buyRedTile':
                            return true;
                        case 'buyWhiteTile':
                            return true;
                        case 'buyBlueTile':
                            return true;

                        default:
                            return true;

                    }

                } else {

                    player.message = "You do not have enough" +
                        " prayer!"
                    return false;
                }
            } else {
                player.message = "You have completed all your" +
                    " actions!"
                return false;
            }

        }

        // This function records moves that players make
        function recordMove(player, action) {
            var move = {
                player: player.name,
                action: action,
                round: GameFactory.round,
                turnsLeft: player.actionsLeft
            };
            MoveFactory.moves.push(move);
        }


        function getPlayer() {
            if (factory.whoseTurn.indexOf('One') > -1) {
                return PlayerOneFactory;
            } else if (factory.whoseTurn.indexOf('Two') > -1) {
                return PlayerTwoFactory;
            } else if (factory.whoseTurn.indexOf('Three') > -1) {
                return PlayerThreeFactory;
            } else if (factory.whoseTurn.indexOf('Four') > -1) {
                return PlayerFourFactory;
            } else if (factory.whoseTurn.indexOf('Five') > -1) {
                return PlayerFiveFactory;
            } else {
                console.error("Player not found");
            }
        }

        function getPlayerIndex(player) {
            checkParams(this, player);
            for (var n = 0; n < factory.playerArray.length; n++) {
                if (player.name.indexOf(factory.turnOrder[n]) > -1) {
                    return n;
                }
            }
        }

        function populatePlayerArray() {
            //Add player One and Two
            factory.playerArray = [PlayerOneFactory, PlayerTwoFactory];
            //if noOfPlayers > 2
            if (noOfPlayers > 2) {
                factory.playerArray.push(PlayerThreeFactory);
            }
            if (noOfPlayers > 3) {
                factory.playerArray.push(PlayerFourFactory);
            }
            if (noOfPlayers > 4) {
                factory.playerArray.push(PlayerFiveFactory);
            }
            //add player three and so forth
        }

        function populatePlayerBattleCards() {

            for (var i = 0, len = factory.playerArray.length; i < len; i++) {
                for (var n = 0; n < BattleCardFactory.cards.length; n++) {
                    factory.playerArray[i].unplayedBattleCards[n] = BattleCardFactory.cards[n];
                }
            }
        }


        function moveArrayElement(source, target, element) {

            for (var i = 0; i < source.length; i++) {
                if (element == source[i]) {
                    source.splice(i, 1);
                    target.push(element);
                }
            }
            for (var i = 0; i < target.length; i++) {
                if (target[i] == undefined) {
                    console.error('target elm is undefined at index: ' + i);
                    target = cleanArray(target)
                }
            }
            for (var i = 0; i < source.length; i++) {
                if (source[i] == undefined) {
                    console.error('source elm is undefined at index: ' + i);
                    source = cleanArray(source)
                }
            }
        }

        function toTitleCase(str) {
            return str.replace(/\w\S*/g, function (txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            });
        }

        function getPlayerNumber(player) {
            switch (player.name) {
                case ('One'):
                    return 1;
                case('Two'):
                    return 2;
                case('Three'):
                    return 3;
                case('Four'):
                    return 4;
                case('Five'):
                    return 5;
                default:
                    console.error("Couldn't find player number");
            }
        }


        function getPlayerObj(owner) {
            switch (owner) {
                case 'One':
                    return PlayerOneFactory;
                case 'Two':
                    return PlayerTwoFactory;
                case 'Three':
                    return PlayerThreeFactory;
                case 'Four':
                    return PlayerFourFactory;
                case 'Five':
                    return PlayerFiveFactory;
                default:
                    console.error("Player not found");
            }
        }

        function shuffle(array) {
            var currentIndex = array.length, temporaryValue, randomIndex;

            // While there remain elements to shuffle...
            while (0 !== currentIndex) {

                // Pick a remaining element...
                randomIndex = Math.floor(Math.random() * currentIndex);
                currentIndex -= 1;

                // And swap it with the current element.
                temporaryValue = array[currentIndex];
                array[currentIndex] = array[randomIndex];
                array[randomIndex] = temporaryValue;
            }

            return array;
        }

        function cleanArray(actual) {
            var newArray = new Array();
            for (var i = 0; i < actual.length; i++) {
                if (actual[i] != undefined) {
                    newArray.push(actual[i]);
                }
            }
            return newArray;
        }

        function getRandomNumber(limit) {
            var number = Math.floor((Math.random() * limit));
            return number;
        }

        function allPlayers(playerFunc) {
            for (var i=0; i<factory.playerArray.length; i++) {
                playerFunc(factory.playerArray[i]);
            }
        }

        function returnTileToDeck(player, tile) {
            moveArrayElement(player[tile.color + 'Tiles'], TileFactory[tile.color + 'Tiles'], tile);
        }


        function resetGame() {
            attacker = {};
            defender = {};
            currentPlayer = {};
            resetBattleCards();
            allPlayers(function(player) {
                resetTiles(player);
                resetDICards(player);
                resetPlayerProperties(player);
                resetPlayerArmies(player);
            });
            recallEmptyArmies();
            factory.gameOver = false;
            GameFactory.round = 1;
            preGameActions();
        }

        function resetDICards(player) {
            //     console.log('player ' + player.name + ' has ' + player.dICards.length + ' dI cards')
            //     for (var i=0; i<player.dICards.length; i++) {
            //         returnCardToDeck(player, player.dICards[i]);
            //     }
            //     console.log('player ' + player.name + ' has ' + player.dICards.length + ' dI cards')
            //     console.log('no of DI cards: ' + DICardFactory.cards.length)
            //
            //     reshuffleDICards();
            //     console.log('no of DI cards after shuffle: ' + DICardFactory.cards.length)
            //
            DICardFactory.discardPile = [];

            DICardFactory.cards = [
                {
                    name: 'Bloodbath',
                    cost: 1,
                    phase: 'battle',
                    img: "img/bloodbath.jpg",
                    effect: bloodBath,
                },
                {
                    name: 'Bloodbath',
                    cost: 1,
                    phase: 'battle',
                    img: "img/bloodbath.jpg",
                    effect: bloodBath,
                },
                {
                    name: 'Bloody Battle',
                    cost: 0,
                    phase: 'battle',
                    img: "img/bloody-battle.jpg",
                    effect: bloodyBattle,
                },
                {
                    name: 'Bloody Battle',
                    cost: 0,
                    phase: 'battle',
                    img: "img/bloody-battle.jpg",
                    effect: bloodyBattle,
                },
                {
                    name: 'Bloody Battle',
                    cost: 0,
                    phase: 'battle',
                    img: "img/bloody-battle.jpg",
                    effect: bloodyBattle,
                },
                {
                    name: 'Bronze Wall',
                    cost: 0,
                    phase: 'battle',
                    img: "img/bronze-wall.jpg",
                    effect: bronzeWall,
                },
                {
                    name: 'Bronze Wall',
                    cost: 0,
                    phase: 'battle',
                    img: "img/bronze-wall.jpg",
                    effect: bronzeWall,
                },
                {
                    name: 'Bronze Wall',
                    cost: 0,
                    phase: 'battle',
                    img: "img/bronze-wall.jpg",
                    effect: bronzeWall,
                },
                {
                    name: 'Enlistment',
                    cost: 0,
                    phase: 'day',
                    img: "img/enlistment.jpg",
                    effect: enlistment,
                },
                {
                    name: 'Enlistment',
                    cost: 0,
                    phase: 'day',
                    img: "img/enlistment.jpg",
                    effect: enlistment,
                },
                {
                    name: 'Enlistment',
                    cost: 0,
                    phase: 'day',
                    img: "img/enlistment.jpg",
                    effect: enlistment,
                },
                {
                    name: 'Enlistment',
                    cost: 0,
                    phase: 'day',
                    img: "img/enlistment.jpg",
                    effect: enlistment,
                },
                {
                    name: 'Escape',
                    cost: 0,
                    phase: 'other',
                    img: "img/escape.jpg",
                    effect: escape,
                },
                {
                    name: 'Iron Wall',
                    cost: 1,
                    phase: 'battle',
                    img: "img/iron-wall.jpg",
                    effect: ironWall,
                },
                {
                    name: 'Iron Wall',
                    cost: 1,
                    phase: 'battle',
                    img: "img/iron-wall.jpg",
                    effect: ironWall,
                },
                {
                    name: 'Mana Theft',
                    cost: 0,
                    phase: 'day',
                    img: "img/mana-theft.jpg",
                    effect: manaTheft,
                },
                {
                    name: 'Mana Theft',
                    cost: 0,
                    phase: 'day',
                    img: "img/mana-theft.jpg",
                    effect: manaTheft,
                },
                {
                    name: 'Open Gates',
                    cost: 1,
                    phase: 'day',
                    img: "img/open-gates.jpg",
                    effect: openGates,
                },
                {
                    name: 'Prayer',
                    cost: 0,
                    phase: 'day',
                    img: "img/prayer.jpg",
                    effect: prayer,
                },
                {
                    name: 'Prayer',
                    cost: 0,
                    phase: 'day',
                    img: "img/prayer.jpg",
                    effect: prayer,
                },
                {
                    name: 'Raining Fire',
                    cost: 1,
                    phase: 'day',
                    img: "img/raining-fire.jpg",
                    effect: prayer,
                },
                {
                    name: 'Raining Fire',
                    cost: 1,
                    phase: 'battle',
                    img: "img/raining-fire.jpg",
                    effect: rainingFire,
                },
                {
                    name: 'Raining Fire',
                    cost: 1,
                    phase: 'day',
                    img: "img/raining-fire.jpg",
                    effect: rainingFire,
                },
                {
                    name: 'Raining Fire',
                    cost: 1,
                    phase: 'day',
                    img: "img/raining-fire.jpg",
                    effect: prayer,
                },

                {
                    name: 'Teleportation',
                    cost: 1,
                    phase: 'day',
                    img: "img/teleportation.jpg",
                    effect: teleportation,
                },
                {
                    name: 'Teleportation',
                    cost: 1,
                    phase: 'day',
                    img: "img/teleportation.jpg",
                    effect: teleportation,
                },
                {
                    name: 'Teleportation',
                    cost: 1,
                    phase: 'day',
                    img: "img/teleportation.jpg",
                    effect: teleportation,
                },

                //{
                //    name: 'Veto',
                //    cost: 0,
                //    phase: 'day',
                //    img: "img/veto.jpg",
                //    effect: veto,
                //},
                //{
                //    name: 'Veto',
                //    cost: 0,
                //    phase: 'day',
                //    img: "img/veto.jpg",
                //    effect: veto,
                //},
                {
                    name: 'War Fury',
                    cost: 1,
                    phase: 'battle',
                    img: "img/war-fury.jpg",
                    effect: warFury,
                },
                {
                    name: 'War Fury',
                    cost: 1,
                    phase: 'battle',
                    img: "img/war-fury.jpg",
                    effect: warFury,
                },
                {
                    name: 'War Fury',
                    cost: 1,
                    phase: 'battle',
                    img: "img/war-fury.jpg",
                    effect: warFury,
                },
                {
                    name: 'War Rage',
                    cost: 0,
                    phase: 'battle',
                    img: "img/war-rage.jpg",
                    effect: warRage,
                },
                {
                    name: 'War Rage',
                    cost: 0,
                    phase: 'battle',
                    img: "img/war-rage.jpg",
                    effect: warRage,
                },
                {
                    name: 'War Rage',
                    cost: 0,
                    phase: 'battle',
                    img: "img/war-rage.jpg",
                    effect: warRage,
                },
            ];
            function bloodBath(player) {
                player.cardDamage += 2;
            }

            function bloodyBattle(player) {
                player.dICardDamage += 1;
            }

            function bronzeWall(player) {
                player.cardShield += 1;
            }

            function enlistment(player) {
                if(player.aiControlled) {

                }
            }

            function escape(player) {
            }

            function ironWall (player) {
                player.cardShield += 2;
            }

            function manaTheft(player) {

            }

            function openGates(player) {
                player.hasOpenGates = true;
            }

            function prayer(player) {
                player.prayer += 2;
            }

            function rainingFire(player) {
                player.cardStrength += 1;
            }


            function teleportation(player) {
                player.prayer += 1;
            }
            function veto(player) {

            }

            function warFury(player) {
                player.cardStrength += 2;

            }

            function warRage(player) {
                player.cardStrength += 1;
            }
        }

        function resetTiles(player) {
            for (var n=0; n<GameFactory.colors.length; n++) {
                var deck = player[GameFactory.colors[n] + 'Tiles'];
                for (var i = 0; i < deck.length; i++) {
                    returnTileToDeck(player, deck[i]);
                }
            }
        }

        function resetBattleCards() {
            BattleCardFactory.cards = [
                {
                    strength: 4,
                    damage: 1,
                    shield: 0,
                    img: "img/battle41.jpg",
                },
                {
                    strength: 1,
                    damage: 3,
                    shield: 0,
                    img: "img/battle13.jpg",
                },
                {
                    strength: 3,
                    damage: 0,
                    shield: 1,
                    img: "img/battle31.jpg",
                },
                {
                    strength: 2,
                    damage: 0,
                    shield: 2,
                    img: "img/battle22.jpg",
                },
                {
                    strength: 3,
                    damage: 2,
                    shield: 0,
                    img: "img/battle32.jpg",
                },

                {
                    strength: 2,
                    damage: 2,
                    shield: 1,
                    img: "img/battle221.jpg",
                },




            ]

            BattleCardFactory.offensiveStrategy = {
                strength: 3,
                damage: 3,
                shield: 0,
                img: "img/battle33.jpg"
            };

            BattleCardFactory.defensiveStrategy = {
                strength: 3,
                damage: 0,
                shield: 3,
                img: "img/battle33D.jpg"
            };
        }



        function resetPlayerArmies(player) {
            for (var i = 0; i < player.armies.length; i++) {
                var army = player.armies[i];
                army.location = 'heaven';
                army.size = 0;
                army.creature = '';
            }
        }


        function resetPlayerProperties(player) {
            player.position = 0;
            player.turnMarker = {};
            player.turnMarker.posx = GameFactory.turnMarker[player.position].posx;
            player.turnMarker.posy = GameFactory.turnMarker[player.position].posy;
            player.isTurn = true;
            player.playedBattleCards = [];
            player.unplayedBattleCards = [];
            player.dICards = [];
            player.fightCards = [];

            player.tileBought = false;
            player.isBuyingRedTile = false;
            player.isBuyingWhiteTile = false;
            player.isBuyingBlueTile = false;
            player.canRecruit = false;
            player.isRecruiting = false;
            player.isMoving = false;
            player.canMove = false;
            player.moved = false;
            player.movementStarted = false;
            player.isUpgrading = false;

            player.redTileBought = false;
            player.whiteTileBought = false;
            player.blueTileBought = false;
            player.hasUpgraded = false;
            player.noOfPrayers = 0;
            player.noOfRecruitments = 0;
            player.noOfMovementActions = 0;

            player.attackStrength = 0;
            player.defenseStrength = 0;
            player.movement = GameFactory.movement;
            player.damage = 0;
            player.shield = 0;
            player.points = 0;
            player.battlePoints = 0;
            player.templeTPoints = 0;
            player.templePPoints = 0;
            player.tilePoints = 0;
            player.pyramidPoints = 0;
            player.nightPoints = 0;
            player.nightPrayer = 2;
            player.templePrayerBonus = 0;
            player.maxArmy = 5;
            player.dIBonus = 0;
            player.teleportCost = 2;

            player.isBuyingOffensiveStrategy = false;
            player.isBuyingDefensiveStrategy = false;

            player.godSpeedAbility = false;
            player.openGatesAbility = false;
            player.obeliskTeleportAbility = false;
            player.divineWoundAbility = false;
            player.initiativeAbility = false;
            player.actOfGodAbility = false;
            player.handOfGodAbility = false;
            player.divineWillAbility = false;
            player.crusadeAbility = false;
            player.prescienceAbility = false;
            player.reinforcementsAbility = false;
            player.visionAbility = false;
            player.prayerIncrement = 2;
            player.holyWarAbility = false;
            player.deepDesertSnakeAbility = false;
            player.defensiveVictoryAbility = false;
            player.sphinxPointsApplied = false;


            player.generalDiscount = false;
            player.teleportDiscount = 0;
            player.tileDiscount = 0;
            player.pyramidDiscount = 0;
            player.recruitDiscount = 0;

            player.redPyramidLevel = 0;
            player.whitePyramidLevel = 0;
            player.bluePyramidLevel = 0;
            player.prayer = GameFactory.initialPrayer;
            player.actionsLeft = GameFactory.maxActions;
            player.actionsTaken = 0;
            player.message = "";
            player.fieldArmyNo = 0;
            player.reserveArmyNo = GameFactory.maxSoldiers - player.fieldArmyNo;

            player.cardStrength = 0;
            player.cardDamage = 0;
            player.cardShield = 0;
            player.dWCardStrength = 0;

            player.creatureMovement = 0;

            player.battleCardApplied = false;
            player.cardDiscarded = false;
            player.isSelectingDivineWoundCards = false;
            player.divineWoundCardsSelected = false;
            player.actOfGodAbilitySelected = false;
            player.divineWillAbilitySelected = false;
            player.visionDICardSelected = false;
            player.isUsingTeleportationCard = false;
            player.isRainingFire = false;
            player.isMovingOpponent = false;
            player.isEscaping = false;
            player.hasOpenGates = false;
            player.pyramidColorSelected = "";
            player.victoryPointBought = false;
            player.actOfGodBought = false;
            player.actOfGodUsed = false;
            player.divineWillUsed = false;
            player.isAttacking = false;
            player.aiControlled = false;
            player.postBattleChoice = '';
            player.retreatLocation = '';
            player.showCards = false;
            player.attacksInitiated = 0;

            player.redTiles = [];
            player.whiteTiles = [];
            player.blueTiles = [];

            player.creatureInHand = false;
            player.creatureName = '';
            player.creaturesDeployed = [];
        }

        function preGameActions() {
            factory.isBattlePhase = false;
            factory.destination = "";


            factory.readyForBattle = false;


            populatePlayerArray();

            populatePlayerBattleCards();
        }


        return factory;
    }


    function checkParams() {
        //Figure out why arguments[0] is sometimes undefined

        if(arguments.length <= 0) { return false }
        for (var i = 1; i < arguments.length; i++) {
            if (arguments[i] == undefined || arguments[i] == null) {
                if (arguments[0] == undefined) {
                    arguments[0] = {name: 'Unknown'};
                    console.log("caller of checkParams is " + arguments.callee.caller.name)
                } else {
                    //console.warn("parameter no " + (i + 1) + " is missing in " + arguments.callee.caller.name + " function");

                }
                return false;
            }
        }
        return true;
    }


})();