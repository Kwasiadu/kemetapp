/**
 * This is the AI/machine learning factory. In addition to telling the AI player what to do, it records each move the ai
 * makes and stores the outcome so that the AI can teach itself how to play
 **/


(function () {
    //'use strict';

    angular.module('kemetApp').factory('MLFactory', MLFactory);

    MLFactory.$inject = ['PlayFactory', 'GameFactory', 'PlayerOneFactory',
        '$http', 'TileFactory', 'BattleCardFactory'];
    function MLFactory(PlayFactory, GameFactory, PlayerOneFactory,
                       $http, TileFactory, BattleCardFactory) {
        var factory = {};

        factory.moves = [];
        factory.games = [];

        factory.run = run;
        factory.recordGame = recordGame;
        factory.getAllMoves = getAllMoves;
        factory.storeMove = storeMove;
        factory.storeGame = storeGame;
        factory.getAllGames = getAllGames;
        factory.aiFight = aiFight;
        factory.pickPyramids = pickPyramids;
        factory.placeRecruits = placeRecruits;
        factory.beginGame = beginGame;



        var idx;
        var ai = {};
        var moves =[];

        ai.pyramids = ['whitePyramidLevel', 'redPyramidLevel', 'bluePyramidLevel' ];
        ai.colors = ['white', 'red', 'blue'];

        var spaces = ['temp3', 'temp2', 'temp1',
            'redC1', 'whiteC1', 'blueC1',
            'redC2', 'whiteC2', 'blueC2',
            'buff1', 'buff2', 'buff3', 'buff4',
            'obel1'];

        ai.count = 0;
        
        if (GameFactory.testing) {
            tests();
        }

        /**
         * Takes initial ai actions like collecting DI cards,
         * picking pyramids and placing initial recruits.
         */
        function beginGame() {
            PlayFactory.allPlayers(function(player) {
                if (player.aiControlled) {
                    PlayFactory.giveDICard(player);
                    pickPyramids(player);
                    placeRecruits(player);
                } else {
                    PlayFactory.beginGame();
                }
            });
        }

        /**
         * Adds all possible ai actions to an array and returns it.
         * @returns {String[]}
         */
        function populateActionsArray() {
            var player = PlayFactory.getPlayer();
            // Empty actionsArray;
            var actionsArray = [];

            actionsArray = addTileActions(player, actionsArray);
            actionsArray = addUpgradeActions(player, actionsArray);
            actionsArray = addPrayActions(player, actionsArray);
            actionsArray = addRecruitActions(player, actionsArray);
            actionsArray = addAttackActions(player, actionsArray);
            actionsArray = addTempleMovementActions(player, actionsArray);
            // actionsArray = addManeuveringActions(player, actionsArray);
            return actionsArray;

        }

        /**
         * Adds available tile buying actions to the actions sarray
         * @param {Object} player
         * @param {String[]} actionsArray
         * @returns {String[]}
         */

        function addTileActions(player, actionsArray) {
            var tileNames = getNamesOfAvailableTiles(player);
            for (var i = 0; i < tileNames.length; i++) {
                var action = ['buy', tileNames[i], '', ''];
                actionsArray.push(action);
            }
            return actionsArray
        }

        /**
         * Adds available pyramid upgrade actions to the actions array
         * @param {Object} player 
         * @param {String[]} actionsArray 
         * @returns {String[]}
         */
        function addUpgradeActions(player, actionsArray) {
            if (!player.hasUpgraded) {
                for (var n = 0; n < ai.colors.length; n++) {
                    for (var noOfUpgrades = 1; noOfUpgrades < 4; noOfUpgrades++) {
                        actionsArray.push(['upgrade', ai.colors[n], noOfUpgrades])
                    }
                }
            }
            return actionsArray;
        }

        /**
         * Adds available pray actions to the actions array
         * @param {Object} player 
         * @param {String[]} actionsArray 
         * @returns {String[]}
         */
        function addPrayActions(player, actionsArray) {
            if (player.noOfPrayers < 3) {
                actionsArray.push(['pray', 'row2']);
                actionsArray.push(['pray', 'row3']);
            }
            return actionsArray;
        }

        /**
         * Adds available recruiting actions to the actions array
         * @param {Object} player 
         * @param {String[]} actionsArray 
         * @returns {String[]}
         */
        function addRecruitActions(player, actionsArray) {
            if (player.noOfRecruitments < 1 || (player.divineWillAbility && player.noOfRecruitments < 2)) {
                var noOfReserveSoldiers = GameFactory.maxSoldiers - PlayFactory.getFieldArmyNo(player);
                for (var i = 1; i < noOfReserveSoldiers; i++) {
                    actionsArray.push(['recruit', i]);
                }
            }
            return actionsArray;
        }

        /**
         * Adds available movement actions to the actions array
         * @param {Object} player 
         * @param {String[]} actionsArray 
         * @returns {String[]}
         */
        function addMovementActions(player, actionsArray, destinations, army_condition) {
            var armyLocations = PlayFactory.getArmyLocations(player);
            army_condition = army_condition || function () { return true; }
            for (var i = 0; i<armyLocations.length; i++) {
                var origin_army = PlayFactory.getPlayerArmyAtLocation(player, armyLocations[i]);
                for (var j=0, len=destinations.length; j<len; j++) {
                    var dest_army = PlayFactory.getPlayerArmyAtLocation(
                        PlayFactory.getOpponent(player), destinations[i]);
                    if (armyLocations[i] == destinations[j]) {
                        continue;
                    }
                    if (army_condition(origin_army, dest_army)) {
                        actionsArray.push(['move', armyLocations[i], destinations[j], 'row1']);
                        actionsArray.push(['move', armyLocations[i], destinations[j], 'row2']);
                    }

                }
            }
            return actionsArray;
        }

        /**
         * adds available strategic movement actions to the actions array
         * @param {Object} player
         * @param {String[]} actionsArray
         * @returns {String[]}
         */

        function addManeuveringActions(player, actionsArray) {
            if (player.noOfMovementActions < 3 || (player.divineWillAbility && player.noOfMovementActions < 3)) {
                addMovementActions(player, actionsArray, getEmptyLocations());
            }
            return actionsArray;
        }

        /**
         * adds available attack actions to the actions array
         * @param {Object} player
         * @param {String[]} actionsArray
         * @returns {String[]}
         */
        function addAttackActions(player, actionsArray) {
            var enemyLocations = PlayFactory.getArmyLocations(PlayFactory.getOpponent());
            actionsArray = addMovementActions(player, actionsArray, enemyLocations);
            return actionsArray;
        }

        /**
         * adds available temple movement actions to the actions array
         * @param {Object} player
         * @param {String[]} actionsArray
         * @returns {String[]}
         */
        function addTempleMovementActions(player, actionsArray) {
            var templeLocations = getTempleLocations();
            actionsArray = addMovementActions(player, actionsArray, templeLocations, templeMovementCondition);
            return actionsArray;
        }

        /**
         * determines whether it is a good idea for an army to enter a temple
         * @param {Object} army
         * @returns {Boolean}
         */
        function templeMovementCondition(army) {
            return army.size >= 5;
        }

        /**
         * returns the names of the tiles that the ai can buy
         * @param {Object} player
         * @returns {String[]}
         */
        function getNamesOfAvailableTiles(player) {
            var tile_names_arr = [];
            for (var i=0, len=ai.colors.length; i<len; i++) {
                var color = ai.colors[i];
                if (player[color + 'TileBought']) {
                    continue;
                }
                for (var j=0, length=TileFactory[color + 'Tiles'].length; j<length; j++) {
                    var tile = TileFactory[color + 'Tiles'][j];
                    if (tile.cost <= player[color + 'PyramidLevel']) {
                        tile_names_arr.push(tile.name);
                    }
                }
            }
            return tile_names_arr;
        }

        /**
         * logs an error message in the console
         * @param {String} location
         * @param {String} message

         */
        function err (location, message) {
            console.warn(message + ' in ' + location);
        }

        /**
         * takes a single ai action and records it
         * @param {Object} player

         */
        function run(player) {
            var result;
            player = player || PlayFactory.getPlayer();
            var actionsArray = populateActionsArray();

            if (player.actionsLeft > 0){
                for (var i = 0; i < 50; i++) {
                    result = takeAction(actionsArray);
                    if (result == 'success') {
                        return;
                    } else if (result == 'battle') {
                        var opponent = PlayFactory.getOpponent();

                        aiFight(player, opponent);
                        return;
                    }
                }
                //console.warn('No action taken');
                noAction();
                recordMove(['No action']);
            }
        }

        /**
         * randomly increases the player's pyramid level 3 times
         * It caps pyramid levels at 2 in accordance with game rules
         * @param {Object} player

         */
        function pickPyramids(player) {
            player = player || PlayFactory.getPlayer();
            // get array of the 3 pyramids
            var pyramids = ai.pyramids.slice(0);
            for (var i=0; i<3; i++) {
                // Get a random pyramid an increase it's level by 1
                var rand = PlayFactory.getRandomNumber(pyramids.length);
                player[pyramids[rand]] += 1;
                // Prevent any pyramid from being increased to level 3 by
                // removing it from the array when its greater than 2
                if(player[pyramids[rand]] > 1) {
                    pyramids.splice(rand, 1)
                }
            }
        }

        /**
         * places ai player's initial 10 recruits
         * @param {Object} player

         */
        function placeRecruits(player) {
            player = player || PlayFactory.getPlayer();
            var cities = ai.colors.map(function(color) {
                return color + 'C' + PlayFactory.getPlayerNumber(player).toString()
            });
            for (var i=0; i<10; i++) {
                if (cities.length <= 0 ) {
                    return; }

                var rand = PlayFactory.getRandomNumber(cities.length);
                var result = PlayFactory.placeSoldier(player, cities[rand]);
                if (result == 'max') {
                    cities.splice(rand, 1);
                    i--;
                }
            }
        }

        /**
         *
         * @param player
         * @param opponent
         */
        function aiFight(player, opponent) {
            player = player || PlayFactory.getPlayer();
            opponent = opponent || PlayFactory.getOpponent(player);
            var battleZone = PlayFactory.getConflictZone();

            player.postBattleChoice = getPostBattleChoice();
            player.retreatLocation = getRetreatLocationChoice(battleZone);

            // if ai is defending against human opponent
            if(PlayFactory.firstFighterReady) {
                PlayFactory.secondFightReady = true;
            } else {
                if(battleZone) {
                    opponent.postBattleChoice = getPostBattleChoice();
                    opponent.retreatLocation = getRetreatLocationChoice(battleZone);
                } else {
                    opponent.postBattleChoice = 'sacrifice';
                    console.warn('no battleZone found. No retreatLocation selected')
                }

                PlayFactory.firstFighterReady = true;

            }
            playCards(player, opponent);

            // Refactor fight function to get data about fight
            PlayFactory.fight(player, opponent);
        }

        /**
         * Returns the location where the ai's defeated army should retreat to
         * @param {String} battleZone
         * @returns {Array<String>}
         */
        function getRetreatLocationChoice(battleZone) {
            var locations = PlayFactory.getRetreatLocations(battleZone);
            var rtlc = locations[PlayFactory.getRandomNumber(locations.length)];
            return rtlc;
        }

        /**
         *
         * @returns {String}
         */
        function getPostBattleChoice () {

            var rand = PlayFactory.getRandomNumber(2);
            if (rand) {
                return '';
            } else {
                return 'sacrifice';
            }
        }

        /**
         * plays the ai players battle and divine intervention cards before the battle
         * @param {Object} player
         * @param {Object} opponent

         */
        function playCards(player, opponent) {
            player = player || PlayFactory.getPlayer();
            opponent = opponent || PlayFactory.getOpponent();
            playBattleCards(player);
            playDICards(player);
            if(opponent.aiControlled) {
                playBattleCards(opponent);
                playDICards(opponent);
                PlayFactory.applyCards(player, opponent);
            }
        }

        /**
         * Completes the ai turn without taking an action
         * @param {Object} player
         * @returns {String}
         */
        function noAction (player) {
            player = player || PlayFactory.getPlayer();
            player.actionsLeft--;
            player.actionsTaken++;
            PlayFactory.checkForEndOfRound();
        }

        /**
         * selects a random action from the player's actions array
         * @param {String[]} actionsArray
         * @param {Object} player

         */
        function takeAction(actionsArray, player) {
            player = player || PlayFactory.getPlayer();
            if (actionsArray.length > 0) {
                idx = PlayFactory.getRandomNumber(actionsArray.length);
                var actionParams = actionsArray[idx];
                playDICards(player);
                var result = aiFn(actionParams[0], actionParams[1], actionParams[2], actionParams[3]);
                if (!result) {
                    console.info("player" + player.name + ' result was ' + result + ": " + actionParams);
                }

                if(result != 'error' && result != undefined && GameFactory.recordGame) {
                    recordMove(actionParams, result);
                }
                if (result == 'success') {
                    if (actionParams[0] == 'buy') {
                        actionsArray.splice(idx, 1);
                    }
                }
                return result;
            } else {
                console.warn('actionsArray is empty')
            }
        }

       /**
         * The aiFight function
         * @param {Object} player

         */
       function playBattleCards(player) {
            player = player || PlayFactory.getPlayer();
            // Select Card
            var idx = PlayFactory.getRandomNumber(player.unplayedBattleCards.length);
            PlayFactory.applyBattleCard(player.unplayedBattleCards[idx], idx, player.name);
            // Discard Card
            idx = PlayFactory.getRandomNumber(player.unplayedBattleCards.length);
            PlayFactory.applyBattleCard(player.unplayedBattleCards[idx], idx, player.name);
        }

       /**
         * The playDICards function
         * @param {Object} player

         */
       function playDICards(player) {
            player = player || PlayFactory.getPlayer();
            for (var i=0, len=player.dICards.length; i<len; i++) {
                if (PlayFactory.isBattlePhase) {
                    if(player.dICards[i].phase == 'battle') {
                        randomDICard(player, i, 50);
                    } else if (player.divineWoundAbility) {
                        randomDICard(player, i, 50);
                    }
                } else if(player.dICards[i].phase == 'day') {
                    randomDICard(player, i, 30)
                }
            }
        }

       /**
        * The randomDICard function
        * @param {Object} player
        * @param {Number} index
        * @param {Number} odds

        */
        function randomDICard(player, index, odds) {
            player = player || PlayFactory.getPlayer();
            var number = PlayFactory.getRandomNumber(100);
            if (number < odds) {
                PlayFactory.applyDICard(player.dICards[index], index, player.name)
            }
        }

        /**
         * The attachCreature function
         * @param {Object} player
         * @param {Object} tileInfo

         */
        function attachCreature(player, tileInfo) {
            var city = findCityWithArmy('no creature');
            if(city && city.length > 0) {
                PlayFactory.powerTileFunction(tileInfo.tile, player);
                //add it to army
                PlayFactory.recruit(city, player);
            }
        }

        /**
         * The getPlayerArmies function
         * @param {Object} player
         * @returns {String[]}
         */
        function getPlayerArmies(player) {
            player = player || PlayFactory.getPlayer();
            var city = '';
            var cityName = 'C' + PlayFactory.getPlayerNumber(player);
            var army = {};
            var armies = [];
            for (var i=0, len=GameFactory.colors.length; i<len; i++){
                city = GameFactory.colors[i] + cityName;
                army = queryArray(player.armies, city, 'location');
                if(army) {
                    armies.push(army);
                }
            }
            return armies
        }

        /**
         * Sorts objects in array according to size largest first, and returns array
         * @param {Object[]}  arr:  Array of objects
         * @returns: {Object[]}
         */
        function sortObjInArrayBySize (arr) {
            if (!arr[0].hasOwnProperty('size')) {
                console.error('Array of objects cannot be sorted');
                console.info('Array:');
                console.info(arr);
                return arr;
            }

            if (!arr) {
                console.error('Missing arr parameter');
                return [];
            }

            for (var n=0, len=arr.length; n<len; n++) {
                for (var i = 1; i < arr.length; i++) {
                    if (arr[i].size > arr[i - 1].size) {
                        var holder = arr[i - 1];
                        arr[i - 1] = arr[i];
                        arr[i] = holder;
                    }
                }
            }
            return arr;
        }

        /*
         * main AI action function
         * @params action: name of action
         * @params item: object that action will be performed on
         * @params info: information about action to be performed
         * @params id: id of angular button corresponding to the action
         * @params player: player performing action
         * @returns {String}
         */
        function aiFn(action, item, info, id, player){
            player = player || PlayFactory.getPlayer();
            var result;
            if(action === 'buy') {
                result = aiBuyAction(item, player);
            } else if(action == 'pray') {
                return PlayFactory.prayAction(player.name, 'pray' + player.name + item);
            } else if(action == 'upgrade') {
                result = aiUpgradeAction(player, item, info);
            } else if(action == 'move') {
                result = aiMoveAction(player, item, info, id);
            } else if(action === 'recruit') {
                result = aiRecruitAction(player, item);
            }
            //PlayFactory.checkForEndOfRound()
            PlayFactory.destination = '';
            return result;

        }

        /**
         *
         * @param {String} tileName
         * @param {Object} player
         * @returns {String}
         */
        function aiBuyAction(tileName, player) {
            player = player || PlayFactory.getPlayer();

            //retrieve tile information

            var tileInfo = getTile(tileName);
            if (!tileInfo) {
                return 'error';
            }
            var result = PlayFactory.buyTile(tileInfo.tile, tileInfo.index);
            //If the tile bought requires extra action,

            //and also happens to be a creature tile
            if (GameFactory.creatures.indexOf(tileInfo.tile.name) > -1) {
                // attach creature if an army is available
                attachCreature(player, tileInfo);
                PlayFactory.actionButton('finish', player.name);
                if (PlayFactory.tileButton) {
                    PlayFactory.tileButton.css('fill', player.color);
                }
                return result;


            } else if(tileInfo.tile.name == 'offensive strategy' && result == 'success') {
                replaceOffensiveBattleCard(player);
            } else if (tileInfo.tile.name == 'defensive strategy' && result == 'success') {
                replaceDefensiveBattleCard(player);
            }
            console.log('result: ' + result);
            return result;

        }
        /**
         *
         * @param {Object} player
         * @param {String} pyramid
         * @param {Number} noOfUpgrades
         * @returns {String}
         */
        function aiUpgradeAction (player, pyramid, noOfUpgrades) {
            for (var i=0, len=noOfUpgrades; i<len; i++) {
                var result = PlayFactory.upgradePyramid(pyramid, player.name);
                // if upgrade failed on the first try, return error
                if (i == 0 && result != 'success') {
                    return 'error';
                }
            }
            PlayFactory.upgradeAction(player.name, 'upgrade' + player.name);
            return 'success';
        }

        /**
         *
         * @param {Object} player
         * @param {String} origin
         * @param {String} destination
         * @param {String} button_id
         * @returns {String}
         */
        function aiMoveAction(player, origin, destination, button_id) {
            var armyObject = queryArray(player.armies, origin, 'location');
            if(!armyObject) { return 'error'; }

            var result = moveAI(player, armyObject, origin, destination);
            if(result == 'success' || result == 'battle') {
                PlayFactory.finishMoveAction(player.name,
                    'move' + player.name + button_id);
            }
            return result;
        }
        /**
         *
         * @param {Object} player
         * @param {Number} noOfRecruits
         * @returns {String}
         */
        function aiRecruitAction(player, noOfRecruits) {
            var cities = getCitiesWithoutFullArmies(player);
            if (cities.length == 0) { return 'error'; }
            var result = addSoldiersToCities(player, noOfRecruits, cities);
            //end recruiting
            PlayFactory.recruitAction(player.name, 'recruit' + player.name);
            return result;
        }
        /**
         *
         * @param {Object} player
         * @returns {String[]}
         */
        function getCitiesWithoutFullArmies(player) {
            player = player || PlayFactory.getPlayer();
            var cities = [];
            var city;
            for (var i=0, len=ai.colors.length; i<len; i++) {
                // get city name for recruitment
                city = ai.colors[i] + 'C' + PlayFactory.getPlayerNumber(player).toString();
                var armySize = queryArray(player.armies, city, 'location', 'size');
                // if city has no army, or an army that isn't full, add the city to array
                if (armySize < player.maxArmy || armySize == undefined) {
                    cities.push(city)
                }
            }
            return cities;
        }
        /**
         *
         * @param {Object} player
         * @param {Number} noOfRecruits
         * @param {String[]} cities
         * @returns {String}
         */
        function addSoldiersToCities(player, noOfRecruits, cities) {
            var result;
            var soldiers = 0;
            // Add recruits to available cities
            for (var i=0, len=cities.length; i<len; i++) {
                while (soldiers < noOfRecruits) {
                    result = PlayFactory.recruit(cities[i], player);
                    // if recruiting isn't possible, end recruiting
                    if (result == 'error') { return 'error'}
                    // if city is full, move on to next city
                    if (result == 'max') { break; }
                    soldiers ++;
                }
            }
            return 'success';
        }
        /**
         *
         * @param {Object} player

         */
        function replaceOffensiveBattleCard(player) {
            if(!player.aiControlled) { return; }
            TileFactory.offensiveStrategy(player);
            for (var i=0, len=player.unplayedBattleCards.length; i<len; i++) {
                var card = player.unplayedBattleCards[i];
                if (card.strength == 3 && card.damage == 1) {
                    player.unplayedBattleCards.splice(i, 1);
                    player.unplayedBattleCards.push(BattleCardFactory.offensiveStrategy);
                    player.isBuyingOffensiveStrategy = false;
                    return;
                }
            }
        }
        /**
         *
         * @param {Object} player
         */
        function replaceDefensiveBattleCard(player) {
            if(!player.aiControlled) { return; }
            TileFactory.offensiveStrategy(player);
            for (var i=0, len=player.unplayedBattleCards.length; i<len; i++) {
                var card = player.unplayedBattleCards[i];
                if (card.strength == 3 && card.shield == 1) {
                    player.unplayedBattleCards.splice(i, 1);
                    player.unplayedBattleCards.push(BattleCardFactory.defensiveStrategy);
                    player.isBuyingDefensiveStrategy = false;
                    return
                }
            }
        }
        /**
         *
         * @param {Object} player
         * @param {Object} army
         * @param {String} origin
         * @param {String} destination
         * @returns {String}
         */
        function moveAI(player, army, origin, destination) {
            var result;
            if (!canEnterLocation(player, origin, destination)) {
                return 'error';
            }
            if(canWalk(player, origin, destination)) {
                result = walk(player, army, origin, destination);
            } else if(canTeleportWalk(player, origin, destination)) {
                result = teleportWalk(player, army, origin, destination);

            } else {
                return 'error'
            }
            return result;
        }

        /**
         *
         * @param {Object} player
         * @param {String} origin
         * @param {String} destination
         * @returns {Boolean}
         */
        function canEnterLocation(player, origin, destination) {
            if(GameFactory.cities.indexOf(destination) > -1 ) {
                if(!PlayFactory.canEnterCity(player, origin, destination)) {
                    return false;
                }
            }
            return true;
        }

        /**
         *
         * @param {Object} player
         * @param {String} origin
         * @param {String} destination
         * @returns {Boolean}
         */
        function canTeleportWalk(player, origin, destination) {
            if(player.prayer + player.teleportDiscount + player.generalDiscount < 2) {
                return false;
            }

            var teleport_start = getClosestUnoccupiedSpecialLocation(origin,
                GameFactory.cities);
            var teleport_end = getClosestUnoccupiedSpecialLocation(destination,
                GameFactory.obeliskSpaces);

            if (!teleport_start || !teleport_end) {
                return false;
            }

            var distance = getDistance(origin, teleport_start) +
                getDistance(teleport_end, destination);
            return distance <= player.movement + player.creatureMovement;
        }

        /**
         *
         * @param {Object} player
         * @param {String} origin
         * @param {String} destination
         * @returns {Boolean}
         */
        function canWalk(player, origin, destination) {
            if(getPath(origin, destination) === undefined) {
                return false;
            }
            return (getDistance(origin, destination)
            <= player.movement + player.creatureMovement);
        }

        /**
         *
         * @param {Object} player
         * @param {Object} army
         * @param {String} origin
         * @param {String} destination
         * @returns {String}
         */
        function teleportWalk(player, army, origin, destination) {
            var teleport_start_space = getClosestUnoccupiedSpecialLocation(origin, GameFactory.cities);
            var teleport_end_space = getClosestUnoccupiedSpecialLocation(destination, GameFactory.obeliskSpaces);
            walk(player, army, origin, teleport_start_space);
            var result = PlayFactory.changeLocation(player, army,
                teleport_start_space, teleport_end_space);
            // If army is at destination end function else walk to destination
            if(teleport_end_space == destination) {
                return result;
            } else {
                return walk(player, army, teleport_end_space, destination);
            }
        }

        /**
         *
         * @param {Object} player
         * @param {Object} army
         * @param {String} origin
         * @param {String} destination
         * @returns {String}
         */
        function walk(player, army, origin, destination) {
            var path_arr = getPath(origin, destination);
            if (!path_arr) {
                console.error('No path array')
                console.info(origin);
                console.info('destination')
            }


            for (var i=0, len=path_arr.length-1; i<len; i++) {
                var result = PlayFactory.changeLocation(player, army, path_arr[i], path_arr[i+1])
            }
            return result;
        }

        /**
         *
         * @param {String} space
         * @param {String[]} special_locations
         * @returns {String[]}
         */
        function getClosestUnoccupiedSpecialLocation(space, special_locations) {
            // starting from space, expand outwards, check for closest neighbor
            // that is a special location and is unoccupied (unless it's the space itself);
            for (var n=0, len=GameFactory.maxDistance; n<len; n++) {
                var neighbors = getNeighborsAtDistance(space, n);
                for (var i = 0; i < neighbors.length; i++) {
                    if (special_locations.indexOf(neighbors[i]) > -1 &&
                        (!PlayFactory.checkSpace(neighbors[i]) || neighbors[i] == space)) {
                        return neighbors[i];
                    }
                }
            }
        }

        /**
         *
         * @param {String}  origin
         * @param {String}  destination
         * @returns {String[]} or {undefined}
         */

        function getPath(origin, destination) {
            if (!origin || !destination) {
                console.error('Missing Parameters');
                return;
            }
            var distance = getDistance(origin, destination);
            if (distance == undefined) { return; }

            var ori_arr = [];
            var dest_arr = [];

            for (var i=0, len=distance+1; i<len; i++) {
                ori_arr.push(getNeighborsAtDistance(origin, i));
                dest_arr.push(getNeighborsAtDistance(destination, i));
            }
            var passable_locations = getEmptyLocations();
            // Add origin to passable locations array in order to ensure inclusion in path array
            passable_locations.push(origin);
            passable_locations.push(destination);
            var path_arr = findMatchingElements(ori_arr, dest_arr.reverse(), passable_locations);
            return path_arr;
        }


        // For each corresponding array, finds a common element of arr_1 and arr_2
        // that is the allowed_elements_arr
        // It returns an array of the common elements if there was a common element in each array row
        /**
         *
         * @param arr_1
         * @param arr_2
         * @param allowed_elements_arr
         * @returns {Array}
         */
        function findMatchingElements(arr_1, arr_2, allowed_elements_arr) {
            if (!arr_1 || !arr_2 || !allowed_elements_arr ||arr_1.length != arr_2.length) {
                console.error('Three arrays must be passed as parameters and ' +
                    'first two arrays must be of equal length')
            }
            var common = [];
            for (var b=0, len=arr_1.length; b<len; b++) {
                for (var i = 0; i<arr_2[b].length; i++) {
                    // check if elements in row 'b' are allowed
                    if(allowed_elements_arr.indexOf(arr_2[b][i]) == -1) {
                        continue;
                    }
                    // check if any of the elements in row 'b' of both arrays
                    // match. If they do, check next row
                    if (arr_1[b].indexOf(arr_2[b][i]) > -1) {
                        common.push(arr_2[b][i]);
                        break;
                    }
                }
            }
            if (arr_1.length == common.length) {
                return common;
            }
        }

        /**
         *
         * @param origin
         * @param destination
         * @returns {number}
         */
        function getDistance(origin, destination) {
            if(origin == destination) { return 0; }
            for (var i=0, len=GameFactory.maxDistance; i<len; i++) {
                if (getDegreeNeighbors(origin, i).indexOf(destination) > -1) {
                    return i;
                }
            }
        }

        /**
         *
         * @param space
         * @param degree
         * @returns {*[]}
         */
        function getDegreeNeighbors(space, degree) {

            if (degree < 0 || degree > 4) {
                console.error('degree parameter out of range. Value between 0-4 inclusive expected');
            }

            var neighbors = [space];
            for (var i=0, len=degree; i<len; i++) {
                neighbors = getNeighbors(neighbors);
            }
            return neighbors;
        }

        /**
         *
         * @param space
         * @param distance
         * @returns {*}
         */
        function getNeighborsAtDistance(space, distance) {
            if(distance == 0) {
                return [space];
            }
            var allNeighbors = getDegreeNeighbors(space, distance);
            var prevDegreeNeighbors = getDegreeNeighbors(space, distance - 1);
            return arr_diff(allNeighbors, prevDegreeNeighbors);
        }

        /**
         *
         * @param spaces
         * @returns {Array}
         */
        function getNeighbors(spaces) {
            var neighbors = [];
            for (var n=0, len=spaces.length; n<len; n++) {
                var adjSpaces = GameFactory.nextTo[spaces[n]];
                for (var i=0, length=adjSpaces.length; i<length; i++) {
                    if (neighbors.indexOf(adjSpaces[i]) == -1) {
                        neighbors.push(adjSpaces[i]);
                    }
                }
            }
            return neighbors;
        }


        /**
         * gets tile information
         @param {String}  name
         @returns: (Object) tile object and index
         */
        function getTile(name) {
            var tiles = {};

            for (var i = 0; i<ai.colors.length; i++) {
                tiles = TileFactory[ai.colors[i]+"Tiles"];

                for (var n = 0; n < tiles.length; n++) {
                    if(tiles[n].name == name) {
                        return {tile: tiles[n], index: n};
                    }
                }
            }
        }

        /**
         * Finds a city that contains an army
         * @param {String}  type: information about what type of army the function is looking for
         * @returns {String}: Location of army
         */
        function findCityWithArmy(type) {

            var armies = getPlayerArmies();
            if(armies.length>0) {
                // Sort armies array largest first
                armies = sortObjInArrayBySize(armies);

                if (type == 'weakest') {
                    return armies[armies.length -1].location;
                } else if (type == 'no creature') {
                    // return largest army without creature attached
                    for (var i=0, len=armies.length; i<len; i++) {
                        if (!armies[i].creature) {
                            return armies[i].location;
                        }
                    }
                } else {
                    return armies[0].location;
                }
            }
        }

        /**
         *
         * @returns {String[]}
         */
        function getTempleLocations() {
            var locations = [];
            for (var n = 0; n < spaces.length; n++) {
                if (spaces[n].indexOf('temp') > -1) {
                    locations.push(spaces[n]);
                }
            }
            return locations;
        }

        /**
         *
         * @returns {String[]}
         */
        function getEmptyLocations() {
            var armyLocations = [];

            //For each player in the game, put army locations in one array
            for (var i=0, len=PlayFactory.playerArray.length; i<len; i++) {
                var playerArmyLocations = PlayFactory.getArmyLocations(PlayFactory.playerArray[i]);
                armyLocations = armyLocations.concat(playerArmyLocations);
            }
            // return an array of locations without armies
            return arr_diff(spaces, armyLocations);
        }



        //Loop through array
        //if you find value in spec corresponding to query
        // return the ans that corresponds to the query
        // else return the whole object
        /**
         *
         * @param {String[]} array
         * @param {Array} query
         * @param {String} spec
         * @param {String} ans
         * @returns {Array}
         */
        function queryArray(array,  query, spec, ans) {
            for(var i=0; i<array.length; i++){
                if(spec) {
                    if(array[i][spec] === query) {
                        if(ans) {
                            return array[i][ans];
                        }
                        return array[i];
                    }
                } else {
                    if(array[i] === query) {
                        if(ans) {
                            return array[i][ans];
                        }
                        return array[i];
                    }
                }
            }
        }

        /**
         * returns the difference between two arrays
         * @param {Array} a1
         * @param {Array} a2
         * @returns {Array}
         */

        function arr_diff (a1, a2) {

            var a = [], diff = [];
            for (var i = 0; i < a1.length; i++) {
                a[a1[i]] = true;
            }
            for (var i = 0; i < a2.length; i++) {
                if (a[a2[i]]) {
                    delete a[a2[i]];
                } else {
                    a[a2[i]] = true;
                }
            }
            for (var k in a) {
                diff.push(k);
            }
            return diff;
        }

        /**
         *
         * returns {Promise}
         */
        function getAllMoves() {
            return $http.get('/moves').success(function(data) {
                angular.copy(data, factory.moves);
            });
        }

        /**
         *
         * returns {Promise}
         */
        function getAllGames() {
            return $http.get('/games').success(function(data) {
                angular.copy(data, factory.games);
            });
        }

        /**
         *
         * @param {Object} move
         * returns {Promise}
         */
        function storeMove(move) {
            return $http.post('/moves', move).success(function(data){
                factory.moves.push(data);
            })
        }

        /**
         *
         * @param {Object} game
         * returns {Promise}
         */
        function storeGame(game) {
            return $http.post('/games', game).success(function(data) {
                factory.games.push(data);
            })
        }

        /**
         *
         * @param {Number} gameNo
         */
        function recordGame(gameNo) {
            var winner = PlayFactory.determineWinner();
            var loser = PlayFactory.getOpponent(winner);
            winner.finalData = {
                movement: winner.movement,
                damage: winner.damage,
                shield: winner.shield,
                points: winner.points,
                battlePoints: winner.battlePoints,
                templeTPoints: winner.templeTPoints,
                templePPoints: winner.templePPoints,
                tilePoints: winner.tilePoints,
                pyramidPoints: winner.pyramidPoints,
                attacks: winner.attacksInitiated,
            },

                loser.finalData = {
                    movement: loser.movement,
                    damage: loser.damage,
                    shield: loser.shield,
                    points: loser.points,
                    battlePoints: loser.battlePoints,
                    templeTPoints: loser.templeTPoints,
                    templePPoints: loser.templePPoints,
                    tilePoints: loser.tilePoints,
                    pyramidPoints: loser.pyramidPoints,
                    attacks: loser.attacksInitiated,

                },

                factory.storeGame({
                    game: gameNo,
                    winner: winner.name,
                    winnerData: winner.finalData,
                    winnerTiles: getTilesBought(winner),
                    loser: loser.name,
                    loserData: loser.finalData,
                    loserTiles: getTilesBought(loser),
                    // moves: moves
                })
        }

        /**
         *
         * @param {Object} player
         */
        function getTilesBought(player) {
            var tiles = [];
            for (var i=0, len=GameFactory.colors.length; i<len; i++) {
                var colorTiles = player[GameFactory.colors[i] + 'Tiles'];
                for(var n=0; n<colorTiles.length; n++) {
                    tiles.push(colorTiles[n].name);
                }
            }
            return tiles;
        }

        /**
         *
         * @param {String[]} action
         * @param {String} result
         */
        function recordMove(action, result) {
            var player = PlayFactory.getPlayer();
            var move = {};
            move.game = 0;
            move.player = player.name;
            move.round = GameFactory.round;
            move.turn = player.actionsTaken;
            move.points = player.points;
            move.action = action;
            move.result = result;

            factory.storeMove({
                game: move.game,
                player: move.player,
                round: move.round,
                turn: move.turn,
                points: move.points,
                action: move.action,
                result: move.result
            });

            moves.push(move);

        }

        /**
         * Tests MLFactory functions
         */
        function tests() {
            var player = PlayerOneFactory;
            player.aiControlled = true;
            player.prayer = 7;
            var origin = 'redC1';
            var destination = 'buff1';
            var army = player.armies[0];
            var space = 'redC1';
            var distance = 1;
            var degree = 1;

            funcExists(beginGame);
            funcExists(run);

            checkAfterFuncRuns(pickPyramids, "player.redPyramidLevel + "
                + "player.bluePyramidLevel + player.whitePyramidLevel == 3");
            funcReturns("populateActionsArray()", 'array');
            checkAfterFuncRuns(placeRecruits, "player.armies[0].size >= 0");
            checkArrayLength(getNamesOfAvailableTiles);
            checkArrayLength(addPrayActions);
            checkArrayLength(addTileActions);
            checkArrayLength(addUpgradeActions);
            checkArrayLength(addTileActions);
            equals("getPath('blueC1', 'buff1')[1]", 'buff1');
            equals("getPath('redC1', 'redC2')[3]", 'buff3');
            funcExists(aiFight);
            funcRuns("aiFight()");
            funcReturns("getRetreatLocationChoice('buff1')", 'string');
            funcReturns("getPostBattleChoice()", 'string');
            funcRuns("getPostBattleChoice()");
            funcRuns("playCards()");
            checkAfterFuncRuns(noAction, "player.actionsTaken >= 1");
            funcExists(takeAction);
            funcRuns("playBattleCards()");
            funcRuns("playDICards()");
            funcRuns("randomDICard(player, 0, 50)");
            checkAfterFuncRuns("attachCreature(player, {tile: TileFactory.redTiles[8]})",
            "player.armies[0].creature || player.armies[1].creature || " +
            "player.armies[2].creature ");
            checkArrayLength(getPlayerArmies);
            equals("sortObjInArrayBySize([{size:1}, {size:3}, {size:2}])[0].size", 3);
            funcExists(aiFn);
            player.prayer = 7;
            checkAfterFuncRuns("aiFn('pray', 'row2')", "player.prayer == 8");
            checkAfterFuncRuns("aiUpgradeAction(player, 'white', 1)",
                'player.whitePyramidLevel == 1');
            checkAfterFuncRuns("aiBuyAction('priestess')",
                "player.whiteTiles[0].name == 'priestess'");
            checkAfterFuncRuns("aiMoveAction(player, 'redC1', 'buff1', 'row1')",
                "player.armies[0].location == 'buff1' || player.armies[1]." +
                "location == 'buff1' || player.armies[2].location == 'buff1'");
            checkAfterFuncRuns("aiRecruitAction(player, 1)",
                "player.armies[0].size + player.armies[1].size + " +
                "player.armies[2].size + player.armies[3].size == 11");
            checkArrayLength(getCitiesWithoutFullArmies);
            var cities = getCitiesWithoutFullArmies(player);
            player.prayer = 7;
            funcRuns("addSoldiersToCities(player, 1, cities)");
            funcRuns("replaceDefensiveBattleCard(player)");


            funcRuns("moveAI(player, player.armies[0], origin, destination)");
            funcRuns("canEnterLocation(player, origin, destination)");
            funcRuns("canTeleportWalk(player, origin, destination)");
            funcRuns("canWalk(player, origin, destination)");
            funcRuns("teleportWalk(player, army, origin, destination)");
            funcRuns("walk(player, army, origin, destination)");
            funcRuns("getClosestUnoccupiedSpecialLocation(space, GameFactory.obeliskSpaces)");
            funcRuns("getPath(origin, destination)");
            funcRuns("findMatchingElements(['redC1'], ['redC1'], ['redC1'])");
            funcRuns("getDistance(origin, destination)");
            funcRuns("getDegreeNeighbors(space, degree)");
            funcRuns("getNeighborsAtDistance(space, distance)");
            funcRuns("getNeighbors(spaces)");
            funcRuns("getTile('Priestess')");
            funcRuns("findCityWithArmy('no creature')");
            funcRuns("getTempleLocations()");
            funcRuns("getEmptyLocations()");
            funcRuns("queryArray(player.armies, 'redC1', 'location', 'size')");
            funcRuns("arr_diff (['redC1', 'blueC1'], ['redC1'])");
            funcRuns("getTilesBought(player)");


            player.redPyramidLevel = 0;
            player.bluePyramidLevel = 0;
            player.whitePyramidLevel = 0;

            /**
             *
             * @param {Function} func
             */
            function funcExists(func) {
                GameFactory.assert(typeof(func) == 'function', func.name + ' is' +
                    'not a function', func.name + ' exists')
            }

            /**
             *
             * @param {String} funcExpr
             * @param {String} condition
             */
            function checkAfterFuncRuns(funcExpr, condition) {
                var func_name;
                // run function;
                if (typeof(funcExpr) == 'string'){
                    eval(funcExpr);
                    func_name = getFuncName(funcExpr);
                } else {
                    funcExpr();
                     func_name = funcExpr.name;
                }
                // capture the lhs of the condition
                var match = /.+?(?==|>|<)/.exec(condition);
                var rhs = /[=<>].+?/.exec(condition);
                rhs = rhs ? rhs[0] : condition;

                var expr = match ? match[0] : condition;
                var err_message = func_name + ' function failed \n' + expr +
                        'evaluates to ' + eval(expr) + ' and not ' + rhs;
                var success_message = func_name + ' function passed \n' + expr +
                    'evaluates to ' + eval(expr);
                GameFactory.assert(eval(condition), err_message, success_message);
            }

            /**
             * Returns the name of the function in the funcExpr string
             * @param {String} funcExpr
             * @returns {String}
             */
            function getFuncName(funcExpr) {
                return /.+?(?=\()/.exec(funcExpr)[0]
            }

            /**
             * Checks if the function referred to in funcExpr returns the
             * correct type of value
             * @param {String} funcExpr
             * @param {String} expected_return_type
             */
            function funcReturns(funcExpr, expected_return_type) {

                var actual_return_type = typeof(eval(funcExpr));
                var func_name = getFuncName(funcExpr);
                // check if the return value is an array
                if (actual_return_type == 'object' && eval(funcExpr) instanceof Array) {
                    actual_return_type = 'array';
                }

                var err_message = func_name + ' should return a '
                    + expected_return_type + '. ' + actual_return_type + ' returned';

                var success_message = func_name + ' returns a(n) ' + actual_return_type;

                GameFactory.assert(actual_return_type == expected_return_type,
                    err_message, success_message);
            }

            /**
             *
             * @param {String} funcExpr
             */
            function funcRuns(funcExpr) {
                var func_name = getFuncName(funcExpr);
                eval(funcExpr);
                console.log("Pass: " + func_name + ' ran successfully');
            }

            /**
             * Checks if function returns an array that contains an element
             * @param {Function} func
             */
            function checkArrayLength(func) {
                var player = PlayerOneFactory;
                var input_arr = [];
                var arr = func(player, input_arr);
                if (arr instanceof Array) {
                    GameFactory.assert(arr.length > 0, func.name + ' returned'
                        + ' an empty array.', func.name + ' returned an array of '
                    + arr.length + ' ' + typeof(arr[0]) + '(s)');
                } else {
                    console.log(func.name + ' returned a ' + typeof(func) +
                            ' instead of an array');
                }
            }

            /**
             *
             * @param {String} funcExpr
             * @param {String} expected_value
             */
            function equals(funcExpr, expected_value) {
                // Capture function name
                var func_name = getFuncName(funcExpr);
                // Evaluate function expression
                var actual_value = eval(funcExpr);

                var err_message = func_name + ' returned ' + actual_value +
                        ' instead of ' + expected_value;
                var success_message = func_name + ' returned ' + actual_value;
                GameFactory.assert(actual_value === expected_value, err_message,
                success_message)
            }
        }
        return factory;
    }
})();