var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Move = mongoose.model('Move');
var Game = mongoose.model('Game');


/* GET home page. */
router.get('/', function(req, res) {
  res.render('index', { title: 'Express' });
});


router.post('/moves', function(req, res, next) {
  var move = new Move(req.body);

  move.save(function(err, move) {
    if(err) { return next(err); }

    res.json(move);
  });
});

router.post('/games', function(req, res, next) {
  var game = new Game(req.body);

  game.save(function(err, game) {
    if(err) { return next(err); }

    res.json(game)
  });
});



router.get('/moves', function(req, res, next) {
  Move.find(function(err, moves) {
    if (err) {return next(err); }
    res.json(moves);
  });
});

router.get('/games', function(req, res, next) {
  Game.find(function(err, games) {
    if(err) { return next(err); }
    res.json(games);
  });
});

router.get('/games/latest', function(req, res, next) {

  Game.findOne().sort('-created_at').exec(function(err, game) {
    if(err) { return next(err); }
    console.log(game.game);
    res.json(game.game);
  });
});



module.exports = router;
