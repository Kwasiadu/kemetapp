var mongoose = require('mongoose');

var MoveSchema = new mongoose.Schema({
    game: {type: Number, default: 0},
    player : String,
    round : {type: Number},
    turn : {type: Number},
    points : {type: Number},
    action : [String],
    result : String
});

mongoose.model('Move', MoveSchema);