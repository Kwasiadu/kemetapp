/**
 * Created by Kwasi on 4/29/16.
 */
var mongoose = require('mongoose');

var GameSchema = mongoose.Schema({
    game: {type: Number},
    winner: String,
    winnerData: {
        points: Number,
        battlePoints: Number,
        templeTPoints: Number,
        templePPoints: Number,
        tilePoints: Number,
        pyramidPoints: Number,
        attacks: Number,
    },

    winnerTiles: [String],
    loser: String,
    loserData: {
        points: Number,
        battlePoints: Number,
        templeTPoints: Number,
        templePPoints: Number,
        tilePoints: Number,
        pyramidPoints: Number,
        attacks: Number,

    },
    
    loserTiles: [String],
    // moves: []
});

mongoose.model('Game', GameSchema);
